$(document).ready(function () {

    $('#inquiry').on('submit', function (e) {
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var post_data = $('#inquiry').serialize();
    simPost(post_data, 'POST', '/contact/inquiry', createcontactResponse);
    e.preventDefault();
    return false;
});

function createcontactResponse( response ) 
{
    $('.modal-message').html('');
    if(response == true)
    {
        $('#inquiry')[0].reset();
        swal("Inquiry Sent!", "We will contact you shortly", "success");
    } else {
         swal("Validation Error!", "Please complete the required fields below", "error");
    }

    return false;
}
});