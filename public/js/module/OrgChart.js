// Chart
$("#addOrgChartForm").on('submit', function(e) {
      $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createChart', createOrgChartResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);

    return false;

});

function createOrgChartResponse(response) 
{
    swal("Success!", "OrgChart has been added", "success");
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('#addOrgChart').modal('hide');
    $('.form-group').removeClass('has-error');
}

$(".updateOrgChart").on('click', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/ChartInfo', OrgChartInfoResponse);
    e.preventDefault();

   
});


$("#updateOrgChartForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateChart', updateChartResponse);
    e.preventDefault();
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

$('a.deleteOrgChart').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
  simPost({id:id}, 'POST', '/dashboard/deleteChart', deleteOrgChartResponse);
  e.preventDefault();
     swal("Success!", "The Position has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});
function deleteOrgChartResponse( response ) 
{
  if(response==true)
  {
    window.setInterval(function(){
      location.reload();
    }, 1200);
    swal("Success!", "You have deleted the slider!", "success");
  }
  else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 

  return false;
}
function OrgChartInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#name").val(value.name);
        $("input#jobtitle").val(value.jobtitle);
        $("select#dept_id").val(value.dept_id);
        $("input#rank").val(value.rank);
    });
}

function updateChartResponse(response) {
    swal("Success!", "Organization Chart has been updated", "success");
    $('#updateChart').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

