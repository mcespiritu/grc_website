// Career
$("#addCareerForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createCareer', createCareerResponse);
    e.preventDefault();
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);
    return false;

});

function createCareerResponse(response) {
    swal("Success!", "Job Post has been added", "success");
    $('#addCareer').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
}




$('a.deleteCareerBanner').on('click', function(e) {
    var id = this.id;
    swal({
            title: "Are you sure?",
            text: "Are you sure you want to delete this slider",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {

            if (isConfirm) {
                simPost({
                    id: id
                }, 'POST', '/dashboard/deleteCareerBanner', deleteCareerBannerResponse);
                e.preventDefault();
                swal("Success!", " Banner has been deleted", "success");
                setTimeout(function() {
                    window.location.reload(1);
                }, 1000);
            } else {
                swal("Cancelled", "You've cancelled to delete this banner", "error");
            }
        });

    return false;
});

function deleteCareerBannerResponse(response) {
    if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
        return false;
    }

    $('a.deleteBidBanner').on('click', function(e) {
        var id = this.id;
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this slider",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

                if (isConfirm) {
                    simPost({
                        id: id
                    }, 'POST', '/dashboard/deleteBidBanner', deleteBidBannerResponse);
                    e.preventDefault();
                    swal("Success!", "Bid Banner has been deleted", "success");
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 1000);
                } else {
                    swal("Error!", "You unsucessfully deleted the slider!", "error");
                }
            });

        return false;
    });
}

function deleteBidBannerResponse(response) {

    if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
        return false;
    }

  }
    $('a.deleteCareer').on('click', function(e) {
        var id = this.id;
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this job post?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

                if (isConfirm) {
                    simPost({
                        id: id
                    }, 'POST', '/dashboard/deleteCareer', deleteJobPostResponse);
                    e.preventDefault();
                    swal("Success!", "Job Post has been deleted", "success");
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 1000);
                } else {
                    swal("Error!", "You unsucessfully deleted the slider!", "error");
                }
            });

        return false;
    });
    function deleteJobPostResponse(response) {

        if (response == '') {
            window.setInterval(function(){
              location.reload();
            }, 1200);
            swal("Success!", "You have deleted the Job Post!", "success");
            return false;
        }

    }
    $("#updateCareerForm").on('submit', function(e) {

        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
        let post_data = new FormData(this)
        simPostUpload(post_data, 'POST', '/dashboard/updateCareer', updateCareerResponse);
        e.preventDefault();
        setTimeout(function() {
            window.location.reload(1);
        }, 1000);
    });

    function CareerInfoResponse(response) {
        $.each(response, function(key, value) {
            $("input#id").val(value.id);
            $("input#job_title").val(value.job_title);
            $("select.categories").val(value.category_id);
            $("textarea#job_desc").val(value.job_desc);
        });
    }

    function updateCareerResponse(response) {
        swal("Success!", "Job Post has been updated", "success");
        $('#updateCareer').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');

    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#banner')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#createCareerBanner").on('submit', function(e) {
        var post_data = new FormData(this);
        simPostUpload(post_data, 'POST', '/dashboard/createCareerBanner', createCareerBannerResponse);
        e.preventDefault();
        setTimeout(function() {
            window.location.reload(1);
        }, 1000);
        return false;

    });

    function createCareerBannerResponse(response) {
        swal("Success!", "Banner has been added", "success");
        $('#addCareerBanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    }
    $(".updateCareerbanner").on('click', function(e) {
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
        var id = this.id;
        simPost({
            id: id
        }, 'POST', '/fetch/careerBannerInfo', CareerBannerInfoResponse);
        e.preventDefault();
    });


    $("#updateCareerBannerForm").on('submit', function(e) {

        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
        let post_data = new FormData(this)
        simPostUpload(post_data, 'POST', '/dashboard/updateCareerBanner', updateCareerBannerResponse);
        e.preventDefault();

        setTimeout(function() {
            window.location.reload(1);
        }, 1000);
    });

    $('a.deleteCareerBanner').on('click', function(e) {
        var id = this.id;
        simPost({
            id: id
        }, 'POST', '/dashboard/deleteCareerBanner', deleteCareerbannerResponse);
        e.preventDefault();
        return false;
    });

    function deleteCareerbannerResponse(response) {
        if (response == true) {
            window.setInterval(function() {
                location.reload();
            }, 1200);
            swal("Success!", "You have deleted the slider!", "success");
        } else {
            swal("Error!", "You unsuceessfully deleted the slider!", "success");
        }

        return false;
    }

    function CareerBannerInfoResponse(response) {
        $.each(response, function(key, value) {
            $("input#id").val(value.id);
            $("input#title").val(value.title);
            $("textarea#subtitle").val(value.subtitle);
            $(".img-upload").append('' +
                '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/' + value.banner_filename + '" alt="' + value.title + '" />');
        });
    }

    function updateCareerBannerResponse(response) {
        swal("Success!", "Banner has been updated", "success");
        $('#updateCareerBanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');

    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#banner')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function() {
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    });