// Company Details
$("#addFAQForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createFAQ', createFAQResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createFAQResponse(response) {
        swal("Success!", "Job Post has been added", "success");
        $('#addCareer').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
}
$(".updateFAQClass").on('click', function(e){
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/FAQInfo', FAQInfoResponse);
    e.preventDefault();
});


$("#updateFAQForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateFAQ', updateFAQResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});



function FAQInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#question").val(value.question);
        $("textarea#answer").val(value.answer);
    });
}

function updateFAQResponse(response) {
    swal("Success!", "Job Post has been updated", "success");
    $('#updateFAQ').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$('a.deleteFAQ').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteFAQ', deleteFAQResponse);
     e.preventDefault();
     swal("Success!", "Banner has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteFAQResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}
