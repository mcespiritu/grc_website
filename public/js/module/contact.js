$("#createContactBannerForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createContactBanner', createContactBannerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createContactBannerResponse(response) {
    if (response == 'success') {
        swal("Success!", "Contact Banner has been added", "success");
        $('#addContactbanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "Contact Banner not saved", "error");
        $('#addContactbanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}
$(".updateContactbanner").on('click', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/ContactBannerInfo', aboutBannerInfoResponse);
    e.preventDefault();
});


$("#updateContactbannerForm").on('submit', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateContact', updateContactbannerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

function aboutBannerInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#title").val(value.title);
        $("textarea#subtitle").val(value.subtitle);
      $(".img-upload").append(''+
        '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.display_filename+'" alt="'+value.title+'" />');   
    });
}

function updateContactbannerResponse(response) {
    swal("Success!", "Banner has been updated", "success");
    $('#updateContactbanner').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$('a.deleteContactbanner').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteContactBanner', deleteContactBannerResponse);
     e.preventDefault();
     swal("Success!", "Contact Banner has been deleted", "success");

    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteContactBannerResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}

$("#inquiry").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/inquiry', createInquiryResponse);
    e.preventDefault();

        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createInquiryResponse(response) {
    if (response == true) {
        swal("Success!", "Your message has been sent! Our team will contact you soon.", "success");
       
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "Your message did'nt send", "error");
        
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}