$("#addBidForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createBid', addBidResponse);
    e.preventDefault();
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function addBidResponse(response) {
    if (response == true) {
        swal("Success!", "Bid  has been added", "success");
        $('#addBid').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "Bid  not saved", "error");
        $('#addBid').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}


$("#bidEntryForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createBidEntries', bidEntriesResponse);
    e.preventDefault();
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});
$('a.deleteBidbanner').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteBidBanner', deleteBidResponse);
     e.preventDefault();
     swal("Success!", " Banner has been deleted", "success");
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
  swal("Cancelled", "You've cancelled to delete this banner", "error");
  } 
 });
  
  return false;
});

function deleteBidResponse( response ) 
{
  if(response==true)
  {
    window.setInterval(function(){
      location.reload();
    }, 1200);
    swal("Success!", "You have deleted the slider!", "success");
  }
  else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "success");
  } 

  return false;
}
function bidEntriesResponse(response) {
    if (response == true) {
        swal("Success!", "Bid has been added", "success");
        $('#openBid').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "Bid not saved", "error");
        $('#openBid').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}

$(".updateBid").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/bidInfo', BidInfoResponse);
    e.preventDefault();
});

function BidInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("select#project_id").val(value.project_id);
        $("select#unit").val(value.unit);
        $("input#starting_amount").val(value.starting_amount);
        $("input#increment_limit").val(value.increment_limit);
        $("input#start_date").val(value.start_date);
        $("input#end_date").val(value.end_date);
    });
}

$(".openBid").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/bidDetails', BidDetailsResponse); /* get information of bid*/

    simPost({
        id: id
    }, 'POST', '/fetch/getBidAttachments', getBidAttachmentsResponse); /*get attachments*/

     simPost({

        id: id
    }, 'POST', '/fetch/getLastBid', LastBidDetailsResponse); /*get Last Bid*/

    e.preventDefault();

});


function getBidAttachmentsResponse(response) 
{
    $("#specificationRow").html("");
    $.each(response, function(key, value)
    {
        $("#specificationRow").append(''+
        '<div class="col-md-2 attachments text-center">'+
            '<a href="/storage/imageGallery/'+value.attachment_files+'" class=" text-center" target="_blank"><i class="o-paperwork-1 mr-3 text-gray attachment-icon"></i></a>'+
        '</div>'
        );
    });
}

function BidDetailsResponse(response) {
    let readPerBidData = response.readPerBidData
    let getLastBid     = response.getLastBid
    $.each(response.readPerBidData, function(key, value) {
        var tcp = new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'PHP' }).format(value.starting_amount);
        var il = new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'PHP' }).format(value.increment_limit);
        var lba = new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'PHP' }).format(getLastBid.bid_amount);
        $("input#bid_id").val(value.id);
        $("h5#project_name").html(value.project_name);
        $("span#blk").html(value.block);
        $("span#lt").html(value.lot);
        $("span#starting_amount").html(tcp);
        $("span#increment_limit").html(il);
        $("span#start_date").html(value.start_date);
        $("span#end_date").html(value.end_date);
        $("div#LastBid").html(lba)
        // $(".attachments").append(''+
        // '<a href="/storage/imageGallery/'+value.attachment_files+'" id="PDF"></a>');  
    });
}
function LastBidDetailsResponse(response) {
    $.each(response, function(key, value) {
        $("#LastBid").html(value.bid_amount);
    });
}


$("#updateBidForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateBid', updateBidResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

function updateBidResponse(response) {
    swal("Success!", "Bid has been updated", "success");
    $('#updateBid').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

//Bid Banner

$("#addBidBannerForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createBidBanner', addBidBannerResponse);
    e.preventDefault();
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function addBidBannerResponse(response) {
        swal("Success!", "Banner has been added", "success");
        $('#addBidBanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
}

 function bidnow()
 {
  $('#bid_box').append('<p><b>Bid Entry</b> : </p><input type="number" name="bid_entry" id="bid_entry" readonly=""/>');
    document.getElementById("placeBid").style.visibility = 'hidden';
    document.getElementById("submitBid").style.visibility = 'visible';
    var id = $("input#bid_id").val();
    simPost({id:id}, 'POST', '/getMinMaxIncrementalBid', getMinMaxIncrementalBidResponse);
    e.preventDefault();
}

function getMinMaxIncrementalBidResponse ( response )
{
    var id = $("input#bid_id").val();
    $.each(response, function(key, value) {
        $('input#bid_entry').attr({step:value.increment_limit});
    });
    simPost({id:id}, 'POST', '/getLastBidMade', getLastBidMadeResponse);
}

function getLastBidMadeResponse ( response )
{
        $('input#bid_entry').attr({value:response});
}

