$("#createAboutBannerForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createAboutBanner', createAboutBannerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createAboutBannerResponse(response) {
    if (response == 'success') {
        swal("Success!", "About Banner has been added", "success");
        $('#addAboutbanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "About Banner not saved", "error");
        $('#addAboutbanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}
$(".updateAboutbanner").on('click', function(e) {

    $('.modal-message').html("");
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/aboutBannerInfo', contactBannerInfoResponse);
    e.preventDefault();
    console.log(id);
});



$("#updateAboutBanner").on('submit', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateAbout', updateaboutBannerResponse);
    e.preventDefault();
    console.log();
    //     setTimeout(function(){
    //    window.location.reload(1);
    // }, 1000);
});

$('a.deleteAboutbanner').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteAbout', deleteAboutResponse);
     e.preventDefault();
     swal("Success!", "About Banner has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteAboutResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}
function contactBannerInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#about_title").val(value.about_title);
        $("textarea#about_subtitle").val(value.about_subtitle);
        $(".img-upload").append('' +
           '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.display_filename+'" alt="'+value.title+'" />');  
    });
}

function updateaboutBannerResponse(response) {
    swal("Success!", "Banner has been updated", "success");
    $('#updateAboutbanner').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


// Vision & Mission

$(".updateVision").on('click', function(e) {

    $('.modal-message').html("");
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/visionInfo', visionInfoResponse);
    e.preventDefault();
});


$("#updateVisionForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateVision', updateVisionResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);

});

function visionInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("textarea#vision_text").val(value.vision);
        $("textarea#mission_text").val(value.mission);
        $("textarea#quality_policy").val(value.quality_policy);
    });
}

function updateVisionResponse(response) {
    swal("Success!", "Vision and Mission has been updated", "success");
    $('#updateVision').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


// Company History
$("#createHistoryForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createHistory', createHistoryResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createHistoryResponse(response) {
    if (response == '') {
        swal("Success!", "A new Company History has been added", "success");
        $('#addHistory').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "About Banner not saved", "error");
        $('#addHistory').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}

$(".updateHistory").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/HistoryInfo', HistoryInfoResponse);
    e.preventDefault();
});


$("#updateHistoryForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateHistory', updateHistoryResponse);
    e.preventDefault();
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);

});

function HistoryInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#date").val(value.date);
        $("textarea#content_text").val(value.content);
    });
}

function updateHistoryResponse(response) {
    swal("Success!", "History has been updated", "success");
    $('#updateHistory').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


$(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     
});