$("#addAmenitiesForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createAmenities', createAmenitiesResponse);
    e.preventDefault();
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);
    return false;

});

function createAmenitiesResponse(response) {
    swal("Success!", "Feature has been added", "success");
    $('#addAmenities').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
}
$(".updateAmenitiesClass").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/AmenitiesInfo', AmenitiesInfoResponse);
    e.preventDefault();
});


$("#updateAmenitiesForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateAmenities', updateAmenitiesResponse);
    e.preventDefault();

    setTimeout(function() {
        window.location.reload(1);
    }, 1000);
});

function updateAmenitiesResponse(response) {
    swal("Success!", "Feature has been updated", "success");
    $('#updateAmenities').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

$('a.deleteAmenities').on('click', function(e)

    {
        var id = this.id;
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this Feature?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

                if (isConfirm) {
                  
                    simPost({
                        id: id
                    }, 'POST', '/dashboard/deleteAmenities', deleteAmenitiesResponse);
                    e.preventDefault();
                    swal("Success!", "Feature has been deleted", "success");
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 1000);
                } else {
                    swal("Error!", "You unsucessfully deleted the Feature!", "error");
                }
            });

        return false;
    });

function deleteAmenitiesResponse(response) {
    if (response == '') {
        window.setInterval(function() {
            location.reload();
        }, 1200);
        swal("Success!", "You have deleted the Feature!", "success");
        return false;
    }
}

function AmenitiesInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#name").val(value.name);
    });
}



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}