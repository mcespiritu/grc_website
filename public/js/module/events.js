// Company Details
$("#addEventForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createEvent', createEventResponse);
    e.preventDefault();
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createEventResponse(response) 
{
    swal("Success!", "Event has been added", "success");
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
}
$(".updateEventClass").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/EventInfo', EventInfoResponse);
    e.preventDefault();
});


$("#updateEventForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateEvent', updateEventResponse);
    e.preventDefault();
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

function EventInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("select#featured").val(value.featured);
        $("input#event_name").val(value.event_name);
        $("input#event_date").val(value.event_date);
        $("input#event_start").val(value.event_start);
        $("input#event_end").val(value.event_end);
        $("textarea#event_content").val(value.event_content);
        $(".img-upload").append(''+
        '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.thumbnail+'" alt="'+value.title+'" />');   
    });
}

function updateEventResponse(response) {
    swal("Success!", "Event has been updated", "success");
    $('#updateEvent').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


$('a.deleteEvent').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteEvent', deleteEventResponse);
     e.preventDefault();
     swal("Success!", "Banner has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteEventResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}
