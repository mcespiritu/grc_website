// Company Details

$(".updateCompanyDetails").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/companyDetailsInfo', companyDetailsInfoResponse);
    e.preventDefault();
});


$("#updateCompanyDetailsForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateCompanyDetails', updateCompanyDetailsResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

function companyDetailsInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#company_name").val(value.company_name);
        $("input#address").val(value.address);
        $("input#phone").val(value.phone);
        $("input#mobile").val(value.mobile);
        $("input#email").val(value.email);
        $("input#facebook").val(value.facebook);
        $("input#instagram").val(value.instagram);
        $("input#youtube").val(value.youtube);
        $("input#linkedin").val(value.linkedin);
         $("input#twitter").val(value.twitter);
    });
}

function updateCompanyDetailsResponse(response) {
    swal("Success!", "Company Details has been updated", "success");
    $('#updateCompanyDetails').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

