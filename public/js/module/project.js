$(document).ready(function() {
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
});


var form = $("#createProject");
form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    onStepChanging: function(event, currentIndex, newIndex) {
        form.validate().settings.ignore = ":disabled,:hidden";
        $("a[href='#finish']").css("display", "none");
        return form.valid();
    },
    onFinishing: function(event, currentIndex) {
        form.validate().settings.ignore = ":disabled";
        return form.valid();

    },
    onFinished: function(event, currentIndex) {
        //$("img").attr("width","500");
        // /* alert(form);
        //  var form = $("#createProject");
        //  var post_data = new FormData(form);
        //  var post_data = $('#createProject').serialize();
        //  simPostUpload(post_data, 'POST', '/dashboard/createProject', createProjectResponse);
        // // return false;
        //  var post_data = new FormData('#createProject');
        //  submitProject(post_data);
    }
});
// function submitProject() {
//     // var post_data = $('#createProject').serialize();
//     var post_data = new FormData(this);
//     simPostUpload(post_data, 'POST', '/dashboard/createProject', createProjectResponse);
//     e.preventDefault();
//     return false;
// }

$('#submitProject').on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var post_data = new FormData(form);
    simPostUpload(post_data, 'POST', '/dashboard/createProject', createProjectResponse);
    return false;
});

function createProjectResponse(response) {
    swal("Success!", "New Project has been added", "success");
    $('#addProject').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset message
    $('.form-group').removeClass('has-error');

}

$(".updateProject").on('click', function(e) {

    $('.modal-message').html("");
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/ProjectInfo', ProjectInfoResponse);
    e.preventDefault();

});

function ProjectInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#project_name").val(value.about_title);
        $("input#project_name").val(value.about_title);
        $("textarea#project_summary").val(value.about_subtitle);
        $(".img-upload").append('' +
            '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/' + value.display_filename + '" alt="' + value.title + '" />');
    });
}

$('a.deleteModelUnit').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteModelUnit', deleteModelUnitResponse);
     e.preventDefault();
     swal("Success!", "About Banner has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteModelUnitResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}



