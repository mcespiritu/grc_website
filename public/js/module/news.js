// Company Details
$("#addNewsForm").on('submit', function(e) {
        $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createNews', createNewsResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);

    return false;

});

function createNewsResponse(response) 
{
    swal("Success!", "News has been added", "success");
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('#addNews').modal('hide');
    $('.form-group').removeClass('has-error');
}
$(".updateNewsClass").on('click', function(e) {
      $('.modal-message').html('');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/NewsInfo', NewsInfoResponse);
    e.preventDefault();
});


$("#updateNewsForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateNews', updateNewsResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

function NewsInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#title").val(value.title);
        $("input#publish_date").val(value.publish_date);
        $("select#featured").val(value.featured);
        $("textarea#content").val(value.content);
        $(".img-upload").append(''+
        '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.thumbnail+'" alt="'+value.title+'" />');   
    });
}

function updateNewsResponse(response) {
    swal("Success!", "News has been updated", "success");
    $('#updateNews').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


$('a.deleteNews').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteNews', deleteNewsResponse);
     e.preventDefault();
     swal("Success!", "Banner has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteNewsResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}