$("#createBuyerBannerForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createBuyerBanner', createBuyerBannerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createBuyerBannerResponse(response) {
    if (response == 'success') {
        swal("Success!", "Buyer Banner has been added", "success");
        $('#updateBuyerBanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "Buyer Banner not saved", "error");
        $('#updateBuyerBanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}


$("#updateBuyerBannerForm").on('submit', function(e)
{
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateBuyerBanner', updateBuyerBannerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
  });


function updateBuyerBannerResponse(response) {
  swal("Success!", "The Banner has been updated", "success"); 
    $('#updateBuyerbanner').modal('hide');
    $('.modal-message').html('')
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    location.reload();
}
$(".updateBuyerBanner").on('click', function(e) {

    $('.modal-message').html("");
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/buyerBannerInfo', buyerBannerInfoResponse);
    e.preventDefault();
});


function buyerBannerInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#buyer_title").val(value.buyer_title);
        $("textarea#buyer_subtitle").val(value.buyer_subtitle);
        $(".img-upload").append('' +
           '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.display_filename+'" alt="'+value.title+'" />');  
    });
}


$('a.deleteBuyerbanner').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteBuyerbanner', deleteBuyerbannerResponse);
     e.preventDefault();
     swal("Success!", "Buyer Banner has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteBuyerbannerResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


$(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     



// Content

$(".updateBuyerContent").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/buyerContentInfo', buyerContentInfoResponse);
    e.preventDefault();
});


$("#updateBuyerContentForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateBuyerContent', updateBuyerContentResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

function buyerContentInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("textarea#content").val(value.content);

    });
}

function updateBuyerContentResponse(response) {
    swal("Success!", "Buyer Content has been updated", "success");
    $('#updateBuyerContent').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}



});


$("#register_buyer").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/registerBuyer', registerBuyerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function registerBuyerResponse(response) {
    if (response == true) {
        swal("Success!", "Your are registered.", "success");
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "You are not registered. Please fill up the required field.", "error");
        $('.form-group').addClass('has-error');
    }
}

$('#project_id').on('change', function(e)
{
   ;
  var id = $('#project_id').val();
  simPost({id:id}, 'POST', '/readHouseUnitFromTracker', readHouseUnitFromTrackerResponse);
   
});
function readHouseUnitFromTrackerResponse(response) 
{
  $('select#unit').val("")
  $("#unit").append("<option> Select Model Unit </option>"); 
  $.each(response, function(key, value)
  {
    $("#unit").append("<option value="+value.id+">"+value.block+' (' +value.lot+ ') - ' +value.model+' '+value.model_type+"</option>"); 
  });
}