$("#createBrokerBannerForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', './../brokers/createBroker', createBrokerBannerResponse);
    // simPostUpload(post_data, 'POST', '/dashboard/createBrokerBanner', createBrokerBannerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    return false;

});

function createBrokerBannerResponse(response) {
    if (response == 'success') {
        swal("Success!", "Broker Banner has been added", "success");
        $('#updateBrokerBanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "Broker Banner not saved", "error");
        $('#updateBrokerBanner').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}


$("#updateBrokerBannerForm").on('submit', function(e)
{

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateBrokerBanner', updateBrokerBannerResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);
  });


function updateBrokerBannerResponse(response) {
  swal("Success!", "The Banner has been updated", "success"); 
    $('#updateBrokerbanner').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
}
$(".updateBrokerBanner").on('click', function(e) {

    $('.modal-message').html("");
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/brokerBannerInfo', brokerBannerInfoResponse);
    e.preventDefault();
});


function brokerBannerInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#broker_title").val(value.broker_title);
        $("textarea#broker_subtitle").val(value.broker_subtitle);
        $(".img-upload").append('' +
           '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.display_filename+'" alt="'+value.title+'" />');  
    });
}


$('a.deleteBrokerbanner').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this slider",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteBrokerbanner', deleteBrokerbannerResponse);
     e.preventDefault();
     swal("Success!", "Broker Banner has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);

    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the slider!", "error");
  } 
 });
  
  return false;
});

function deleteBrokerbannerResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


$(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     


});


        $(document).ready(function(){

            // Custom method to validate username
            $.validator.addMethod("usernameRegex", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
            }, "Username must contain only letters, numbers");

            $(".next").click(function(){
                var form = $("#form-create-user");
                form.validate({
                    errorElement: 'span',
                    errorClass: 'help-block',
                    highlight: function(element, errorClass, validClass) {
                        $(element).closest('.form-group').addClass("has-error");
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).closest('.form-group').removeClass("has-error");
                    },
                    rules: {
                        first_name: {
                            required: true,
                        },
                        middle_name: {
                            required: true,
                        },
                        last_name: {
                            required: true,
                        },
                        mobile_number: {
                            required: true,
                        },
                        home_address: {
                            required: true,
                        },

                        sex: {
                            required: true,
                        },

                        email_address: {
                            required: true,
                            minlength: 3,
                        },
                        
                    },
                    messages: {
                        first_name: {
                            required: "Name required",
                        },
                        middle_name: {
                            required: "Last Name required",
                        },
                        last_name: {
                            required: "Last Name required",
                        },

                        mobile_number: {
                            required: "Phone required",
                        },
                        home_address: {
                            required: "Address required",
                        },
                        sex: {
                            required: "Gender required",
                        },
                    
                        email_address: {
                            required: "Email required",
                        },
                    }
                });
                if (form.valid() === true){
                    if ($('#basic_information').is(":visible")){
                        current_fs = $('#basic_information');
                        next_fs = $('#additional_information');
                    }else if($('#additional_information').is(":visible")){
                        current_fs = $('#additional_information');
              
                    }
                    
                    next_fs.show(); 
                    current_fs.hide();
                }
            });

            $('#previous').click(function(){
                if($('#additional_information').is(":visible")){
                    current_fs = $('#additional_information');
                    next_fs = $('#basic_information');
                }else if ($('#basic_information').is(":visible")){
                    current_fs = $('#basic_information');
                    next_fs = $('#additional_information');
                }
                next_fs.show(); 
                current_fs.hide();
            });
    
    
            
        });


    function readUsersResponse( response ) 
    {
        $("tbody").html("");
        $.each(response, function(key, value)
        {
                $("#user-list-data").append('<tr class="ct">'+
                  '<th>'+value.last_name+', '+value.first_name+' '+value.middle_name+'</th>'+
                  '<th>'+value.email_address+'</th>'+
                  '<th>'+value.birth_date+'</th>'+
                  '<th>'+value.tin+'</th>'+
                  '<th>'+value.status+'</th>'+
                  '<th>'+
                  '<button data-toggle="modal" data-target="#update_user" id="update-user_'+value.user_id+'" class="update_user btn btn-primary btn-xs update-user btn-circle" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></button><th>'+ 
                '</tr>');
        });

        // Disable
        $("button.deactivate_user").on('click', function(e)
        {
            var id = this.id;
            simPost({id:id}, 'POST', '/deactivateUser', updateUsersResponse);
            e.preventDefault();
        });

        // Enable
        $("button.activate_user").on('click', function(e)
        {
            var id = this.id;
            simPost({id:id}, 'POST', '/approveBroker', updateUsersResponse);
            e.preventDefault();
        });   

        var final_count = $('.ct').length;

        $("#cter").html(final_count);

        if(final_count==0 || final_count=="" || final_count.empty ){
            $("#search-notice").html("<a href='#' class='refresh'>[ Reset Search ]</a><i style='color:grey'> No result found!</i>");
        }

        $("div.preloader", "div.loader").html("");
        return false;
    }

    // Create Users
    $("#form-create-user").on("submit", function(e)
    {
        $('.modal-message').html('');
        console.log(this)
        var post_data = new FormData(this)
        simPostUpload(post_data, 'POST', './../brokers/createBroker', createBrokerResponse);        
        e.preventDefault();
        return false;
    });

    function createBrokerResponse(response) {
  swal("Success!", "Form has been submitted. Our Team will will contatc you soon. Thank you!", "success"); 
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
}


    // Read User
    // simPost(null, 'GET', './../brokers/createBroker', readUsersResponse);
