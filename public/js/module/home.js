$(".updateHomeBanner").on('click', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/homeBannerInfo', homeBannerInfoResponse);
    e.preventDefault();
});

function homeBannerInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#title").val(value.title);
        $("textarea#subtitle").val(value.subtitle);
        $(".img-upload").append('' +
            '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/' + value.banner_filename + '" alt="' + value.title + '" />');
    });
}

$("#createHomeBanner").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createHomeBanner', createHomeBannerResponse);
    e.preventDefault();
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);
    return false;

});
$("#addHomeBanner").click(function() {
    $('.modal-message').html('');
});


$("#updateHomeBanner").on('submit', function(e) {
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateHomeBanner', updateHomeBannerResponse);
    e.preventDefault();
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);
});

function createHomeBannerResponse(response) {

    swal("Success!", "An Home has been updated", "success");
    $('#addHomebanner').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');


}
$('a.deleteHomebanner').on('click', function(e) {

    if (confirm("Are you sure you want to delete this banner?")) {
        var id = this.id;
        simPost({
            id: id
        }, 'POST', '/dashboard/deleteHomeBanner', deleteHomeResponse);
        e.preventDefault();
    } else {
        swal("Cancelled", "You've cancelled to delete this banner", "error");
        return false;
    }


});

function deleteHomeResponse(response) {
    if (response == true) {
        window.setInterval(function() {
            location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
    } else {
        swal("Error!", "You unsucessfully deleted the slider!", "success");
    }

    return false;
}

function updateHomeBannerResponse(response) {
    swal("Success!", "An Home has been updated", "success");
    $('#updateHomebanner').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    location.reload();
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


// Summary

$(".updateHomeSummary").on('click', function(e) {
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/homeSummaryInfo', CompanySummaryBannerInfoResponse);
    e.preventDefault();
});

function CompanySummaryBannerInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#summary_title").val(value.summary_title);
        $("textarea#summary_content").val(value.summary_content);
        $("input#video_link").val(value.video_link);
    });
}



$("#addHomeSummaryForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createCompanySummary', createCompanySummaryBannerResponse);
    e.preventDefault();
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);
    return false;

});

$("form[id='updateHomeSummary']").on('submit', function(e) {
    let post_data = new FormData(this)
    console.log(post_data)
    e.preventDefault();
    simPostUpload(post_data, 'POST', '/dashboard/updateCompanySummary', updateCompanySummaryBannerResponse);
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);

});

function createCompanySummaryBannerResponse(response) {
    swal("Success!", "Company Summary has been created", "success");
    $('#addHomeSummary').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');


}

function updateCompanySummaryBannerResponse(response) {
    swal("Success!", "Company Summary has been updated", "success");
    $('#updateHomeSummary').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#banner')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


$(document).ready(function() {
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });
});
//Logo 



$("#uploadpartnerForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createLogo', createLogoResponse);
    e.preventDefault();
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);

});


function createLogoResponse(response) {

    swal("Success!", "An Partner Logo has been updated", "success");
    $('#addLogo').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');


}

$('a.deleteLogo').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this logo",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteLogo', deleteLogoResponse);
     e.preventDefault();
     swal("Success!", "Logo has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the logo!", "error");
  } 
 });
  
  return false;
});

function deleteLogoResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}

$(".updatePartner").on('click', function(e) {
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/LogoInfo', LogoInfoResponse);
    e.preventDefault();
});


function LogoInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("input#link").val(value.link);
        $(".img-upload").append('' +
            '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/' + value.logo + '" alt="' + value.link + '" />');
    });
}


$("form[id='updatePartnerForm']").on('submit', function(e) {
    let post_data = new FormData(this)
    console.log(post_data)
    e.preventDefault();
    simPostUpload(post_data, 'POST', '/dashboard/updateLogo', updateLogoResponse);
    setTimeout(function() {
        window.location.reload(1);
    }, 1000);

});

function updateLogoResponse(response) {
    swal("Success!", "Logo has been updated", "success");
    $('#updatepartner').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}