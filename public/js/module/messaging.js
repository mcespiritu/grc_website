simPost(null, 'GET', '/maintenance/contactInitialList', contactInitialListResponse);

function contactInitialListResponse( response )
{
  $("#contactInitialList").html("");
  $.each(response, function(key, value)
  {
    $("#contactInitialList").append(''+
      '<li class="contact">'+
      '<div class="wrap">'+
        '<span class="contact-status busy"></span>'+
        '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
        '<div class="meta">'+
          '<p class="name">'+value.uname+' '+value.lname+'</p>'+
      '</div>'+
      '</div>'+
    '</li>'
    );
    //Receiver
    var recUser = value.uid;

    $("input#sendToUser").val(value.uid);
    simPost({recUser:recUser}, 'POST', '/maintenance/recUser', recUserResponse); 

  });

  function recUserResponse( response )
  { 
    var userLoggedID = $(".userLoggedID").val();

    $("#convoMessages").html("");
    $.each(response, function(key, value)
    {

      var cutOrigdate = new Date(value.created_at);
      var year = cutOrigdate.getFullYear();
      var month = cutOrigdate.getMonth() + 1; // months are zero indexed
      var day = cutOrigdate.getDate();
      var hour = cutOrigdate.getHours();
      var minute = cutOrigdate.getMinutes();
      var second = cutOrigdate.getSeconds();
      var hourFormatted = hour % 12 || 12; // hour returned in 24 hour format
      var minuteFormatted = minute < 10 ? "0" + minute : minute;
      var morning = hour < 12 ? "am" : "pm";

      var formatedDate = month + "/" + day + "/" + year + " " + hourFormatted + ":" +
      minuteFormatted + morning;

      if(value.sent_by == userLoggedID)
      {

        $("#convoMessages").append(''+
          '<li class="sent">'+
            '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
              '<div class="sent-msg">'+
              '<p>'+value.message+'</p>'+
              '<span class="text-muted pull-right"><small>'+formatedDate+'</small></span>'+
              '<div>'+
          '</li>'
        );
      }
      else
      {
        $("#convoMessages").append(''+
          '<li class="replies">'+
            '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
              '<div class="reply-msg">'+
              '<p>'+value.message+'</p>'+
              '<span class="text-muted"><small>'+formatedDate+'</small></span>'+
              '<div>'+
          '</li>'
        );
      }
    });
  }

  $("#contact").click(function(){
      $("li.contact.active").removeClass("active");
      $(this).addClass('active');
    
    if ($('li.contact').hasClass('active')){
          
     $('.content').show();
  
      }
    else 
      {
        $('.content').hide();
      }
  });
}

// $("#notifications").click(function(){
//   $('#valueTypeAhead').keyup(function () {
//       setTimeout(function () {
//           var fieldData = $("#valueTypeAhead").val();
//            simPost({fieldData:fieldData}, 'POST', '../../maintenance/autocomplete', getAutoCompleteResponse);
//       }, 100);
//   });
// });

function getAutoCompleteResponse( response )
{ 

  $("#contactsInner").html("");
  $.each(response, function(key, value)
  {
    $("#contactsInner").append(''+
      '<li class="contact">'+
      '<div class="wrap">'+
        '<span class="contact-status busy"></span>'+
        '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
        '<div class="meta">'+
          '<p class="name">'+value.uname+' '+value.lname+'</p>'+
          '<p class="preview">You: '+value.msg+'</p>'+
      '</div>'+
      '</div>'+
    '</li>'
    );
    // Receiver
    var recUser = value.uid;

    $("input#sendToUser").val(value.uid);
    simPost({recUser:recUser}, 'POST', '/maintenance/recUser', recUserResponse);
  });


  function recUserResponse( response )
  { 
    var userLoggedID = $(".userLoggedID").val();

    $("#convoMessages").html("");
    $.each(response, function(key, value)
    {

      var cutOrigdate         = new Date(value.created_at);
      var year                = cutOrigdate.getFullYear();
      var month               = cutOrigdate.getMonth() + 1; // months are zero indexed
      var day                 = cutOrigdate.getDate();
      var hour                = cutOrigdate.getHours();
      var minute              = cutOrigdate.getMinutes();
      var second              = cutOrigdate.getSeconds();
      var hourFormatted       = hour % 12 || 12; // hour returned in 24 hour format
      var minuteFormatted     = minute < 10 ? "0" + minute : minute;
      var morning             = hour < 12 ? "am" : "pm";

      var formatedDate        = month + "/" + day + "/" + year + " " + hourFormatted + ":" +
      minuteFormatted + morning;

      if(value.sent_by == userLoggedID)
      {

        $("#convoMessages").append(''+
          '<li class="sent">'+
            '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
              '<div class="sent-msg">'+
              '<p>'+value.message+'</p>'+
              '<span class="text-muted pull-right"><small>'+formatedDate+'</small></span>'+
              '<div>'+
          '</li>'
        );
      }
      else
      {
        $("#convoMessages").append(''+
          '<li class="replies">'+
            '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
              '<div class="reply-msg">'+
              '<p>'+value.message+'</p>'+
              '<span class="text-muted"><small>'+formatedDate+'</small></span>'+
              '<div>'+
          '</li>'
        );
      }
    });
  }

  $("li.contact").click(function(){
      $("li.contact.active").removeClass("active");
      $(this).addClass('active');
    
    if ($('li.contact').hasClass('active')){
          
     $('.content').show();
  
      }
    else 
      {
        $('.content').hide();
      }
  });

}


// function readMessageResponse( response )
// {
//   $(".convo").html("");
//   $.each(response, function(key, value)

//   {
//   $(".convo").append(''+
//      '<div class="contact-profile">'+
//      '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
//      '<p>'+value.uname+' '+value.lname+'</p>'+
//    '</div>'+
//    '<div class="messages">'+
//      '<ul>'+
//        '<li class="sent">'+
//          '<img src="" alt="" />'+
//          '<p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>'+
//        '</li>'+
//        '<li class="replies">'+
//          '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
//          '<p>When youre backed against the wall, break the god damn thing down.</p>'+
//        '</li>'+
//      '</ul>'+
//    '</div>'
//     ); 
//   });
// }


//Create
$('#form-create-messagePM').on('submit', function (e) {
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var post_data = $('#form-create-messagePM').serialize();
    simPost(post_data, 'POST', '/maintenance/createMessage', createMessageResponse);
    e.preventDefault();
    return false;
});

function createMessageResponse( response )
{
  var recUser = $("input#sendToUser").val();
  simPost({recUser:recUser}, 'POST', '/maintenance/recUser', innerrecUserResponse);

  var fieldData = $("#valueTypeAhead").val();
  simPost({fieldData:fieldData}, 'POST', '../../maintenance/autocomplete', getLatestMessageUponSubmitResponse);

  function getLatestMessageUponSubmitResponse( response )
  { 

    $("#contactsInner").html("");
    $.each(response, function(key, value)
    {
      $("#contactsInner").append(''+
        '<li class="contact">'+
        '<div class="wrap">'+
          '<span class="contact-status busy"></span>'+
          '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
          '<div class="meta">'+
            '<p class="name">'+value.uname+' '+value.lname+'</p>'+
            '<p class="preview">You: '+value.msg+'</p>'+
        '</div>'+
        '</div>'+
      '</li>'
      );
    });
  }

  function innerrecUserResponse( response )
  { 
    var userLoggedID = $(".userLoggedID").val();

    $("#convoMessages").html("");
    $.each(response, function(key, value)
    {
    
      var cutOrigdate     = new Date(value.created_at);
      var year            = cutOrigdate.getFullYear();
      var month           = cutOrigdate.getMonth() + 1; // months are zero indexed
      var day             = cutOrigdate.getDate();
      var hour            = cutOrigdate.getHours();
      var minute          = cutOrigdate.getMinutes();
      var second          = cutOrigdate.getSeconds();
      var hourFormatted   = hour % 12 || 12; // hour returned in 24 hour format
      var minuteFormatted = minute < 10 ? "0" + minute : minute;
      var morning         = hour < 12 ? "am" : "pm";

      var formatedDate = month + "/" + day + "/" + year + " " + hourFormatted + ":" +
      minuteFormatted + morning;

      if(value.sent_by == userLoggedID)
      {
        $("#convoMessages").append(''+
          '<li class="sent">'+
            '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
            '<div class="sent-msg">'+
              '<p>'+value.message+'</p>'+
              '<span class="text-muted pull-right"><small>'+formatedDate+'</small></span>'+
              '</div>'+
          '</li>'
        );
      }
      else
      {
        $("#convoMessages").append(''+
          '<li class="replies">'+
            '<img src="/storage/student/'+value.profilepic+'" alt="" />'+
             '<div class="reply-msg">'+
              '<p>'+value.message+'</p>'+
              '<span class="text-muted"><small>'+formatedDate+'</small></span>'+
              '</div>'+
          '</li>'
        );
      }
    });
  }
}