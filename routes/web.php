<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@websitehome')->name('/');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about/company', 'AboutController@company');
Route::get('/about/history', 'AboutController@history');
Route::get('/about/vision-mission-quality-policy', 'AboutController@vision');

//projects
Route::get('/project', 'ProjectController@websiteindex');
// Route::get('/project/project/{data}', 'ProjectController@websiteindex');

//career
Route::get('/careers', 'CareerController@websiteCareer');
Route::get('/careers/job-description', 'CareerController@jobdesc');


//contact
Route::get('/contact', 'ContactController@websiteContact');
Route::post('/contact/inquiry', 'ContactController@inquiry');

//brokers corner
Route::get('/broker', 'CornerController@broker');


//privacy & terms //

Route::get('/privacypolicy', function () {
    return view('policy');
});

Route::get('/termsofservice', function () {
    return view('terms');
});
Auth::routes(); 

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function(){

	Route::get('/dashboard', 'HomeController@dashboard');
	Route::get('/dashboard/home', 'HomeController@index');
	Route::get('/dashboard/project', 'ProjectController@index');
	Route::get('/dashboard/model_unit', 'ProjectController@modelindex');
	Route::get('/dashboard/about', 'AboutController@index');
	Route::get('/dashboard/vision-mission', 'AboutController@visionindex');
	Route::get('dashboard/news', 'NewsController@index');
	Route::get('dashboard/events', 'EventsController@index');
	Route::get('dashboard/chart', 'ChartController@index');
	Route::get('/dashboard/company_details', 'CompanyDetailsController@index');
	Route::get('/dashboard/careers', 'CareerController@index');
	Route::get('/dashboard/faq', 'FAQController@index');
	Route::get('/dashboard/contact', 'ContactController@index');
	Route::get('/dashboard/buyer', 'BuyerController@index');
	Route::get('/dashboard/brokers', 'BrokerController@index');
	Route::get('/dashboard/amenities', 'BuyerController@index');
	Route::get('/dashboard/account-setting', 'AccountController@index');
});

// Dashboard - Partners
Route::post('/dashboard/createLogo', 'HomeController@createLogo');
Route::get('/dashboard/readLogo', 'HomeController@readLogo');
Route::post('/dashboard/deleteLogo', 'HomeController@deleteLogo');
Route::post('/dashboard/updateLogo', 'HomeController@updateLogo');
Route::post('/dashboard/getLogo', 'HomeController@getLogo');
Route::post('/fetch/LogoInfo', 'HomeController@LogoInfo');


// Dashboard - Home Summary
Route::post('/dashboard/createCompanySummary', 'HomeController@createCompanySummary');
Route::get('/dashboard/readCompanySummary', 'HomeController@readCompanySummary');
Route::post('/dashboard/updateCompanySummary', 'HomeController@updateCompanySummary');
Route::post('/dashboard/getCompanySummary', 'HomeController@getCompanySummary');
Route::post('/fetch/homeSummaryInfo', 'HomeController@companySummaryBannerInfo');

// Dashboard - Model Unit
Route::post('/dashboard/createModelUnit', 'ProjectController@createModelUnit');
Route::get('/dashboard/readModelUnit', 'ProjectController@readModelUnit');
Route::post('/dashboard/updateModelUnit', 'ProjectController@updateModelUnit');
Route::post('/dashboard/getModelUnit', 'ProjectController@getModelUnit');
Route::post('/dashboard/deleteModelUnit', 'ProjectController@deleteModelUnit');
Route::post('/fetch/modelUnitInfo', 'ProjectController@modelUnitInfo');


// Dashboard - Project
Route::post('/dashboard/deleteProject', 'ProjectController@deleteProject');
Route::post('/dashboard/deleteFeature', 'ProjectController@deleteFeature');
Route::post('/dashboard/createProject', 'ProjectController@createProject');
Route::get('/dashboard/readProject', 'ProjectController@readProject');
Route::post('/dashboard/updateProject', 'ProjectController@updateProject');
Route::post('/dashboard/getProject', 'ProjectController@getProject');
Route::post('/fetch/projectInfo', 'ProjectController@projectInfo');
Route::get('/project/{data}', 'ProjectController@websiteindex');
Route::get('/updateProjectPage/{id}', 'ProjectController@updateProjectPage');
Route::post('/getProjectDataForEditing', 'ProjectController@getProjectDataForEditing');
Route::post('/dashboard/deleteBannerImage', 'ProjectController@deleteBannerImage');


//Dashboard - About
Route::get('/about', 'AboutController@websiteAbout');
Route::post('/dashboard/createAboutBanner', 'AboutController@createAboutBanner');
Route::post('/dashboard/deleteAbout', 'AboutController@deleteAbout');
Route::post('/dashboard/updateAbout', 'AboutController@updateAbout');
Route::post('/dashboard/getAbout', 'AboutController@getAbout');
Route::post('/fetch/aboutBannerInfo', 'AboutController@aboutBannerInfo');


//Dashboard - Vision and Mission
Route::post('/dashboard/getVision', 'AboutController@getVision');
Route::post('/fetch/visionInfo', 'AboutController@visionInfo');
Route::post('/dashboard/updateVision', 'AboutController@updateVision');


//Dashboard - History
Route::post('/dashboard/createHistory', 'AboutController@createHistory');
Route::get('/dashboard/readHistory', 'AboutController@readHistory');
Route::post('/dashboard/updateHistory', 'AboutController@updateHistory');
Route::post('/dashboard/getHistory', 'AboutController@getHistory');
Route::post('/fetch/HistoryInfo', 'AboutController@HistoryInfo');


// Dashboard - News
Route::post('/dashboard/deleteNews', 'NewsController@deleteNews');
Route::get('/news/{data}', 'NewsController@websitenews');
Route::post('/dashboard/createNews', 'NewsController@createNews');
Route::get('/dashboard/readNews', 'NewsController@readNews');
Route::post('/dashboard/updateNews', 'NewsController@updateNews');
Route::post('/dashboard/getNews', 'NewsController@getNews');
Route::post('/fetch/NewsInfo', 'NewsController@NewsInfo');

// Dashboard - Events
Route::post('/dashboard/deleteEvent', 'EventsController@deleteEvent');
Route::get('event/{data}', 'EventsController@websiteEvent');
Route::post('/dashboard/createEvent', 'EventsController@createEvent');
Route::get('/dashboard/readEvent', 'EventsController@readEvent');
Route::post('/dashboard/updateEvent', 'EventsController@updateEvent');
Route::post('/dashboard/getEvent', 'EventsController@getEvent');
Route::post('/fetch/EventInfo', 'EventsController@EventInfo');

// Dashboard - Chart
Route::post('/dashboard/deleteChart', 'ChartController@deleteOrgChart');
Route::post('/dashboard/createChart', 'ChartController@createChart');
Route::get('/dashboard/readChart', 'ChartController@readChart');
Route::post('/dashboard/updateChart', 'ChartController@updateChart');
Route::post('/dashboard/getChart', 'ChartController@getChart');
Route::post('/fetch/ChartInfo', 'ChartController@ChartInfo');


// Dashboard - Contact
Route::get('/contact', 'ContactController@websiteContact');
Route::post('/inquiry', 'ContactController@createInquiry');
Route::post('/dashboard/createContactBanner', 'ContactController@createContactBanner');
Route::get('/dashboard/readContact', 'ContactController@readContact');
Route::post('/dashboard/updateContact', 'ContactController@updateContact');
Route::post('/dashboard/getContact', 'ContactController@getContact');
Route::post('/fetch/ContactBannerInfo', 'ContactController@ContactBannerInfo');
Route::post('/dashboard/deleteContactBanner', 'ContactController@deleteContactBanner');

//Company Details
Route::post('/dashboard/getCompanyDetails', 'CompanyDetailsController@getCompanyDetails');
Route::post('/fetch/companyDetailsInfo', 'CompanyDetailsController@companyDetailsInfo');
Route::post('/dashboard/updateCompanyDetails', 'CompanyDetailsController@updateCompanyDetails');

// Dashboard - Career Banner
Route::get('/dashboard/career', 'CareerController@websiteCareer'); 
Route::post('/dashboard/createCareerBanner', 'CareerController@createCareerBanner');
Route::get('/dashboard/readCareerBanner', 'CareerController@readCareerBanner');
Route::post('/dashboard/deleteCareerBanner', 'CareerController@deleteCareerBanner');
Route::post('/dashboard/updateCareerBanner', 'CareerController@updateCareerBanner');
Route::post('/dashboard/getCareer', 'CareerController@getCareerBanner');
Route::post('/fetch/careerBannerInfo', 'CareerController@CareerBannerInfo');


// Dashboard - Career
Route::get('/career', 'CareerController@websiteCareer');
Route::post('/dashboard/deleteCareer', 'CareerController@deleteCareer');
Route::post('/dashboard/createCareer', 'CareerController@createCareer');
Route::get('/dashboard/readCareer', 'CareerController@readCareer');
Route::post('/dashboard/updateCareer', 'CareerController@updateCareer');
Route::post('/dashboard/getCareer', 'CareerController@getCareer');
Route::post('/fetch/CareerInfo', 'CareerController@CareerInfo');


//Messaging
Route::get('/messaging', 'MessagingController@index');
Route::get('/maintenance/contactInitialList', 'MessagingController@contactInitialList');
Route::post('/maintenance/autocomplete', 'MessagingController@search');
Route::post('/maintenance/recUser', 'MessagingController@recUser');
Route::post('/maintenance/createMessage', 'MessagingController@createMessage');


//Career - Job Detail
Route::get('/jobdetails/{data}', 'CareerController@jobDetail');


//Buyer
Route::get('/buyers-corner', 'BuyerController@websiteBuyer');
Route::post('/dashboard/deleteBuyerbanner', 'BuyerController@deleteBuyerbanner');
Route::post('/dashboard/createBuyerBanner', 'BuyerController@createBuyerBanner');
Route::get('/dashboard/readBuyerBanner', 'BuyerController@readBuyerBanner');
Route::post('/dashboard/updateBuyerBanner', 'BuyerController@updateBuyerBanner');
Route::post('/dashboard/getBuyerBanner', 'BuyerController@getBuyerBanner');
Route::post('/fetch/buyerBannerInfo', 'BuyerController@buyerBannerInfo');


// Dashboard - Buyer Content

Route::post('/dashboard/updateBuyerContent', 'BuyerController@updateBuyerContent');
Route::post('/dashboard/getBuyerContent', 'BuyerController@getBuyerContent');
Route::post('/fetch/buyerContentInfo', 'BuyerController@buyerContentInfo');
Route::post('/dashboard/registerBuyer', 'BuyerController@registerBuyer');
Route::get('/dashboard/readProjectFromTracker', 'BuyerController@readProjectFromTracker');
Route::post('/readHouseUnitFromTracker', 'BuyerController@readHouseUnitFromTracker');
Route::post('/readStageFromTracker', 'BuyerController@readStageFromTracker');

//Broker
Route::get('/brokers-corner', 'BrokerController@websiteBroker');
Route::post('/dashboard/deleteBrokerbanner', 'BrokerController@deleteBrokerbanner');
Route::post('/dashboard/createBrokerBanner', 'BrokerController@createBrokerBanner');
Route::get('/dashboard/readBrokerBanner', 'BrokerController@readBrokerBanner');
Route::post('/dashboard/updateBrokerBanner', 'BrokerController@updateBrokerBanner');
Route::post('/dashboard/getBrokerBanner', 'BrokerController@getBrokerBanner');
Route::post('/fetch/brokerBannerInfo', 'BrokerController@brokerBannerInfo');

Route::post('/brokers/createBroker', 'BrokerController@createBroker');
Route::get('/brokers/readBrokers', 'BrokerController@readBrokers');


///FAQ
Route::get('faq', 'FAQController@websiteCareer');
Route::post('/dashboard/deleteFAQ', 'FAQController@deleteFAQ');
Route::post('/dashboard/createFAQ', 'FAQController@createFAQ');
Route::get('/dashboard/readFAQ', 'FAQController@readFAQ');
Route::post('/dashboard/updateFAQ', 'FAQController@updateFAQ');
Route::post('/dashboard/getFAQ', 'FAQController@getFAQ');
Route::post('/fetch/FAQInfo', 'FAQController@FAQInfo');

///Amenities
Route::post('/dashboard/deleteAmenitiesImage', 'AmenitiesController@deleteAmenitiesImage');
Route::post('/dashboard/deleteAmenities', 'AmenitiesController@deleteAmenities');
Route::get('/dashboard/amenities', 'AmenitiesController@index');
Route::post('/dashboard/createAmenities', 'AmenitiesController@createAmenities');
Route::get('/dashboard/readAmenities', 'AmenitiesController@readAmenities');
Route::post('/dashboard/updateAmenities', 'AmenitiesController@updateAmenities');
Route::post('/dashboard/getAmenities', 'AmenitiesController@getAmenities');
Route::post('/fetch/AmenitiesInfo', 'AmenitiesController@AmenitiesInfo');



//  Login/ Register

Route::get('/register', 'RegisterController@createWebsiteAccount');
Route::post('register', 'RegisterController@storeWebsiteAccount');
Auth::routes();