<?php

namespace App;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

        public static function createInquiry( $post_data )
    {
        $result = DB::table('matvis_gland.inquiry')
            ->insert(
                array(
                    'id'        => $post_data['id'],
                    'name'      => $post_data['name'],
                    'email'     => $post_data['email'],
                    'mobile'   => $post_data['mobile'],
                    'message'   => $post_data['message'],
                    'created_at'=> date("Y-m-d H:i:s"),
                    'status'   => 'unread'
                )
            );

        return $result;
    }

    public static function readContact()
    {

        $result = DB::table('contact_banner as c')
            ->select(
                'c.id as id',
                'c.display_filename as banner',
                'c.title as title',
                'c.subtitle as subtitle'
            )

            ->get();
        return $result;
    }

    public static function createContactBanner( $post_data )
    {
        if(isset($post_data['display_filename'])){
            return self::saveContactBanner($post_data);
        }else{
            return 'error';
        }

    }


    private static function saveContactBanner($post_data)
    {
        $result = DB::transaction(function() use($post_data){
            $file = $post_data['display_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            $title    = $post_data['title'];
            $subtitle = $post_data['subtitle'];
            DB::table('contact_banner')
                ->insert(
                    array(
                        'display_filename' => $filename,
                        'title'            => $title,
                        'subtitle'         => $subtitle
                    )
                    
                );
        });
        return $result == null ? 'success' : 'error';
    }
   

    public static function ContactBannerInfo( $post_data )
    {
       $result = DB::table('contact_banner')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

       public static function deleteContactBanner( $post_data )
    {
        $result = DB::table('contact_banner')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }

    
    public static function getContact( $post_data )
    {
        $result = DB::table('contact_banner as c')
            ->select(
                'c.id as id',
                'c.display_filename as banner',
                'c.title as title',
                'c.subtitle as subtitle'
            )
            ->get();
        return $result;
    }
    

        public static function updateContact( $post_data )
    {
        $title      = $post_data['title'];
        $subtitle    = $post_data['subtitle'];
        if(isset($post_data['display_filename'])){
            $file = $post_data['display_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            DB::table('contact_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'display_filename'  => $filename,
                        'title'             => $title,
                        'subtitle'          => $subtitle,
                        'updated_at'        => date("Y-m-d H:i:s")
                    )
                );
        }else{
            DB::table('contact_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'title'           => $title,
                        'subtitle'        => $subtitle,
                        'updated_at'      => date("Y-m-d H:i:s")
                    )
                );
            }
    }
}