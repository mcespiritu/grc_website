<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    public static function readAbout()
    {

        $result = DB::table('about_banner as a')
            ->select(
                'a.id as id',
                'a.display_filename as banner',
                'a.about_title as about_title',
                'a.about_subtitle as about_subtitle'
            )

            ->get();
        return $result;
    }

    public static function createAbout( $post_data )
    {
        if(isset($post_data['display_filename'])){
            return self::saveAboutBanner($post_data);
        }else{
            return 'error';
        }

    }


    private static function saveAboutBanner($post_data)
    {
        $result = DB::transaction(function() use($post_data){
            $file = $post_data['display_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            $about_title    = $post_data['about_title'];
            $about_subtitle = $post_data['about_subtitle'];
            DB::table('about_banner')
                ->insert(
                    array(
                        'display_filename' => $filename,
                        'about_title'      => $about_title,
                        'about_subtitle'   => $about_subtitle,
                        //'updated_by'     => Auth::user()->id
                    )
                    
                );
        });
        return $result == null ? 'success' : 'error';
    }
   

    public static function aboutBannerInfo( $post_data )
    {
       $result = DB::table('about_banner')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

   
    
    public static function getAbout( $post_data )
    {
        $result = DB::table('about_banner as a')
            ->select(
                'a.id as id',
                'a.banner_filename as banner',
                'a.about_title as about_title',
                'a.about_subtitle as about_subtitle'
            )
            ->get();

        return $result;
    }

        public static function deleteAbout( $post_data )
    {
        $result = DB::table('about_banner')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
    

        public static function updateAbout( $post_data )
    {
        $about_title      = $post_data['about_title'];
        $about_subtitle    = $post_data['about_subtitle'];
        if(isset($post_data['display_filename'])){
            $file = $post_data['display_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            DB::table('about_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'display_filename'  => $filename,
                        'about_title'       => $about_title,
                        'about_subtitle'    => $about_subtitle,
                        'updated_at'        => date("Y-m-d H:i:s")
                    )
                );
        }else{
            DB::table('about_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'about_title'       => $about_title,
                        'about_subtitle'    => $about_subtitle,
                        'updated_at'        => date("Y-m-d H:i:s")
                    )
                );
            }
    }


    // Vision & Mision

        public static function readVision()
    {

        $result = DB::table('vision_mission as a')
            ->select(
                'a.id as id',
                'a.vision as vision',
                'a.mission as mission',
                'a.quality_policy as quality_policy'
            )

            ->get();
        return $result;
    }

        public static function getVision( $post_data )
    {
        $result = DB::table('vision_mission as a')
            ->select(
                'a.id as id',
                'a.vision as vision',
                'a.mission as mission',
                'a.quality_policy as quality_policy'
            )
            ->get();

        return $result;
    }
    
    public static function visionInfo( $post_data )
    {
       $result = DB::table('vision_mission')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }
    public static function updateVision( $post_data )
    {
        $result = DB::table('vision_mission')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'vision'        => $post_data['vision'],
                    'mission'       => $post_data['mission'],
                    'quality_policy'=> $post_data['quality_policy'],
                    'updated_at'    => date("Y-m-d H:i:s")
                )
            );
        }


//History

public static function readHistory()
    {
        $result = DB::table('company_history as ch')
            ->select('*')
            ->get();
        return $result;
    }

    public static function createHistory( $post_data )
    {
        $result = DB::table('company_history')
            ->insert(
                array(
                    'date'          => $post_data['date'],
                    'content'       => $post_data['content']
                )
            );
         return $result;
    }
    public static function HistoryInfo( $post_data )
    {
       $result = DB::table('company_history')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }
    public static function getHistory($post_data)
    {
        $result = DB::table('company_history')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->orderBy('date', 'desc')
            ->get();
        return $result;
    }

    public static function updateHistory( $post_data )
    {
        $result = DB::table('company_history')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'date'       => $post_data['date'],
                    'content'    => $post_data['content']
                )
            );
    }
}