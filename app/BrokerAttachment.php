<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerAttachment extends Model
{
    protected $table = 'broker_attachments';

    protected $fillable = [
    	'broker_id',
    	'filename',
    	'uploaded_by'
    ];
}
