<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public static function getPrimaryDetails( $id )
    {
        $result = DB::table('projects as p')
            ->select(
                '*',
                'p.id as id'
            )
            ->where('p.id', '=', $id)
      
            ->get();
        return $result;
    }

        public static function readProjectFromTracker()
    {

        $result = DB::table('matvis_gland.project as p')
            ->select(
                'p.id as id',
                'p.project_name as project_name',
                'p.project_code as project_code'
            )
            ->get();
        return $result;
    }

        public static function readHouseUnitFromTracker()
    {

        $result = DB::table('matvis_gland.bill_of_material_parent as bomp')
            ->select(
                'bomp.id as id',
                'bomp.project_id as project_id',
                'bomp.road_or_lot as road_or_lot',
                'lt.lot as lot'
            )
            ->leftJoin('matvis_gland.project as p', 'p.id', '=', 'bomp.project_id')
            ->leftJoin('matvis_gland.lot as lt', 'lt.id', '=', 'bomp.road_or_lot')
            ->get();
        return $result;
    }

            public static function readStageFromTracker()
    {

        $result = DB::table('matvis_gland.stages_per_model as spm')
            ->select(
                'spm.id as id',
                'spm.stage_id as stage_id',
                'sv.stage as stage'
            )
            ->leftJoin('matvis_gland.stages_vars as sv', 'sv.id', '=', 'spm.stage_id')
            ->get();
        return $result;
    }

        public static function readFeature()
    {

        $result = DB::table('project_features as p')
            ->select(
                'p.id as id',
                'p.value as value',
                'p.feature as feature',
                'am.name as name'
            )
            ->leftJoin('amenities_name as am', 'am.id', '=', 'p.feature')
            ->leftJoin('matvis_grc.project as gmi', 'gmi.id', '=', 'p.model_id')
            ->where('model_id', '=', 'gmi.id')
            ->get();
        return $result;
    }

        public static function readProjectMatvis()
    {

        $result = DB::table('matvis_grc.project as p')
            ->select(
                'p.id as id',
                'p.project_name as project_name'
            )
            ->get();
        return $result;
    }

    public static function readProject()
    {

        $result = DB::table('projects as p')
            ->select(
                'p.id as id',
                'p.display_filename as banner',
                'p.featured as featured',
                'p.status as status',
                'p.project_name as project_name',
                'p.project_summary as project_summary',
                'p.project_intro as project_intro',
                'p.project_location as project_location',
                'p.vicinity_map as vicinity_map'
        
            )
            ->get()
            ->map(function($projects){
                $projects->{'project_url'} = str_replace(' ', '-', strtolower($projects->project_name));
                return $projects;
            });
        return $result;
    }

    public static function readPerProject($url)
    {
        $result = DB::table('projects as p')
            ->select(
                'p.id as id',
                'p.display_filename as banner',
                'p.featured as featured',
                'p.status as status',
                'p.project_name as project_name',
                'p.project_summary as project_summary',
                'p.project_intro as project_intro',
                'p.project_location as project_location',
                'p.walkthrough as walkthrough',
                'p.vicinity_map as vicinity_map'

            )
            ->get()
            ->map(function($projects){
                $projects->{'project_url'} = str_replace(' ', '-', strtolower($projects->project_name));
                return $projects;
            })
            ->filter(function($projects) use($url){
                return $projects->project_url == $url;
            });
        return $result;
    }
        public static function deleteProject( $post_data )
    {
        $result = DB::table('projects')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }

            public static function deleteBannerImage( $post_data )
    {
        $result = DB::table('project_banners')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
    
        public static function getProjectSlider($id)
    {
        $result = DB::table('project_banners')
            ->select('*')
            ->where('project_id', '=', $id)
            ->get();
        return $result;
    }
    public static function getProjectAmenities($id)
    {
        $result = DB::table('amenities')
            ->select('*')
            ->where('project_id', '=', $id)
            ->get();
        return $result;
    }

    public static function getProjectFeatures($id)
    {
        $result = DB::table('project_features')
            ->select('*')
            ->where('model_id', '=', $id)
            ->get();
        return $result;
    }

    public static function createProject( $post_data )
    {
        
        $name               = $post_data['project_name'];
        $featured           = $post_data['featured'];
        $summary            = $post_data['project_summary'];
        $intro              = $post_data['project_intro'];
        $location           = $post_data['project_location'];
        $display_filename   = $post_data['display_filename'];
        $vicinity_map       = $post_data['vicinity_map'];
        $status             = $post_data['status'];
       

        $result = DB::table('projects')
            ->insert(
                array(
                    'featured'          => $featured,
                    'status'            => $status,
                    'project_name'      => $name,
                    'project_summary'   => $summary,
                    'project_intro'     => $intro,
                    'project_location'  => $location,
                    'updated_at'        => date("Y-m-d H:i:s")
                )
                
            );
        

        $projectID = DB::getPdo()->lastInsertId();


        if(isset($post_data['display_filename'])){
            self::saveProjectBanner($post_data, $projectID);
        }
        if(isset($post_data['display_filename'])){
            self::saveProjectSlider($post_data, $projectID);
        }

         if(isset($post_data['vicinity_map'])){
            self::savePVicinityMap($post_data, $projectID);
        }
        
        foreach ($post_data['feature_value'] as $key => $value) {
            DB::table('project_features')
            ->insert(
                array(
                    'project_id' => $projectID,
                    'feature'    => $post_data['feature_id'][$key],
                    'value'      => $post_data['feature_value'][$key]
                )
            );
        }
        return $result;
    }


    private static function saveFeatures($post_data, $projectID)
    {
        foreach ($post_data['feature_value'] as $key => $value) {
            DB::table('project_features')
            ->insert(
                array(
                    'project_id' => $projectID,
                    'feature'    => $post_data['feature_id'],
                    'value'      => $post_data['feature_value']
                )
            );
        }
    }

       private static function saveProjectSlider($post_data, $projectID)
    {
        $file = $post_data['banner'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        DB::table('project_bannerss')
            ->where('id', '=', $projectID)
            ->update(
                array(
                    'banner'      => $filename
                )
            );
    }

    private static function saveProjectBanner($post_data, $projectID)
    {
        $file = $post_data['display_filename'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        DB::table('projects')
            ->where('id', '=', $projectID)
            ->update(
                array(
                    'display_filename'      => $filename
                )
            );
    }

    private static function savePVicinityMap($post_data, $projectID)
    {
        $vicinity_map       = $post_data['vicinity_map'];
        $path = '/storage/imageGallery/';
        $extension = $vicinity_map->getClientOriginalExtension();
        $map = $vicinity_map->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $map, \File::get($vicinity_map));
        DB::table('projects')
            ->where('id', '=', $projectID)
            ->update(
                array(
                    'vicinity_map'      => $map
                )
            );
    }


    public static function createProjectAmmenities( $post_data )
    {
        if(isset($post_data['amenities_image'])){
            self::saveAmmentiesPicture($post_data);
        }
    }

    private static function saveAmmentiesPicture($post_data)
    {       
        $getLatestProjectInserted = DB::table('projects')
            ->select('id')
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->get();

        $projectID = $getLatestProjectInserted[0]->id;
        $file = $post_data['amenities_image'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        DB::table('amenities')
            ->insert(
                array(
                    'project_id'            => $projectID,
                    'amenities_image'       => $filename
                )
            );
    }


    public static function projectInfo( $post_data )
    {
       $result = DB::table('projects')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

   
    
    public static function getProject( $post_data )
    {
        $result = DB::table('projects as p')
            ->select(
                'p.id as id',
                'p.banner_filename as banner',
                'p.featured as featured',
                'p.status as status',
                'p.project_name as project_name',
                'p.project_summary as project_summary',
                'p.project_intro as project_intro',
                'p.project_location as project_location',
                'p.vicinity_map as vicinity_map'
            )
            ->get();    

        return $result;
    }
    

        public static function deleteFeature( $post_data )
    {
        $result = DB::table('project_features')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }

    public static function updateProjectFeatures( $post_data )
    {
        DB::table('project_features')
        ->where('id', '=', $post_data['id'])
        ->update([
            'feature'    => $post_data['feature_id'],
            'value'      => $post_data['feature_value']
        ]);
    }

        public static function updateProject( $post_data )
    {
        $not_allowed = ['',null];
        DB::table('projects')
        ->where('id', '=', $post_data['id'])
        ->update([
            'featured'          => $post_data['featured'],
            'status'            => $post_data['status'],
            'project_name'      => $post_data['project_name'],
            'project_summary'   => $post_data['project_summary'],
            'project_intro'     => $post_data['project_intro'],
            'project_location'  => $post_data['project_location'],
            'walkthrough'       => $post_data['walkthrough'],
            'updated_at'        => date("Y-m-d H:i:s")
        ]);
        if(isset($post_data['display_filename']) && !in_array($post_data['display_filename'], $not_allowed)){
            $file = $post_data['display_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            DB::table('projects')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                    'featured'           => $post_data['featured'],
                    'status'             => $post_data['status'],
                    'project_name'       => $post_data['project_name'],
                    'project_summary'    => $post_data['project_summary'],
                    'display_filename'   => $filename,
                    'project_intro'      => $post_data['project_intro'],
                    'project_location'   => $post_data['project_location'],
                    'walkthrough'        => $post_data['walkthrough'],
                    'updated_at'         => date("Y-m-d H:i:s")
                    )
                );
        }

        if( isset($post_data['vicinity_map']) && !in_array($post_data['vicinity_map'], $not_allowed)){
            $file = $post_data['vicinity_map'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            DB::table('projects')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                    'featured'           => $post_data['featured'],
                    'status'             => $post_data['status'],
                    'project_name'       => $post_data['project_name'],
                    'project_summary'    => $post_data['project_summary'],
                    'vicinity_map'       => $filename,
                    'project_intro'      => $post_data['project_intro'],
                    'project_location'   => $post_data['project_location'],
                    'walkthrough'        => $post_data['walkthrough'],
                    'updated_at'         => date("Y-m-d H:i:s")
                    )
                );
        }

        if( isset($post_data['amenities_image']) && !in_array($post_data['amenities_image'], $not_allowed)){
            $file = $post_data['amenities_image'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            $fileisExist =  DB::table('amenities')
                ->where('amenities_image',$filename)
                ->where('project_id',$post_data['id'])
                ->count();
                if($fileisExist == 0){
                    DB::table('amenities')
                        ->insert([
                            'project_id'       => $post_data['id'],
                            'amenities_image'  => $filename
                        ]);
                }else{
                    DB::table('amenities')
                        ->where('project_id', '=', $post_data['id'])
                        ->update(
                            array(
                              'amenities_image'   => $filename,
                                 'updated_at'        => date("Y-m-d H:i:s")
                               )
                           );
                       }
        }
                if( isset($post_data['banner']) && !in_array($post_data['banner'], $not_allowed)){
            $file = $post_data['banner'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            $fileisExist =  DB::table('project_banners')
                ->where('banner',$filename)
                ->where('project_id',$post_data['id'])
                ->count();
                if($fileisExist == 0){
                    DB::table('project_banners')
                        ->insert([
                            'project_id'       => $post_data['id'],
                            'banner'  => $filename
                        ]);
                }else{
                    DB::table('project_banners')
                        ->where('project_id', '=', $post_data['id'])
                        ->update(
                            array(
                              'banner'   => $filename,
                                 'updated_at'        => date("Y-m-d H:i:s")
                               )
                           );
                       }
        }

    }





    public static function readAmenities()
    {

        $result = DB::table('amenities as a')
            ->select(
                'a.id as id',
                'a.project_id as project_id',
                'a.amenities_caption as amenities_caption',
                'a.amenities_image as amenities_image',
                'p.id as project_id'
            )
            ->leftJoin('projects as p', 'p.id', '=', 'a.project_id')
            ->get();
        return $result;
    }
        public static function getAmenities( $post_data )
    {
        $result = DB::table('amenities')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

    public static function getAm( $id )
    {
        $result = DB::table('amenities')
            ->where('project_id', '=', $id)
            ->get();
        return $result;
    }

        public static function updateAmenities( $post_data )
    {
        $result = DB::table('amenities')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'amenities_image'   => $post_data['amenities_image'],
                    'project_id'        => $project_id,
                    'amenities_caption' => $amenities_caption,
                    'updated_at'    => date("Y-m-d H:i:s")
                )
            );
        return $result;
    }
        public static function createModelUnit( $post_data )
    {
        $result = DB::table('project_model_unit')
            ->insert(
                array(
                    'project_id'    => $post_data['project_id'],
                    'model_name'    => $post_data['model_name'],
                    'floor_area'    => $post_data['floor_area'],
                    'lot_area'      => $post_data['lot_area']
                )
            );
        $modelID = DB::getPdo()->lastInsertId();

        if(isset($post_data['display_filename'])){
            return self::saveModelUnitBanner($post_data);
        }else{
            return 'error';
        }
        foreach ($post_data['feature_value'] as $key => $value) {
            DB::table('project_features')
            ->insert(
                array(
                    'model_id' => $modelID,
                    'feature'    => $post_data['feature_id'][$key],
                    'value'      => $post_data['feature_value'][$key]
                )
            );
        }


        return $result;
    }


       private static function saveModelUnitBanner($post_data, $projectID)
    {
        $file = $post_data['display_filename'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        DB::table('projects')
            ->where('id', '=', $projectID)
            ->update(
                array(
                    'display_filename'      => $filename
                )
            );
    }

        public static function modelUnitInfo( $post_data )
    {
       $result = DB::table('project_model_unit as pmu')
            ->select('pmu.id as id',
                    'pmu.project_id as project_id',
                    'pmu.floor_area as floor_area',
                    'pmu.lot_area as lot_area',
                    'pmu.model_name as model_name',
                    'pf.model_id as model_id',
                    'pf.feature as feature',
                    'pf.value as value')
            ->leftJoin('project_features as pf', 'pf.model_id', '=', 'pmu.id')
            ->where('pmu.id', '=', $post_data['id'])
            ->get();
                
        return $result;
    }

       public static function getModelUnit( $post_data )
    {
        $result = DB::table('project_model_unit as p')
            ->select(
                'p.id as id',
                'p.project_id as project_id',
                'p.floor_area as floor_area',
                'p.lot_area as lot_area'
            )
            ->get();    

        return $result;
    }

        public static function getModelUnitDetails( $id )
    {
        $result = DB::table('project_model_unit')
            ->select('*')
            ->where('project_id', '=', $id)
            ->get();
        return $result;
    }
    public static function readModelUnit()
    {

        $result = DB::table('project_model_unit as p')
            ->select(
                'p.id as id',
                'p.project_id as project_id',
                'p.model_name as model_name',
                'mgp.id as project_id'
            )
             ->leftJoin('matvis_gland.project as mgp', 'mgp.id', '=', 'p.id')
           
            ->get();
        return $result;
    }

        public static function readFeaturePerModel()
    {

        $result = DB::table('project_features as p')
            ->select(
                'p.id as id',
                'p.value as value',
                'p.model_id as model_id',
                'am.name as name',
                'pmu.id as model_id_pmu'

            )
            ->leftJoin('amenities_name as am', 'am.id', '=', 'p.feature')
            ->leftJoin('project_model_unit as pmu', 'pmu.id', '=', 'p.model_id')
            ->get();
    
        return $result;
    }
    
    public static function updateModelUnit( $post_data )
    {
        $result = DB::table('project_model_unit as p')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'project_id'   => $post_data['project_id'],
                    'model_name'   => $post_data['model_name'],
                    'floor_area'   => $post_data['floor_area'],
                    'lot_area'   => $post_data['lot_area']

                )
            );


        return $result;
    }

        public static function deleteModelUnit( $post_data )
    {
        $result = DB::table('project_model_unit')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
}