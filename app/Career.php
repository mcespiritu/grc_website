<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    public static function readCareer()
    {

        $result = DB::table('careers as c')
            ->select(
                'c.id as id',
                'c.job_title as job_title',
                'c.job_desc as job_desc',
                'c.employment_type as employment_type',
                'jc.categories as categories'
            )
            ->leftJoin('job_category as jc', 'c.id', '=', 'jc.id')
            ->get()
            ->map(function($career){
                $career->{'job_url'} = str_replace(' ', '-', strtolower($career->job_title));
                return $career;
            });

        return $result;
    }
    public static function readCareerJobdetails($url)
    {
        $career = Career::readCareer();
        $career = $career->filter(function($career) use($url){
            return $career->job_url == $url;
        });
        return $career;
    }
    public static function readPerCareers($url)
    {
        $result = DB::table('careers as c')
            ->select(
                'c.id as id',
                'c.job_title as job_title',
                'c.job_desc as job_desc',
                'c.employment_type as employment_type',
                'jc.categories as categories'
            )
            ->leftJoin('job_category as jc', 'c.id', '=', 'jc.id')
            ->get()
            ->map(function($career){
                $career->{'job_url'} = str_replace(' ', '-', strtolower($career->job_title));
                return $career;
            })
            ->filter(function($career) use($url){
                return $career->job_url == $url;
            });
        return $result;
    }

    public static function readCategory()
    {

        $result = DB::table('job_category')
            ->select('*')
            ->get();
        return $result;
    }


    public static function deleteCareer( $post_data )
    {
        $result = DB::table('careers')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }

    public static function createCareer( $post_data )
    {

        $result = DB::table('careers')
            ->insert(
                array(
                    'job_title'           => $post_data['job_title'],
                    'job_desc'            => $post_data['job_desc'],
                    'employment_type'     => $post_data['employment_type'],
                    'category_id'         => $post_data['categories']
                )
            );
         return $result;
    }

    public static function CareerInfo( $post_data )
    {
       $result = DB::table('careers')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

   
    
    public static function getCareer( $post_data )
    {
        $result = DB::table('careers as c')
            ->select('*')
            ->get();
        return $result;
    }
    public static function updateCareer( $post_data )
    {
        $result = DB::table('careers')
           ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'job_title'     => $post_data['job_title'],
                    'job_desc'      => $post_data['job_desc'],
                    'updated_at'    => date("Y-m-d h.i:s")
                )
            );
    }


    //Career Banner

    public static function readCareerBanner()
    {

        $result = DB::table('career_banner as cb')
            ->select(
                'cb.id as id',
                'cb.title as title',
                'cb.subtitle as subtitle',
                'cb.banner_filename as banner'
            )
            ->get();
        return $result;
    }
    public static function deleteCareerBanner( $post_data )
    {
        $result = DB::table('career_banner')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
    public static function createCareerBanner( $post_data )
    {

        if(isset($post_data['banner_filename'])){
            self::saveCareerBanner($post_data);
        }

    }

    private static function saveCareerBanner($post_data)
    {
        $file = $post_data['banner_filename'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        $title      = $post_data['title'];
        $subtitle   = $post_data['subtitle'];
        DB::table('career_banner')
            ->insert(
                array(
                    'banner_filename'   => $filename,
                    'title'             => $title,
                    'subtitle'          => $subtitle,
                    'updated_at'        => date("Y-m-d H:i:s")
                )
            );
    }

    public static function CareerBannerInfo( $post_data )
    {
       $result = DB::table('career_banner')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }
    public static function getCareerBanner( $post_data )
    {
        $result = DB::table('career_banner as cb')
            ->select(
                'cb.id as id',
                'cb.title as title',
                'cb.subtitle as subtitle',
                'cb.banner_filename as banner'
            )
            ->get();

        return $result;
    }
    public static function updateCareerBanner( $post_data )
    {


        $title      = $post_data['title'];
        $subtitle    = $post_data['subtitle'];
        if(isset($post_data['banner_filename'])){
            $file = $post_data['banner_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            DB::table('career_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'banner_filename'  => $filename,
                        'title'             => $title,
                        'subtitle'          => $subtitle,
                        'updated_at'        => date("Y-m-d H:i:s")
                    )
                );
        }else{
            DB::table('career_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'title'           => $title,
                        'subtitle'        => $subtitle,
                        'updated_at'      => date("Y-m-d H:i:s")
                    )
                );
            }



    }

}