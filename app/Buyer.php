<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    public static function readBuyerBanner()
    {

        $result = DB::table('buyer_banner as b')
            ->select(
                'b.id as id',
                'b.display_filename as banner',
                'b.buyer_title as buyer_title',
                'b.buyer_subtitle as buyer_subtitle'
            )

            ->get();
        return $result;
    }

    public static function createBuyerBanner( $post_data )
    {
        if(isset($post_data['display_filename'])){
            return self::saveBuyerBanner($post_data);
        }else{
            return 'error';
        }

    }

    private static function saveBuyerBanner($post_data)
    {
        $result = DB::transaction(function() use($post_data){
            $file = $post_data['display_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            $buyer_title    = $post_data['buyer_title'];
            $buyer_subtitle = $post_data['buyer_subtitle'];
            DB::table('buyer_banner')
                ->insert(
                    array(
                        'display_filename' => $filename,
                        'buyer_title'      => $buyer_title,
                        'buyer_subtitle'   => $buyer_subtitle,
                        //'updated_by'     => Auth::user()->id
                    )
                );
        });
        return $result == null ? 'success' : 'error';
    }
   

    public static function BuyerBannerInfo( $post_data )
    {
       $result = DB::table('buyer_banner')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

   
    
    public static function getBuyerBanner( $post_data )
    {
        $result = DB::table('buyer_banner as b')
            ->select(
                'b.id as id',
                'b.banner_filename as banner',
                'b.buyer_title as buyer_title',
                'b.buyer_subtitle as buyer_subtitle'
            )
            ->get();
        return $result;
    }

        public static function deleteBuyerBanner( $post_data )
    {
        $result = DB::table('buyer_banner')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
    

        public static function updateBuyerBanner( $post_data )
    {
        $buyer_title      = $post_data['buyer_title'];
        $buyer_subtitle    = $post_data['buyer_subtitle'];
        if(isset($post_data['display_filename'])){
            $file = $post_data['display_filename'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            DB::table('buyer_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'display_filename'  => $filename,
                        'buyer_title'       => $buyer_title,
                        'buyer_subtitle'    => $buyer_subtitle,
                        'updated_at'        => date("Y-m-d H:i:s")
                    )
                );
        }else{
            DB::table('buyer_banner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'buyer_title'       => $buyer_title,
                        'buyer_subtitle'    => $buyer_subtitle,
                        'updated_at'        => date("Y-m-d H:i:s")
                    )
                );
            }
    }
     // Buyer Content

        public static function readBuyerContent()
    {
        $result = DB::table('buyer_content as bc')
            ->select('*')
            ->get();
        return $result;
    }

    public static function getBuyerContent( $post_data )
    {
      $result = DB::table('buyer_content as bc')
            ->select(
                'bc.id as id',
                'bc.content as content'
            )
        ->get();
      return $result;

    }

    public static function buyerContentInfo( $post_data )
    {
       $result = DB::table('buyer_content')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

    public static function updateBuyerContent( $post_data )
    {
        $result = DB::table('buyer_content')
           ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'content'      => $post_data['content'],
                    'updated_at'   => date("Y-m-d h.i:s")
                )
            );
    }

    // Register Buyer
    public static function registerBuyer($post_data)
    {
        $result = DB::table('matvis_gland.registered_buyer')
            ->insert(
                array(
                    'first_name'   => $post_data['first_name'],
                    'middle_name'  => $post_data['middle_name'],
                    'last_name'    => $post_data['last_name'],
                    'address'      => $post_data['address'],
                    'broker_id'    => $post_data['broker_id'],
                    'project'      => $post_data['project'],
                    'unit'          => $post_data['unit'],
                    'created_at'   => date("Y-m-d h.i:s")

                )
            );
        return $result;
    }

public static function readProjectFromTracker()
    {

        $result = DB::table('matvis_gland.project as p')
            ->select(
                'p.id as id',
                'p.project_name as project_name',
                'p.project_code as project_code'
            )
            ->get();
        return $result;
    }

        public static function readHouseUnitFromTracker( $post_data )
    {
        $result = DB::table('matvis_gland.bill_of_material_parent as bomp')
            ->select(
            'bomp.id as id',
            'bomp.lot_area',
            'bomp.lot_cost_sqm',
            'bomp.lot_price',
            'bomp.house_price',
            'bomp.tcp',
            'bomp.loan_amount as loan',
            'bomp.loan_amount',
            'bomp.house_lot_total',
            'bomp.process',
            'bomp.reservation',
            'bomp.total_cash_out as equity',
            'prj.project_code as project_code',
            'm.model as model',
            'mt.model_type as model_type',
            'l.lot as lot',
            'bl.block as block'
          )
          ->where('bomp.status', '!=', 3)
          ->where('bomp.project_id', '=', $post_data['id'])
          ->where('bomp.reserved', '=', 'No')
          ->leftJoin('matvis_gland.project as prj', 'prj.id', '=', 'bomp.project_id')
          ->leftJoin('matvis_gland.models as m', 'm.id', '=', 'bomp.project_model_type_id')
          ->leftJoin('matvis_gland.lot as l', 'l.id', '=', 'bomp.road_or_lot')
          ->leftJoin('matvis_gland.block as bl', 'bl.id', 'l.block_id')
          ->leftJoin('matvis_gland.model_type as mt', 'mt.id', '=', 'm.model_type')
          ->leftJoin('matvis_gland.types as ty', 'ty.id', '=', 'bomp.development_type_id')
          ->get();
        return $result;
   
    }

            


}