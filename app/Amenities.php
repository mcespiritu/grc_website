<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    public static function readAmenities()
    {

    	$result = DB::table('amenities_name as a')
            ->select(
                'a.id as id',
                'a.name as name'
            )

    		->get();
    	return $result;
    }

    public static function createAmenities( $post_data )
    {
        $result = DB::table('amenities_name')
            ->insert(
                array(
                    'name'  => $post_data['name']
                )
            );
         return $result;
    }

        public static function deleteAmenities( $post_data )
    {
        $result = DB::table('amenities_name')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }

    public static function deleteAmenitiesImage( $post_data )
    {
        $result = DB::table('amenities')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
    
    public static function AmenitiesInfo( $post_data )
    {
       $result = DB::table('amenities_name')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

   
    
    public static function getAmenities( $post_data )
    {
        $result = DB::table('amenities_name as a')
            ->select('*')
            ->get();
        return $result;
    }


    public static function updateAmenities( $post_data )
    {
        $result = DB::table('amenities_name')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'name'      => $post_data['name'],
                    'updated_at'    => date("Y-m-d h.i:s")
                )
            );
        return $result;
    }

}