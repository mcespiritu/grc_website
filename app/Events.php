<?php

namespace App;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    public static function readEvent()
    {

    	$result = DB::table('event as c')
            ->select(
                'c.id as id',
                'c.thumbnail as thumbnail',
                'c.event_name as event_name',
                'c.event_content as event_content',
                'c.event_date as event_date',
                'c.event_start as event_start',
                'c.event_end as event_end'
            )
            ->orderBy('event_start', 'asc')
    		->get()
            ->map(function($event){
                $event->{'event_url'} = str_replace(' ', '-', strtolower($event->event_name));
                return $event;
            });
    	return $result;
    }

    public static function readCategory()
    {

        $result = DB::table('job_category')
            ->select('*')
            ->get();
        return $result;
    }

    public static function readPerEvent($url)
    {
        $result = DB::table('event as c')
            ->select(

                'c.id as id',
                'c.thumbnail as thumbnail',
                'c.event_name as event_name',
                'c.event_content as event_content',
                'c.event_date as event_date',
                'c.event_start as event_start',
                'c.event_end as event_end'
            )
            ->get()
            ->map(function($event){
                $event->{'event_url'} = str_replace(' ', '-', strtolower($event->event_name));
                return $event;
            })
            ->filter(function($event) use($url){
                return $event->event_url == $url;
            });
        return $result;
    }
        public static function createEvent($post_data)
    {

        if(isset($post_data['thumbnail'])){
            self::saveThumbnail($post_data);
        }

    }

    private static function saveThumbnail($post_data)
    {

        $file           = $post_data['thumbnail'];
        $event_name     = $post_data['event_name'];
        $event_content  = $post_data['event_content'];
        $event_date     = $post_data['event_date'];
        $event_start    = $post_data['event_start'];
        $event_end      = $post_data['event_end'];
        $featured       = $post_data['featured'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        DB::table('event')
            ->insert(
                array(
                    'thumbnail'         => $filename,
                    'event_name'        => $event_name,
                    'event_content'     => $event_content,
                    'event_date'        => $event_date,
                    'event_start'       => $event_start,
                    'event_end'         => $event_end,
                    'featured'          => $featured,
                    'updated_at'        => date("Y-m-d H:i:s")
                )
            );

    }
        public static function deleteEvent( $post_data )
    {
        $result = DB::table('event')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
    

    public static function EventInfo( $post_data )
    {
       $result = DB::table('event')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

    
    public static function getEvent( $post_data )
    {
        $result = DB::table('event as c')
            ->select('*')
            ->get();
        return $result;
    }
    public static function updateEvent( $post_data )
    {
        $file = $post_data['thumbnail'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        $event_name      = $post_data['event_name'];
        $event_content   = $post_data['event_content'];
        $event_date      = $post_data['event_date'];
        $event_start     = $post_data['event_start'];
        $event_end       = $post_data['event_end'];
        $featured        = $post_data['featured'];
        DB::table('event')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'thumbnail'       => $filename,
                    'event_name'      => $event_name,
                    'event_content'   => $event_content,
                    'event_date'      => $event_date,
                    'event_start'     => $event_start,
                    'event_end'       => $event_end,
                    'featured'         => $featured,
                    'updated_at'      => date("Y-m-d H:i:s")
                )
            );
    }

}