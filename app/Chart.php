<?php

namespace App;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    public static function readChart()
    {

        $result = DB::table('org_chart as oc')
            ->select(
                'oc.id      as id',
                'oc.name as name',
                'oc.jobtitle as jobtitle',
                'oc.dept_id as dept_id',
                'dt.dept_name as dept_name',
                'oc.rank as rank'
            )
            ->leftJoin('department as dt', 'oc.dept_id', '=', 'dt.id')
            ->get();
        return $result;
    }

    public static function readChartbyDept()
    {

        $result = DB::table('org_chart as oc')
            ->select('oc.dept_id as dept_id')
            ->groupBy('dept_id')
            ->get();
        return $result;

    }
    public static function readDepartment()
    {

        $result = DB::table('department')
            ->select('*')
            ->get();
        return $result;
    }

    public static function deleteOrgChart( $post_data )
    {
        $result = DB::table('org_chart')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }



    public static function createChart( $post_data )
    {

        $result = DB::table('org_chart')
            ->insert(
                array(
                    'name'     => $post_data['name'],
                    'jobtitle' => $post_data['jobtitle'],
                    'dept_id'  => $post_data['dept_id'],
                    'rank'     => $post_data['rank']
                )
            );
         return $result;
    }


    public static function getChart( $post_data )
    {
        $result = DB::table('org_chart as oc')
            ->select(
                'oc.id       as id',
                'oc.name     as name',
                'oc.jobtitle as jobtitle',
                'oc.dept_id  as dept_id',
                'oc.rank     as rank'
            )
            ->get();

        return $result;
    }
    
    public static function ChartInfo( $post_data )
    {
       $result = DB::table('org_chart')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }
    public static function updateChart( $post_data )
    {
        $result = DB::table('org_chart')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'name'          => $post_data['name'],
                    'jobtitle'      => $post_data['jobtitle'],
                    'dept_id'       => $post_data['dept_id'],
                    'rank'          => $post_data['rank'],
                    'updated_at'    => date("Y-m-d H:i:s")
                )
            );
        }
}
