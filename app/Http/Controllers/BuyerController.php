<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\About;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\Chart;
use App\Amenities;
use App\Buyer;
use App\FAQ;
use Illuminate\Http\Request;

class BuyerController extends Controller
{

    public function index()
    {
      $data['chart_list']        = Chart::readChart();
      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['history']           = About::readHistory();
      $data['department_list']   = Chart::readDepartment();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      $data['buyerbanner_list']  = Buyer::readBuyerBanner();
      $data['buyer_content']  = Buyer::readBuyerContent();
      return view('dashboard/buyer', $data);
    }

    public function websiteBuyer()
    {

      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['company_summary']   = Home::readCompanySummary();
      $data['history']           = About::readHistory();
      $data['project_list']      = Project::readProject();
      $data['company_details']   = CompanyDetails::readCompanyDetails();
      $data['chart_list']        = Chart::readChart();
      $data['department_list']   = Chart::readDepartment();
      $data['rank_list']         = Chart::readChartbyDept();
      $data['buyerbanner_list']  = Buyer::readBuyerBanner();
      $data['buyer_content']    = Buyer::readBuyerContent();
      $data['faq_list']         = FAQ::readFAQ();
      $data['project_list_tracker']  = Buyer::readProjectFromTracker();
      return view('/buyer', $data);
    }

   
    public function readBuyerBanner()
    {
        $result = Buyer::readBuyerBanner();
        return \Response::json($result);
    }

    public function getBuyerBanner( request $request )
    {
        $result = Buyer::getBuyerBanner($request);
        return \Response::json($result);
    }

        public function deleteBuyerBanner( request $request)
    {
        $result = Buyer::deleteBuyerBanner($request);
        return \Response::json($result);
    }

    public function buyerBannerInfo ( request $request )
    {
      $result = Buyer::buyerBannerInfo($request);
      return \Response::json($result);
    }



    public function createBuyerBanner( request $request )
  {
        $data           = $request->all();
        $buyer_title    = $data['buyer_title'];
        $buyer_subtitle = $data['buyer_subtitle'];
        foreach ($data['display_filename'] as $key => $value) {
            $result = Buyer::createBuyerBanner([
              'buyer_title'         => $buyer_title,
              'buyer_subtitle'      => $buyer_subtitle,   
              'display_filename'    => $value
            ]);
        }
        return \Response::json( $result );
  }
      public function updateBuyerBanner( request $request )
    {
        $validator = Validator::make(
            $request->all(), array(
                'buyer_title'    => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
                  $data     = $request->all();
            $buyer_title    = $data['buyer_title'];
            $buyer_subtitle = $data['buyer_subtitle'];
                   $id       = $data['id'];
            if(isset($data['display_filename'])){
              foreach ($data['display_filename'] as $key => $value) {
                Buyer::updateBuyerBanner([
                  'buyer_title'       => $buyer_title,
                  'buyer_subtitle'    => $buyer_subtitle,   
                  'display_filename'  => $value,
                  'id'                => $id,
                ]);
              }
            }else{
                Buyer::updateBuyerBanner([
                  'buyer_title'      => $buyer_title,
                  'buyer_subtitle'   => $buyer_subtitle,   
                  'id'               => $id,
                ]);
            }
        }
        die();

    }



    // Buyer Content
    public function getBuyerContent( request $request )
    {
        $result = Buyer::getBuyerContent($request);
        return \Response::json($result);
    }

  public function buyerContentInfo ( request $request )
  {
    $result = Buyer::buyerContentInfo($request);
    return \Response::json($result);
  }

    public function updateBuyerContent( request $request )
    {
        // Declare Validation
        $validator = Validator::make(
            $request->all(), array(
                'content' => 'required'
            )
        );

        // If validation fails return json resonse CompanySummary the error
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
      
            Buyer::updateBuyerContent($request);
        }

        die();
    }


    // register buyer

        public function registerBuyer( request $request )
    {
        $result = Buyer::registerBuyer($request);
        return \Response::json($result);
    }


    public function readProjectFromTracker( request $request )
    {
        $result = Buyer::readProjectFromTracker($request);
        return \Response::json($result);
    }

        public function readHouseUnitFromTracker( request $request )
    {

        $result = Buyer::readHouseUnitFromTracker( $request );
        return \Response::json($result);
    }

    public function readStageFromTracker( request $request)
    {
        $result = Buyer::readStageFromTracker($request);
        return \Response::json($result);
    }
}