<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Project;
use App\CompanyDetails;
use App\Career;
use App\About;
use App\Home;
use App\Amenities;
use Illuminate\Http\Request;

class CareerController extends Controller
{

    public function index()
    {
      $data['career_list']             = Career::readCareer();
      $data['category_list']           = Career::readCategory();
      $data['careerbanner_list']       = Career::readCareerBanner();
      $data['feature_list']            = Amenities::readAmenities();
      $data['project_list']            = Project::readProject();
      return view('dashboard/careers', $data);
    }

    public function websiteCareer()
    {
      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['company_summary']   = Home::readCompanySummary();
      $data['history']           = About::readHistory();
      $data['project_list']      = Project::readProject();
      $data['company_details']   = CompanyDetails::readCompanyDetails();
      $data['career_list']       = Career::readCareer();
      $data['careerbanner_list']       = Career::readCareerBanner();
      return view('/career', $data);
    }

    public function jobDetail($url)
    {
      $data['aboutbanner_list']        = About::readAbout();
      $data['visionmission']           = About::readVision();
      $data['company_summary']         = Home::readCompanySummary();
      $data['history']                 = About::readHistory();
      $data['project_list']            = Project::readProject();
      $data['company_details']         = CompanyDetails::readCompanyDetails();
      $data['career_list']             = Career::readCareerJobdetails($url);
      $data['careerbanner_list']       = Career::readCareerBanner();
      $data['career_list_per_url']     = Career::readPerCareers($url);
      return view('/jobdetails', $data);
    }

        public function readCareerBanner()
    {
        $result = Career::readCareerBanner();
        return \Response::json($result);
    }

        public function deleteCareerBanner(request $request)
    {
        $result = Career::deleteCareerBanner($request);
        return \Response::json($result);
    }
    

    public function getCareerBanner( request $request )
    {
        $result = Career::getCareerBanner($request);
        return \Response::json($result);
    }


        public function createCareerBanner ( request $request )
    {
        $data = $request->all();
        $title = $data['title'];
        $subtitle = $data['subtitle'];
        foreach ($data['banner_filename'] as $key => $value) {
            $result = Career::createCareerBanner ([
              'title'             => $title,
              'subtitle'          => $subtitle,   
              'banner_filename'   => $value
            ]);
        }
        return \Response::json( $result );
    }


  public function CareerBannerInfo ( request $request )
  {
    $result = Career::CareerBannerInfo($request);
    return \Response::json($result);
  }

    public function updateCareerBanner( request $request )
    {
        $validator = Validator::make(
            $request->all(), array(
                'title'    => 'required',
                'subtitle'  => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            $data     = $request->all();
            $title    = $data['title'];
            $subtitle = $data['subtitle'];
            $id       = $data['id'];
            if(isset($data['banner_filename'])){
              foreach ($data['banner_filename'] as $key => $value) {
                Career::updateCareerBanner([
                  'title'             => $title,
                  'subtitle'          => $subtitle,   
                  'banner_filename'  => $value,
                  'id'                => $id,
                ]);
              }
            }else{
                Career::updateCareerBanner([
                  'title'      => $title,
                  'subtitle'   => $subtitle,   
                  'id'         => $id,
                ]);
            }
        }
        die();
    }

     public function readCareer()
    {
        $result = Career::readCareer();
        return \Response::json($result);
    }

    public function createCareer( request $request )
    {
        $validator = Validator::make(
            $request->all(), [
                'job_title'  => 'required',
                'categories' => 'required',
                'job_desc'   => 'required'

            ]
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            return Response::json($error_messages);
        } else {
             Career::createCareer( $request );
        }
    }
        public function deleteCareer(request $request)
    {
        $result = Career::deleteCareer($request);
        return \Response::json($result);
    }
    
    public function getCareer( request $request )
    {
        $result = Career::getCareer($request);
        return \Response::json($result);
    }
    
    public function CareerInfo( request $request )
    {
        $result = Career::CareerInfo($request);
        return \Response::json($result);
    }
    
    public function updateCareer( request $request )
    {

        // Declare Validation
        $validator = Validator::make(
            $request->all(), array(
                'job_title'  => 'required',
                'categories' => 'required',
                'job_desc'   => 'required'
            )
        );

        // If validation fails return json resonse CompanyDetails the error
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            // Else create the user
            Career::updateCareer($request);
        }

        die();
    }
}