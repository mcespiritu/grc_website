<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\News;
use App\Events;
use App\Contact;
use App\Amenities;
use App\Buyer;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {

      $data['company_summary']  = Home::readCompanySummary();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      $data['partners_list']  = Home::readLogo();
      return view('dashboard/home', $data);
    }

    public function websitehome()
    {
      
      $data['company_summary']  = Home::readCompanySummary();
      $data['project_list']     = Project::readProject();
      $data['company_details']  = CompanyDetails::readCompanyDetails();
      $data['partners_list']  = Home::readLogo();
      $data['news_list']        = News::readNews();
      $data['event_list']       = Events::readEvent();
      $data['contact']          = Contact::readContact();
      return view('welcome', $data);
    }

    public function dashboard()
    {

      $data['company_summary']  = Home::readCompanySummary();
      $data['feature_list']     = Amenities::readAmenities();
      $data['project_list']     = Project::readProject();
      return view('dashboard/dashboard', $data);
    }

    public function contact()
    {
      

      $data['company_summary']  = Home::readCompanySummary();
      $data['project_list']     = Project::readProject();
      $data['company_details']  = CompanyDetails::readCompanyDetails();
      return view('/contact', $data);
    }

    public function read()
    {
        $result = Home::readHome();
        return \Response::json($result);
    }
    

    public function deleteHome( Request $request )
    {
        $result = Home::deleteHomeBanner($request);
        return \Response::json($result);
    }


    public function get( request $request )
    {
        $result = Home::getHome($request);
        return \Response::json($result);
    }

    public function createHome( request $request )
    {
        $data = $request->all();
        $title = $data['title'];
        $subtitle = $data['subtitle'];
        foreach ($data['banner_filename'] as $key => $value) {
            $result = Home::createHome([
              'title'             => $title,
              'subtitle'          => $subtitle,   
              'banner_filename'   => $value
            ]);
        }
        return \Response::json( $result );
    }

  public function homeBannerInfo ( request $request )
  {
    $result = Home::homeBannerInfo($request);
    return \Response::json($result);
  }

    public function updateHome( request $request )
    {
        $validator = Validator::make(
            $request->all(), array(
                'title'           => 'required',
                'subtitle'        => 'required',
                'banner_filename' => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            $data     = $request->all();
            $title    = $data['title'];
            $subtitle = $data['subtitle'];
            $id       = $data['id'];
            foreach ($data['banner_filename'] as $key => $value) {
              Home::updateHome([
                'title'             => $title,
                'subtitle'          => $subtitle,   
                'banner_filename'   => $value,
                'id'                => $id
              ]);
            }
        }
        die();
    }


    // Company Summary
    public function readCompanySummary()
    {
        $result = Home::readCompanySummary();
        return \Response::json($result);
    }

    public function getCompanySummary( request $request )
    {
        $result = Home::getCompanySummary($request);
        return \Response::json($result);
    }

    public function createCompanySummary( request $request )
    {
        $validator = Validator::make(
            $request->all(), [
                'summary_title'    => 'required',
                'summary_content'      => 'required'

            ]
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            return Response::json($error_messages);
        } else {
             Home::createCompanySummary( $request );
        }
    }

  public function companySummaryBannerInfo ( request $request )
  {
    $result = Home::companySummaryBannerInfo($request);
    return \Response::json($result);
  }

    public function updateCompanySummary( request $request )
    {
        // Declare Validation
        $validator = Validator::make(
            $request->all(), array(
                'summary_title'    => 'required',
                'summary_content' => 'required'
            )
        );

        // If validation fails return json resonse CompanySummary the error
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            // Else create the user
            Home::updateCompanySummary($request);
        }

        die();
    }

    //Partners

        public function createLogo( request $request )
    {
        $data = $request->all();
        $link = $data['link'];
        foreach ($data['logo'] as $key => $value) {
            $result = Home::createLogo([
              'link'              => $link,   
              'logo'   => $value
            ]);
        }
        return \Response::json( $result );
    }


        public function readLogo()
    {
        $result = Home::readLogo();
        return \Response::json($result);
    }

        public function deleteLogo( request $request)
    {
        $result = Home::deleteLogo($request);
        return \Response::json($result);
    }


  public function LogoInfo ( request $request )
  {
    $result = Home::LogoInfo($request);
    return \Response::json($result);
  }
    public function updateLogo( request $request )
    {
       $validator = Validator::make(
            $request->all(), array(
                'link'       => 'required',
                'logo'        => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            $data     = $request->all();
            $link    = $data['link']; 
            $id       = $data['id'];
            foreach ($data['logo'] as $key => $value) {
              Home::updateLogo([
                'link'   => $link, 
                'logo'   => $value,
                'id'     => $id
              ]);
            }
        }
        die();
    }


    public function getLogo( request $request )
    {
        $result = Home::getLogo($request);
        return \Response::json($result);
    }
}