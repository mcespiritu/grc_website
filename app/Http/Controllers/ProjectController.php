<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Project;
use App\CompanyDetails;
use App\Amenities;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

      public function websiteindex($url)
    {
        $data['project_list_per_project']   = Project::readPerProject($url);
        $data['project_list']               = Project::readProject();
        $data['company_details']            = CompanyDetails::readCompanyDetails();

        $primary_project = Project::readPerProject($url);
        
        foreach ($primary_project as $key => $value) {
          $id = $value->id;
        }

        $data['amenities_list']  = Project::getProjectAmenities($id);
        $data['feature_list_per_project']  = Project::readFeature($id);
        $data['model_unit_list']  = Project::getModelUnitDetails($id);
        $data['banner_list']  = Project::getProjectSlider($id);
        return view('project', $data);
    }

        public function deleteProject( request $request)
    {
        $result = Project::deleteProject($request);
        return \Response::json($result);
    }

    public function deleteFeature( request $request)
    {
        $result = Project::deleteFeature($request);
        return \Response::json($result);
    }
        public function deleteBannerImage( request $request)
    {
        $result = Project::deleteBannerImage($request);
        return \Response::json($result);
    }


      public function modelindex()
    {

        $data['feature_list']  = Amenities::readAmenities();
        $data['project_list']  = Project::readProjectMatvis();
        $data['feature_list_per_model']  = Project::readFeaturePerModel();
        $data['model_unit_list']  = Project::readModelUnit();

        return view('dashboard/model_unit', $data);

    }

      public function index()
    {

        $data['feature_list']  = Amenities::readAmenities();
        $data['project_list']  = Project::readProject();
        return view('dashboard/project', $data);
    }


     public function updateProjectPage($id)
    {
      $data['id']   = $id;
      $data['project_primary_details']   = Project::getPrimaryDetails($id);
      $data['amenities_list']            = Project::getProjectAmenities($id);
      $data['feature_list_per_project']  = Project::getProjectFeatures($id);
      $data['banner_list']  = Project::getProjectSlider($id);
      $data['feature_list']  = Amenities::readAmenities();
      return view('dashboard/updateProject', $data);
    }

    public function getProjectDataForEditing(request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $request = Project::getPrimaryDetails($id);
        return \Response::json($request);
    }
    
        public function readProject()
    {
        $result = Project::readProject();
        return \Response::json($result);
    }

    public function getProject( request $request )
    {
        $result = Project::getProject($request);
        return \Response::json($result);
    }

    public function projectInfo ( request $request )
    {
      $result = Project::projectInfo($request);
      return \Response::json($result);
    }


     public function createProject( request $request )
    {
        $result = Project::createProject($request);
        $data = $request->all();
        foreach ($data['amenities_image'] as $key => $value) {
            $afterResult = Project::createProjectAmmenities([
              'amenities_image'   => $value
            ]);
        }

        return \Response::json($result);
    }





    public function updateProject( request $request )
    {


        $data               = $request->all();
        $featured           = $data['featured'];
        $status             = $data['status'];
        $project_name       = $data['project_name'];
        $project_summary    = $data['project_summary'];
        $project_location   = $data['project_location'];
        $project_intro      = $data['project_intro'];
        $walkthrough        = $data['walkthrough'];
        $vicinity_map       = isset($data['vicinity_map'])      ? $data['vicinity_map']     : '';
        $display_filename   = isset($data['display_filename'])  ? $data['display_filename'] : '';
        $amenities_image    = isset($data['amenities_image'])   ? $data['amenities_image']  : '';
        $id                 = $data['id'];

         Project::updateProject([
                        'id'                 => $id,
                        'featured'           => $featured,
                        'status'             => $status,
                        'project_name'       => $project_name,
                        'project_summary'    => $project_summary,   
                        'project_intro'      => $project_intro,
                        'project_location'   => $project_location,
                        'walkthrough'        => $walkthrough,
                    ]);
        if(isset($data['display_filename'])){
            foreach ($data['display_filename'] as $key => $value) {
                Project::updateProject([
                    'id'                 => $id,
                    'featured'           => $featured,
                    'status'             => $status,
                    'project_name'       => $project_name,
                    'project_summary'    => $project_summary,   
                    'display_filename'   => $value,
                    'project_intro'      => $project_intro,
                    'project_location'   => $project_location,
                    'walkthrough'        => $walkthrough,
                ]);
            }


        }
        if(isset($data['feat_id'])){
            foreach ($data['feat_id'] as $key => $value) {
                Project::updateProjectFeatures([
                    'id'                        => $data['feat_id'][$key],
                    'feature_value'             => $data['feature_value'][$key],
                    'feature_id'                => $data['feature_id'][$key]
                ]);
            }
        }
        if(isset($data['vicinity_map'])){
            foreach ($data['vicinity_map'] as $key => $value) {
                Project::updateProject([
                    'id'                 => $id,
                    'featured'           => $featured,
                    'status'             => $status,
                    'project_name'       => $project_name,
                    'project_summary'    => $project_summary,   
                    'vicinity_map'   => $value,
                    'project_intro'      => $project_intro,
                    'project_location'   => $project_location,
                    'walkthrough'        => $walkthrough,
                ]);
            }
        }
        if(isset($data['amenities_image'])){
            foreach ($data['amenities_image'] as $key => $value) {
                Project::updateProject([
                    'id'                 => $id,
                    'featured'           => $featured,
                    'status'             => $status,
                    'project_name'       => $project_name,
                    'project_summary'    => $project_summary,   
                    'amenities_image'   => $value,
                    'project_intro'      => $project_intro,
                    'project_location'   => $project_location,
                    'walkthrough'        => $walkthrough,
                ]);
            }
        }
        if(isset($data['banner'])){
            foreach ($data['banner'] as $key => $value) {
                Project::updateProject([
                    'id'                 => $id,
                    'featured'           => $featured,
                    'status'             => $status,
                    'project_name'       => $project_name,
                    'project_summary'    => $project_summary,   
                    'banner'            => $value,
                    'project_intro'      => $project_intro,
                    'project_location'   => $project_location,
                    'walkthrough'        => $walkthrough,
                ]);
            }
        }
            
    }

    public function createModelUnit (request $request)
    {
     $data           = $request->all();
      $project_id    = $data['project_id'];
        $model_name    = $data['model_name'];
        $floor_area     = $data['floor_area'];
         $lot_area     = $data['lot_area'];

        foreach ($data['display_filename'] as $key => $value) {
            $result = Project::createModelUnit([
                'project_id'         => $project_id,
              'model_name'         => $model_name,
              'floor_area'      => $floor_area,   
              'lot_area'      => $lot_area,   
              'display_filename'    => $value
            ]);
        }
        return \Response::json( $result );
    }

    public function readModelUnit()
    {
        $result = Project::readModelUnit();
        return \Response::json($result);
    }

    public function getModelUnit(request $request)
    {
        $result = Project::getModelUnit($request);
        return \Response::json($result);
    }

    public function updateModelUnit(request $request)
    {
        $result = Project::updateModelUnit($request);
        return \Response::json($result);
    }

    public function modelUnitInfo (request $request)
    {
      $result = Project::modelUnitInfo($request);
      return \Response::json($result);
    }

        public function deleteModelUnit( request $request)
    {
        $result = Project::deleteModelUnit($request);
        return \Response::json($result);
    }

    
}