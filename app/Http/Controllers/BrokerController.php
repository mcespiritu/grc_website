<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BrokerController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function broker()
    {
        return view('broker');
    }
    public function buyer()
    {
        return view('buyer');
    }
}
