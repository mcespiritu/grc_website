<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\Chart;
use App\Contact;
use App\Amenities;
use Illuminate\Http\Request;

class CompanyDetailsController extends Controller
{
    
    public function index()
    {
     
      $data['company_details']  = CompanyDetails::readCompanyDetails();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/company_details',$data);
    }

    public function readCompanyDetails()
    {
        $result = CompanyDetails::readCompanyDetails();
        return \Response::json($result);
    }

    public function getCompanyDetails( request $request )
    {
        $result = CompanyDetails::getCompanyDetails($request);
        return \Response::json($result);
    }

    public function companyDetailsInfo ( request $request )
    {
      $result = CompanyDetails::companyDetailsInfo($request);
      return \Response::json($result);
    }
        public static function getPartner($id)
    {
        $result = DB::table('partner')
            ->select('*')
            ->where('company_id', '=', $id)
            ->get();
        return $result;
    }

    public function updateCompanyDetails( request $request )
    {

        // Declare Validation
        $validator = Validator::make(
            $request->all(), array(
                'company_name'  => 'required',
                'address' => 'required'
            )
        );

        // If validation fails return json resonse CompanyDetails the error
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            // Else create the user
            CompanyDetails::updateCompanyDetails($request);
        }

        die();

        foreach ($data['partnerlogo'] as $key => $value) {
            $afterResult = CompanyDetails::createPartnerLogo([
              'partnerlogo'   => $value
            ]);
        }
    }
}