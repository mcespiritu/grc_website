<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\News;
use App\Events;
use App\Buyer;
use App\Amenities;
use App\FAQ;

use Illuminate\Http\Request;

class AmenitiesController extends Controller
{

    public function index()
    {
      // $data['banner_list']      = Home::readHome();
      $data['amenities_list']         = Amenities::readAmenities();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/amenities', $data);
    }

    public function websiteFAQ()
    {
      
      $data['banner_list']      = Home::readHome();
      $data['company_summary']  = Home::readCompanySummary();
      $data['project_list']     = Project::readProject();
      $data['company_details']  = CompanyDetails::readCompanyDetails();
      $data['news_list']        = News::readNews();
      $data['event_list']       = Events::readEvent();
      $data['faq_list']         = Amenities::readFAQ();
      return view('/buyer', $data);
    }

    public function read()
    {
        $result = Amenities::readFAQ();
        return \Response::json($result);
    }
    public function deleteAmenities(request $request )
    {
        $result = Amenities::deleteAmenities($request);
        return \Response::json($result);
    }
        public function deleteAmenitiesImage(request $request )
    {
        $result = Amenities::deleteAmenitiesImage($request);
        return \Response::json($result);
    }
    public function getAmenities( request $request )
    {
        $result = Amenities::getAmenities($request);
        return \Response::json($result);
    }

    public function createAmenities( request $request )
    {
        $validator = Validator::make(
            $request->all(), [
                'name'    => 'required'
            ]
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            return Response::json($error_messages);
        } else {
             Amenities::createAmenities( $request );
        }
    }

  public function AmenitiesInfo ( request $request )
  {
    $result = Amenities::AmenitiesInfo($request);
    return \Response::json($result);
  }
    public function updateAmenities( request $request)
    {
        $result = Amenities::updateAmenities($request);
        return \Response::json($result);
    }
}