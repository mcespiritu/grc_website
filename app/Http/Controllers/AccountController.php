<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\About;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\Chart;
use App\Amenities;
use App\Buyer;
use Illuminate\Http\Request;

class AccountController extends Controller
{

    public function index()
    {
      $data['chart_list']        = Chart::readChart();
      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['history']           = About::readHistory();
      $data['department_list']   = Chart::readDepartment();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/account-setting', $data);
    }



}