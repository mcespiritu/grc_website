<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\Chart;
use App\Contact;
use App\Amenities;
use App\FAQ;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
      $data['contact']  = Contact::readContact();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/contact',$data);
    }

    public function websiteContact()
    {
      $data['contact']  = Contact::readContact();
      $data['company_details']  = CompanyDetails::readCompanyDetails();
      $data['project_list']  = Project::readProject();
      $data['faq_list']         = FAQ::readFAQ();
      return view('/contact', $data);
    }


    public function createInquiry(request $request)
    {
        $result = Contact::createInquiry($request);
        return \Response::json($result);
    }
    public function readContact()
    {
        $result = Contact::readContact();
        return \Response::json($result);
    }

    public function getContact( request $request )
    {
        $result = Contact::getContact($request);
        return \Response::json($result);
    }

    public function ContactBannerInfo ( request $request )
    {
      $result = Contact::ContactBannerInfo($request);
      return \Response::json($result);
    }
    public function deleteContactBanner( request $request )
    {
        $result = Contact::deleteContactBanner($request);
        return \Response::json($result);
    }
    public function createContactBanner ( request $request )
    {
        $data = $request->all();
        $title = $data['title'];
        $subtitle = $data['subtitle'];
        foreach ($data['display_filename'] as $key => $value) {
            $result = Contact::createContactBanner ([
              'title'             => $title,
              'subtitle'          => $subtitle,   
              'display_filename'   => $value
            ]);
        }
        return \Response::json( $result );
    }

      public function updateContact( request $request )
    {
        $validator = Validator::make(
            $request->all(), array(
                'title'    => 'required',
                'subtitle'  => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            $data     = $request->all();
            $title    = $data['title'];
            $subtitle = $data['subtitle'];
            $id       = $data['id'];
            if(isset($data['display_filename'])){
              foreach ($data['display_filename'] as $key => $value) {
                Contact::updateContact([
                  'title'             => $title,
                  'subtitle'          => $subtitle,   
                  'display_filename'  => $value,
                  'id'                => $id,
                ]);
              }
            }else{
                Contact::updateContact([
                  'title'      => $title,
                  'subtitle'   => $subtitle,   
                  'id'         => $id,
                ]);
            }
        }
        die();
    }

}