<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\News;
use App\Events;
use App\Buyer;
use App\FAQ;
use App\Amenities;

use Illuminate\Http\Request;

class FAQController extends Controller
{

    public function index()
    {
      $data['banner_list']      = Home::readHome();
      $data['faq_list']         = FAQ::readFAQ();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/faq', $data);
    }

    public function websiteFAQ()
    {
      
      $data['banner_list']      = Home::readHome();
      $data['company_summary']  = Home::readCompanySummary();
      $data['project_list']     = Project::readProject();
      $data['company_details']  = CompanyDetails::readCompanyDetails();
      $data['news_list']        = News::readNews();
      $data['event_list']       = Events::readEvent();
      $data['faq_list']         = FAQ::readFAQ();
      return view('/buyer', $data);
    }

    public function read()
    {
        $result = FAQ::readFAQ();
        return \Response::json($result);
    }
    public function deleteFAQ(request $request )
    {
        $result = FAQ::deleteFAQ($request);
        return \Response::json($result);
    }
    public function get( request $request )
    {
        $result = FAQ::getFAQ($request);
        return \Response::json($result);
    }

    public function createFAQ( request $request )
    {
        $validator = Validator::make(
            $request->all(), [
                'question'    => 'required',
                'answer' => 'required'

            ]
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            return Response::json($error_messages);
        } else {
             FAQ::createFAQ( $request );
        }
    }

  public function FAQInfo ( request $request )
  {
    $result = FAQ::FAQInfo($request);
    return \Response::json($result);
  }

    public function updateFAQ( request $request )
    {
        // Declare Validation
        $validator = Validator::make(
            $request->all(), array(
                'question'    => 'required',
                'answer' => 'required'
            )
        );

        // If validation fails return json resonse FAQ the error
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            // Else create the user
            FAQ::updateFAQ($request);
        }

        die();
    }
}