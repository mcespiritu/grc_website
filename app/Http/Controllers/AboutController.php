<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\About;
use App\Home;
use App\Project;
use App\CompanyDetails;
use App\Chart;
use App\Amenities;
use App\Buyer;
use Illuminate\Http\Request;
use Storage;

class AboutController extends Controller
{

    public function index()
    {
      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['history']           = About::readHistory();
      $data['department_list']   = Chart::readDepartment();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/about', $data);
    }

    public function websiteAbout()
    {

      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['company_summary']   = Home::readCompanySummary();
      $data['history']           = About::readHistory();
      $data['project_list']      = Project::readProject();
      $data['company_details']   = CompanyDetails::readCompanyDetails();
      $data['department_list']   = Chart::readDepartment();
      $data['rank_list']         = Chart::readChartbyDept();
      return view('/about', $data);
    }

    public function company()
    {
      $data['project_list']      = Project::readProject();
      $data['aboutbanner_list']  = About::readAbout();
      $data['company_summary']   = Home::readCompanySummary();
      $data['company_details']   = CompanyDetails::readCompanyDetails();
      return view('/company', $data);
    }

    public function vision()
    {
      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['project_list']      = Project::readProject();
      $data['company_summary']   = Home::readCompanySummary();
      $data['company_details']   = CompanyDetails::readCompanyDetails();
      return view('/vision-mission', $data);
    }


    public function readAbout()
    {
        $result = About::readAbout();
        return \Response::json($result);
    }

    public function getAbout( request $request )
    {
        $result = About::getAbout($request);
        return \Response::json($result);
    }

        public function deleteAbout( request $request)
    {
        $result = About::deleteAbout($request);
        return \Response::json($result);
    }

    public function aboutBannerInfo ( request $request )
    {
      $result = About::aboutBannerInfo($request);
      return \Response::json($result);
    }

    public function createAboutBanner( request $request )
  {
        $data           = $request->all();
        $about_title    = $data['about_title'];
        $about_subtitle = $data['about_subtitle'];
        foreach ($data['display_filename'] as $key => $value) {
            $result = About::createAbout([
              'about_title'         => $about_title,
              'about_subtitle'      => $about_subtitle,   
              'display_filename'    => $value
            ]);
        }
        return \Response::json( $result );
  }
      public function updateAbout( Request $request )
    {
        $validator = Validator::make(
            $request->all(), array(
                'about_title'    => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            $data     = $request->all();
            $about_title    = $data['about_title'];
            $about_subtitle = $data['about_subtitle'];
            $id       = $data['id'];

            $file = collect($request->file('display_filename'))->first();
            $fileName = $file->getClientOriginalName();

            Storage::disk('public_uploads')->putFileAs('slider/'.$fileName,$file,$fileName);

            // ob_end_clean();
            // ob_start();
            // $content = ob_get_contents($file);
            // ob_end_clean();


            if(isset($data['display_filename'])){
              foreach ($data['display_filename'] as $key => $value) {
                About::updateAbout([
                  'about_title'       => $about_title,
                  'about_subtitle'    => $about_subtitle,   
                  'display_filename'  => $value,
                  'id'                => $id,
                ]);
              }
            }else{
                About::updateAbout([
                  'about_title'      => $about_title,
                  'about_subtitle'   => $about_subtitle,   
                  'id'               => $id,
                ]);
            }
        }
        die();
    }


  // Vision & Mission

  public function readVision()
    {
        $result = About::readVision();
        return \Response::json($result);
    }

    public function getVision( request $request )
    {
        $result = About::getVision($request);
        return \Response::json($result);
    }
    public function visionInfo ( request $request )
    {
      $result = About::visionInfo($request);
      return \Response::json($result);
    }

    public function updateVision( request $request )
    {


        // Declare Validation
        $validator = Validator::make(
            $request->all(), array(
                'vision'  => 'required',
                'mission' => 'required'
            )
        );

        // If validation fails return json resonse about the error
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            // Else create the user
            About::updateVision($request);
        }

        die();
    }



//Company History

     public function readHistory()
    {
        $result = About::readHistory();
        return \Response::json($result);
    }

    public function createHistory( request $request )
    {
        $validator = Validator::make(
            $request->all(), [
                'date'      => 'required',
                'content'   => 'required'

            ]
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            return Response::json($error_messages);
        } else {
             About::createHistory( $request );
        }
    }
    public function getHistory( request $request )
    {
        $result = About::getHistory($request);
        return \Response::json($result);
    }
    public function HistoryInfo( request $request )
    {
        $result = About::HistoryInfo($request);
        return \Response::json($result);
    }
    
    public function updateHistory ( request $request )
    {
        $validator = Validator::make(
            $request->all(), [
                'date'      => 'required',
                'content'   => 'required'

            ]
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            return Response::json($error_messages);
        } else {
             About::updateHistory( $request );
        }
    }

}