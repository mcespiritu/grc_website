<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Chart;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function index()
    {
      $data['chart_list']  = Chart::readChart();
      return view('dashboard/chart',$data);
    }

    public function readChart()
    {
        $result = Chart::readChart();
        return \Response::json($result);
    }

    public function getChart( request $request )
    {
        $result = Chart::getChart($request);
        return \Response::json($result);
    }

    public function deleteOrgChart( request $request )
    {
        $result = Chart::deleteOrgChart($request);
        return \Response::json($result);
    }
    public function ChartInfo ( request $request )
    {
      $result = Chart::ChartInfo($request);
      return \Response::json($result);
    }

    public function updateChart( request $request )
    {

        // Declare Validation
        $validator = Validator::make(
            $request->all(), array(
                'name'      => 'required',
                'jobtitle'  => 'required',
                'dept_id' => 'required',
                'rank'      => 'required'
            )
        );

        // If validation fails return json resonse Chart the error
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
            // Else create the user
            Chart::updateChart($request);
        }

        die();
    }
    public function createChart( request $request )
    {
        $validator = Validator::make(
            $request->all(), [
                'name'  => 'required',
                'jobtitle' => 'required',
                'dept_id'  => 'required',
                'rank' => 'required'

            ]
        );
        if ($validator->fails()) {
            $error_messages = $validator->messages();
            return Response::json($error_messages);
        } else {
             Chart::createChart( $request );
        }
    }

}