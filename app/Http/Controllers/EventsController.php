<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Project;
use App\CompanyDetails;
use App\About;
use App\Home;
use App\Events;
use App\Amenities;
use Illuminate\Http\Request;

class EventsController extends Controller
{

    public function index()
    {
      $data['event_list']      = Events::readEvent();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/event', $data);
    }

    public function websiteEvent($url)
    {
      $data['banner_list']        = Home::readHome();
      $data['company_summary']    = Home::readCompanySummary();
      $data['project_list']       = Project::readProject();
      $data['company_details']    = CompanyDetails::readCompanyDetails();
      $data['event_list']         = Events::readEvent();
      $data['event_list_per_url'] = Events::readPerEvent($url);
      return view('/event', $data);
    }

     public function readEvent()
    {
        $result = Events::readEvent();
        return \Response::json($result);
    }

    public function deleteEvent(request $request )
    {
        $result = Events::deleteEvent($request);
        return \Response::json($result);
    }

    public function createEvent( request $request )
    {

        $data = $request->all();

        // dd($data);
        $event_name     = $data['event_name'];
        $event_content  = $data['event_content'];
        $event_date     = $data['event_date'];
        $event_start    = $data['event_start'];
        $event_end      = $data['event_end'];
        $featured       = $data['featured'];
        foreach ($data['thumbnail'] as $key => $value) {
            $result = Events::createEvent([
              'event_name'    => $event_name,
              'event_content' => $event_content,
              'event_date'    => $event_date,
              'event_start'    => $event_start,   
              'event_end'    => $event_end,
              'featured'      => $featured,
              'thumbnail'     => $value
            ]); 
        }
        return \Response::json( $result );
    }
    public function getEvent( request $request )
    {
        $result = Events::getEvent($request);
        return \Response::json($result);
    }
    public function EventInfo( request $request )
    {
        $result = Events::EventInfo($request);
        return \Response::json($result);
    }
    
    public function updateEvent( request $request )   
{

          $validator = Validator::make(
            $request->all(), array(
                'event_name'    => 'required',
                'event_content'  => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } else {
        $data           = $request->all();
        $event_name     = $data['event_name'];
        $event_content  = $data['event_content'];
        $event_date     = $data['event_date'];
        $event_start    = $data['event_start'];
        $event_end      = $data['event_end'];
        $featured       = $data['featured'];
        $id             = $data['id'];
        if(isset($data['thumbnail'])){
          foreach ($data['thumbnail'] as $key => $value) {
            Events::updateEvent([
              'event_name'      => $event_name,
              'event_content'   => $event_content,   
              'event_date'      => $event_date, 
              'event_start'     => $event_start,    
              'event_end'       => $event_end,  
              'thumbnail'       => $value,
              'featured'        => $featured,
              'id'              => $id,
            ]);
          }
        }
        else{
            Events::updateEvent([
              'event_name'     => $event_name,
              'event_content'  => $event_content,   
              'event_date'     => $event_date,   
              'event_start'    => $event_start,    
              'event_end'      => $event_end, 
              'featured'       => $featured,
              'id'             => $id,
            ]);
        }
                }
        die();
    }

}