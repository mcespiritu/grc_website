<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Response;
use App\Project;
use App\CompanyDetails;
use App\Events;
use App\News;
use App\About;
use App\Home;
use App\Amenities;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function index()
    {
      $data['news_list']      = News::readNews();
      $data['feature_list']  = Amenities::readAmenities();
      $data['project_list']  = Project::readProject();
      return view('dashboard/news', $data);
    }

    public function websitenews($url)
    {

      $data['aboutbanner_list']  = About::readAbout();
      $data['visionmission']     = About::readVision();
      $data['company_summary']   = Home::readCompanySummary();
      $data['history']           = About::readHistory();
      $data['project_list']      = Project::readProject();
      $data['company_details']   = CompanyDetails::readCompanyDetails();
      $data['news_list']          = News::readNews();
      $data['news_list_per_url']   = News::readPerNews($url);
      return view('/news', $data);
    }

     public function readNews()
    {
        $result = News::readNews();
        return \Response::json($result);
    }

    public function deleteNews(request $request )
    {
        $result = News::deleteNews($request);
        return \Response::json($result);
    }

    public function createNews( request $request )
    {

        $data = $request->all();

        // dd($data);
        $title          = $data['title'];
        $content        = $data['content'];
        $publish_date   = $data['publish_date'];
        $featured       = $data['featured'];
        foreach ($data['thumbnail'] as $key => $value) {
            $result = News::createNews([
              'title'         => $title,
              'content'       => $content,
              'publish_date'  => $publish_date,   
              'featured'      => $featured,
              'thumbnail'     => $value
            ]); 
        }
        return \Response::json( $result );
    }
    public function getNews( request $request )
    {
        $result = News::getNews($request);
        return \Response::json($result);
    }
    public function NewsInfo( request $request )
    {
        $result = News::NewsInfo($request);
        return \Response::json($result);
    }
    
    public function updateNews( request $request )  
  {


        $validator = Validator::make(
            $request->all(), array(
                'title'    => 'required',
                'content'  => 'required'
            )
        );
        if ($validator->fails()) 
        {   
            $error_messages = $validator->messages();
            return \Response::json($error_messages);
        } 

        else {

        $data           = $request->all();
        $title          = $data['title'];
        $content        = $data['content'];
        $publish_date   = $data['publish_date'];
        $featured       = $data['featured'];
        $id             = $data['id'];
        if(isset($data['thumbnail'])){
          foreach ($data['thumbnail'] as $key => $value) {
            News::updateNews([
              'title'           => $title,
              'content'         => $content,   
              'publish_date'    => $publish_date,   
              'thumbnail'       => $value,
              'featured'        => $featured,
              'id'              => $id
            ]);
          }
        }
        else{
            News::updateNews([
              'title'           => $title,
              'content'         => $content,   
              'publish_date'    => $publish_date,   
              'featured'        => $featured,
              'id'              => $id
            ]);
        }
    }
  die();
    }
}