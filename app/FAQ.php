<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    public static function readFAQ()
    {

    	$result = DB::table('faq as f')
            ->select(
                'f.id as id',
                'f.question as question',
                'f.answer as answer'
            )

    		->get();
    	return $result;
    }

    public static function createFAQ( $post_data )
    {
        $result = DB::table('faq')
            ->insert(
                array(
                    'question'  => $post_data['question'],
                    'answer'    => $post_data['answer']
                )
            );
         return $result;
    }

        public static function deleteFAQ( $post_data )
    {
        $result = DB::table('faq')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }
    
    public static function FAQInfo( $post_data )
    {
       $result = DB::table('faq')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

   
    
    public static function getFAQ( $post_data )
    {
        $result = DB::table('faq as c')
            ->select('*')
            ->get();
        return $result;
    }
    public static function updateFAQ( $post_data )
    {
        $result = DB::table('faq')
           ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'question'      => $post_data['question'],
                    'answer'        => $post_data['answer'],
                    'updated_at'    => date("Y-m-d h.i:s")
                )
            );
    }

}