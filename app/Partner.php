<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    public static function readAmenities()
    {

        $result = DB::table('partner as p')
            ->select(
                'p.id as id',
                'p.link as link',
                'p.logo as logo'
            )

            ->get();
        return $result;
    }

    public static function createAmenities( $post_data )
    {
        $result = DB::table('partner')
            ->insert(
                array(
                    'link'  => $post_data['link']
                    'logo'  => $post_data['logo']
                )
            );
         return $result;
    }

        public static function deleteAmenities( $post_data )
    {
        $result = DB::table('partner')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }

   
    
    public static function getAmenities( $post_data )
    {
        $result = DB::table('partner as a')
            ->select('*')
            ->get();
        return $result;
    }


    public static function updateAmenities( $post_data )
    {
        $result = DB::table('partner')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'name'      => $post_data['name'],
                    'updated_at'    => date("Y-m-d h.i:s")
                )
            );
        return $result;
    }

}