<?php

namespace App;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class CompanyDetails extends Model
{

    // Company Details
        public static function readCompanyDetails()
    {

        $result = DB::table('company_details as cd')
            ->select(
                'cd.id      as id',
                'cd.company_name as company_name',
                'cd.address    as address',
                'cd.phone      as phone',
                'cd.mobile     as mobile',
                'cd.email      as email',
                'cd.facebook   as facebook',
                'cd.instagram  as instagram',
                'cd.youtube    as youtube',
                'cd.twitter    as twitter',
                'cd.linkedin   as linkedin'
            )
            

            ->get();
        return $result;
    }

        public static function getCompanyDetails( $post_data )
    {
        $result = DB::table('company_details as cd')
            ->select(
                'cd.id      as id',
                'cd.company_name as company_name',
                'cd.address as address',
                'cd.phone   as phone',
                'cd.mobile  as mobile',
                'cd.email   as email',
                'cd.facebook   as facebook',
                'cd.instagram  as instagram',
                'cd.youtube    as youtube',
                'cd.twitter    as twitter',
                'cd.linkedin   as linkedin'
            )
            ->get();

        return $result;
    }
    
    public static function companyDetailsInfo( $post_data )
    {
       $result = DB::table('company_details')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }
    public static function updateCompanyDetails( $post_data )
    {
        $result = DB::table('company_details')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'company_name'  => $post_data['company_name'],
                    'address'       => $post_data['address'],
                    'phone'         => $post_data['phone'],
                    'mobile'        => $post_data['mobile'],
                    'email'         => $post_data['email'],
                    'facebook'      => $post_data['facebook'],
                    'instagram'     => $post_data['instagram'],
                    'youtube'       => $post_data['youtube'],
                    'twitter'       => $post_data['twitter'],
                    'linkedin'      => $post_data['linkedin'],
                    'updated_at'    => date("Y-m-d H:i:s")
                )
            );

             return $result;
        }

public static function createPartnerLogo( $post_data )
    {
        if(isset($post_data['partnerlogo'])){
            self::savePartnerLogo($post_data);
        }
    }

    private static function savePartnerLogo($post_data)
    { 
        
        $getLatestDetailsInserted = DB::table('company_details')
            ->select('id')
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->get();

        $companyID = $getLatestDetailsInserted[0]->id;
        $file = $post_data['partnerlogo'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        DB::table('company_details')
            ->insert(
                array(
                    'company_id'            => $companyID,
                    'partnerlogo'       => $filename
                )
            );
    }
}
