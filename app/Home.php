<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{

     // Read



     // Company Summary
    public static function readCompanySummary()
    {

      $result = DB::table('home_summary as h')
            ->select(
                'h.id as id',
                'h.summary_title as summary_title',
                'h.summary_content as summary_content',
                'h.video_link as video_link'
            )
        ->get();
      return $result;
    }

    public static function getCompanySummary( $post_data )
    {
      $result = DB::table('home_summary as h')
            ->select(
                'h.id as id',
                'h.summary_title as summary_title',
                'h.summary_content as summary_content',
                'h.video_link as video_link'
            )
        ->get();

      return $result;

    }

    public static function companySummaryBannerInfo( $post_data )
    {
       $result = DB::table('home_summary')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }

    public static function updateCompanySummary( $post_data )
    {
        $result = DB::table('home_summary')
           ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'summary_title'      => $post_data['summary_title'],
                    'summary_content'    => $post_data['summary_content'],
                    'video_link'         => $post_data['video_link'],
                    'updated_at'         => date("Y-m-d h.i:s")
                )
            );
    }
    

    public static function createCompanySummary( $post_data )
    {

        $result = DB::table('home_summary')
            ->insert(
                array(
                    'summary_title'      => $post_data['summary_title'],
                    'summary_content'    => $post_data['summary_content'],
                    'video_link'         => $post_data['video_link']
                )
            );
         return $result;
    }

    //Company Logo

        public static function createLogo( $post_data )
    {

        if(isset($post_data['logo'])){
            self::saveLogo($post_data);
        }

    }

    private static function saveLogo($post_data)
    {
        $file = $post_data['logo'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        $link      = $post_data['link'];
        DB::table('partner')
            ->insert(
                array(
                    'logo'              => $filename,
                    'link'             => $link
                )
            );
    }


    public static function readLogo()
    {

        $result = DB::table('partner as p')
            ->select(
                'p.id as id',
                'p.link as link',
                'p.logo as logo'
            )
            ->get();
        return $result;
    }


    public static function deleteLogo( $post_data )
    {
        $result = DB::table('partner')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }


     public static function updateLogo( $post_data )
    {
        $link      = $post_data['link'];
        if(isset($post_data['logo'])){
            $file = $post_data['logo'];
            $path = '/storage/imageGallery/';
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getFilename() . '.' . $extension;
            $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
            DB::table('partner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'logo'  => $filename,
                        'link'       => $link
                    )
                );
        }else{
            DB::table('partner')
                ->where('id', '=', $post_data['id'])
                ->update(
                    array(
                        'link'       => $link
                    )
                );
            }
    }

        public static function getLogo( $post_data )
    {
      $result = DB::table('partner as p')
            ->select(
                'p.id as id',
                'p.link as link'
            )
        ->get();

      return $result;
  }

      public static function LogoInfo( $post_data )
    {
       $result = DB::table('partner')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }


}