<?php

namespace App;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public static function readNews()
    {

    	$result = DB::table('news as n')
            ->select(
                'n.id as id',
                'n.title as title',
                'n.content as content',
                'n.thumbnail as thumbnail',
                'n.publish_date as publish_date',
                'n.featured as featured'
            )
    		->get()
            ->map(function($news){
                $news->{'news_url'} = str_replace(' ', '-', strtolower($news->title));
                return $news;
            });
    	return $result;
    }
    public static function readPerNews($url)
    {
        $result = DB::table('news as n')
            ->select(
                'n.id as id',
                'n.title as title',
                'n.content as content',
                'n.thumbnail as thumbnail',
                'n.publish_date as publish_date',
                'n.featured as featured'
                            )
            ->get()
            ->map(function($news){
                $news->{'news_url'} = str_replace(' ', '-', strtolower($news->title));
                return $news;
            })
            ->filter(function($news) use($url){
                return $news->news_url == $url;
            });
        return $result;
    }

        public static function createNews($post_data)
    {

        if(isset($post_data['thumbnail'])){
            self::saveThumbnail($post_data);
        }

    }

    private static function saveThumbnail($post_data)
    {

        $file           = $post_data['thumbnail'];
        $title          = $post_data['title'];
        $content        = $post_data['content'];
        $publish_date   = $post_data['publish_date'];
        $featured       = $post_data['featured'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        DB::table('news')
            ->insert(
                array(
                    'thumbnail'         => $filename,
                    'title'             => $title,
                    'content'           => $content,
                    'publish_date'      => $publish_date,
                    'featured'          => $featured,
                    'updated_at'        => date("Y-m-d H:i:s")
                )
            );

    }

    public static function NewsInfo( $post_data )
    {
       $result = DB::table('news')
            ->select('*')
            ->where('id', '=', $post_data['id'])
            ->get();
        return $result;
    }
    
    public static function deleteNews( $post_data )
    {
       $result = DB::table('news')
            ->where('id', '=', $post_data['id'])
            ->delete();
        return $result;
    }

    
    public static function getNews( $post_data )
    {
        $result = DB::table('news as n')
            ->select('*')
            ->get();
        return $result;
    }
    public static function updateNews( $post_data )
    {

        $file = $post_data['thumbnail'];
        $path = '/storage/imageGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $avatar = \Storage::disk('local')->put($path . $filename, \File::get($file));
        $title          = $post_data['title'];
        $content        = $post_data['content'];
        $publish_date   = $post_data['publish_date'];
        $featured       = $post_data['featured'];
        DB::table('news')
            ->where('id', '=', $post_data['id'])
            ->update(
                array(
                    'thumbnail'     => $filename,
                    'title'         => $title,
                    'content'       => $content,
                    'publish_date'  => $publish_date,
                    'featured'      => $featured,
                    'updated_at'     => date("Y-m-d H:i:s")
                )
            );
    }

}