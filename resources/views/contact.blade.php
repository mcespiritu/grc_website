@extends('layouts.app') 
@section('content')

<div id="pageslider" class="carousel slide contact" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
          @foreach($contact as $row)
        <div class="carousel-item active" style="background-image: url('{{asset('/storage/imageGallery/'.$row->banner)}}')">
            <div class="carousel-caption d-none d-md-block">
                <p class="subtitle"> Got a question?</p>
                <h1 class="section-title" style="padding-bottom: 10px; padding-top: 10px;font-size:34px!important;">{{$row->title}}</h1>
                <p class="subtitle">{!! nl2br(e($row->subtitle)) !!}
                </p>
            </div>
        </div>
        @endforeach
    </div>
    <div class="arrow bounce">
        <a class="fa  fa-angle-double-down fa-2x" href="#contact-form"></a>
        <p>SCROLL DOWN</p>
    </div>
</div>
<section id="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-12" style="margin-bottom: 60px;">
                <h1 class="section-title text-center">How to reach us</h1>
                <ol class="reachus">
                    @foreach($company_details as $row)
                    <li><i class="zmdi zmdi-phone"></i> {{$row->phone}}</li>
                    <li><i class="zmdi zmdi-smartphone"></i> {{$row->mobile}} </li>
                     @endforeach
                </ol>
            </div>
            <div class="col-12">
                <form action="POST" id="inquiry">
                    <div class="row">
                        <div class="form-col validate-input col-4" data-validate="Name is required">
                            <div class="form-holder">
                                <i class="zmdi zmdi-account-o"></i>
                                <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="" required="">
                                <small class='error-message' id="error-name"></small>
                            </div>
                        </div>
                        <div class="form-col validate-input col-4" data-validate="Email is required">
                            <div class="form-holder">
                                <i class="zmdi zmdi-email"></i>
                                <input type="email" id="email" class="form-control" name="email" placeholder="Email" value="" required="">
                                <small class='error-message' id="error-name"></small>
                            </div>
                        </div>
                        <div class="form-col validate-input col-4" data-validate="Mobile is required">
                            <div class="form-holder">
                                <i class="zmdi zmdi-smartphone"></i>
                                <input type="text" id="mobile" class="form-control" name="mobile" placeholder="Mobile" value="" required="">
                                <small class='error-message' id="error-mobile"></small>
                            </div>
                        </div>
                        <div class="form-col validate-input col-12" data-validate="Mobile is required">
                            <div class="form-holder">
                                <textarea type="text" class="form-control" id="message" name="message" placeholder="Message" value="" required="" style="padding-left: 10px!important; height:200px;"></textarea>
                                <small class='error-message' id="error-message"></small>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary submit" style="font-size: 14px; letter-spacing: 2px;">Send Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="light-background">
    <div class="container">
         
        <div class="faq">
            <h1 class="section-title">FREQUENTLY ASKED QUESTIONS.</h1>

            @foreach($faq_list as $row)

            <p>
                <a class="faq-item" data-toggle="collapse" href="#faq{{$row->id}}" role="button" aria-expanded="false" aria-controls="faq{{$row->id}}">
                    {{$row->question}}
                </a>
            </p>
            <div class="collapse" id="faq{{$row->id}}">
                <div class="card card-body">
                   {!! nl2br(e($row->answer)) !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
@section('script')
<script src="js/contactform.js"></script>
@endsection