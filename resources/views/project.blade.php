@extends('layouts.app')
@section('content')

 @foreach($project_list_per_project as $row)
<div id="pageslider" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active" style="background-image: url('{{asset('img/slider/serene.png')}}')">
        </div>
    </div>
    <div class="arrow bounce">
        <a class="fa  fa-angle-double-down fa-2x" href="#project"></a>
        <p>SCROLL DOWN</p>
    </div>
</div>
<!-- carousel-inner -->

<section id="project">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2>{{$row->project_name}}</h2>
                <ul class="list">
                    <li class="list-item">Location: {{$row->project_location}}</li>
                    <li class="list-item">{!! nl2br(e($row->project_intro)) !!}</li>
                    
                </ul>
            </div>
            <div class="col-md-7">
                <div class="featured-thumb">
                    <video width="100%" preload="auto" autoplay loop muted>
                        >
                        <source src="{{asset('img/projects/serene-interior-design.mp4')}}" width="1024" height="552" type="video/mp4">
                    </video>
    
                </div>
            </div>
        </div>
    </div>
</section>
<section id="model">
    <div class="container">
        <div class="row margin20">
            <div class="col-md-7">
                <div class="featured-thumb">
                    <img src="{{asset('img/projects/valeria.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-md-5 modelunit">
                <h2>VALERIA</h2>
                <div class="row">
                    <div class="col-md-6 col-sm-6 text-center">
                        <div class="features-box">
                            <p style="text-transform: uppercase; font-size:16px; font-weight: 700;"> FLOOR AREA </p>
                            <p style="font-weight: 800; font-size:22px;">
                                92sqm
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 text-center">
                        <div class="features-box">
                            <p style="text-transform: uppercase; font-size:16px; font-weight: 700;"> LOT AREA </p>
                            <p style="font-weight: 800; font-size:22px;">
                                121-175sqm
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 property-feature">
                <h4>FEATURES</h4>
                <div class="row margin20">
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <i class="fa fa-bed"></i>
                            <p>
                                3 Bedrooms
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <i class="fa fa-bed"></i>
                            <p>
                                </i> 1 Maids Room
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <i class="fa fa-bath"></i>
                            <p>
                                3 Toilet and baths
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <i class="fas fa-restroom"></i>
                            <p>
                                1 powder room
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-door-open"></i>
                                <p>
                                    Service Area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-users"></i>
                                <p>
                                    Family Hall
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-umbrella-beach"></i>
                                <p>
                                    Patio
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-couch"></i>
                                <p>
                                    Living Area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-utensils"></i>
                                <p>
                                    Dining Area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-concierge-bell"></i>
                                <p>
                                    Kitchen area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-archway"></i>
                                <p>
                                    Balcony
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-archway"></i>
                                <p>
                                    Carport
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="model">
    <div class="container">
        <div class="row margin20">
            <div class="col-md-5 modelunit">
                <h2>VERBENA</h2>
                <div class="row">
                    <div class="col-md-6 col-sm-6 text-center">
                        <div class="features-box">
                            <p style="text-transform: uppercase; font-size:16px; font-weight: 700;"> FLOOR AREA </p>
                            <p style="font-weight: 800; font-size:22px;">
                                81sqm
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 text-center">
                        <div class="features-box">
                            <p style="text-transform: uppercase; font-size:16px; font-weight: 700;"> LOT AREA </p>
                            <p style="font-weight: 800; font-size:22px;">
                                99-170sqm
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div class="featured-thumb">
                    <img src="{{asset('img/projects/verbena.jpg')}}" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 property-feature">
                <h4>FEATURES</h4>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <i class="fa fa-bed"></i>
                            <p>
                                3 Bedrooms
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <i class="fa fa-bath"></i>
                            <p>
                                2 Toilet and baths
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <i class="fas fa-restroom"></i>
                            <p>
                                1 powder room
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-door-open"></i>
                                <p>
                                    Service Area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-users"></i>
                                <p>
                                    Family Hall
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-couch"></i>
                                <p>
                                    Living Area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-utensils"></i>
                                <p>
                                    Dining Area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-concierge-bell"></i>
                                <p>
                                    Kitchen area
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-archway"></i>
                                <p>
                                    Balcony
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="features-box">
                            <div class="features-content">
                                <i class="fas fa-archway"></i>
                                <p>
                                    Carport
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="portfolioFilter clearfix text-center">
                    <a href="#" data-filter="*" class="current">All</a>
                    <a href="#" data-filter=".playground">Playground</a>
                    <a href="#" data-filter=".pool">Pool</a>
                    <a href="#" data-filter=".park">Park</a>
                    <a href="#" data-filter=".floorplan">Floor Plan</a>
                    <a href="#" data-filter=".vicinity">Vicinity Map</a>
                </div>
            </div>
        </div>
    </div>
    <div id="portfolio-list">
        <div class="col-md-3 col-sm-6 col-xs-12 mix  playground">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/playground.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/playground.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        @foreach($amenities_list as $row)
        <div class="col-md-3 col-sm-6 col-xs-12 mix  playground">
            <div class="portfolio-item">
                <img src="{{asset('/storage/imageGallery/'.$row->amenities_image)}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('/storage/imageGallery/'.$row->amenities_image)}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div>
        @endforeach
<!--         <div class="col-md-3 col-sm-6 col-xs-12 mix pool">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/pool.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/pool.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 mix park">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/park.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/park.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 mix park">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/park1.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/park1.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 mix park">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/park2.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/park2.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 mix  floorplan">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/valeria-floorplan.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/valeria-floorplan.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 mix  floorplan">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/verbena-floorplan.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/verbena-floorplan.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 mix  vicinity">
            <div class="portfolio-item">
                <img src="{{asset('img/gallery/vicinity-map.png')}}" alt="image">
                <div class="overlay">
                    <div class="icon">
                        <a href="{{asset('img/gallery/vicinity-map.png')}}" data-toggle="lightbox"> <i class="fas fa-eye"></i></a>
                    </div>
                </div>

            </div>
        </div> -->
    </div>
    @endforeach
@section('script')
@endsection