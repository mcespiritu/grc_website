
<style type="text/css">
    
input[type=number] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    border: 1px solid #000;
}

</style>
<div class="modal" id="addLogo">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Upload Partner Logo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="uploadpartnerForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                    <section>
                                <div class="form-group files">
                                    <label>Upload your Slider</label>
                            
                                    <input type="file" name="logo[]" id="logo" class="form-control">
                                </div>

                                 <div class="form-group">
                                    <label>Link</label>
                                    <input type="text" name="link" id="link" class="form-control">
                                </div>
                    </section>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript">
    
    
</script>