
<div class="modal" id="addCareerbanner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Career Slider</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
                <form id="createCareerBanner" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title">
                        </div>
                        <div class="input-field">
                            <label for="subtitle">Subtitle</label>
                            <textarea class="form-control" id="subtitle" name="subtitle" rows="8"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Browse… <input type="file" name="banner_filename[]" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly hidden>
                            </div>
                            <div class="col-offset-lg-3 col-lg-6 mx-auto "><img id="img-upload" class="img-fluid"></div>
                        </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                 <button type="submit" id="submitBanner" class="btn btn-outline-success btn-circle btn-md">Submit</button>
            </div>
                </form>

        </div>
    </div>
</div>
