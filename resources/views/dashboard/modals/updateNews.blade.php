<style>
.column {
  float: left;
  width: 50%;
}
.column:first-child {
margin-right:20px;
}
.datepicker {
    background: #eee;
    width: 300px;
}
.datepicker table{
    width: 100%;
}
.bootstrap-datetimepicker-widget.dropdown-menu.top {
    background: #eee;
    width: 200px;
}
.timepicker-picker table {
    width: 100%;
}
span.timepicker-hour, span.timepicker-minute {
    padding: 10px;
}

</style>
<div class="modal" id="updateNews">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update News</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="updateNewsForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="title">News Title</label>
                            <input type="text" name="title" id="title">
                            <input type="hidden" class="form-control" id="id" name="id" readonly>
                        </div>
                        <div class="form-group">
                            <label>News Date</label>
                            <div class="input-group publish_date" id="publish_date">
                                <input class="form-control" id="publish_date" name="publish_date">
                                <span class="input-group-append input-group-addon">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Featured?</label>
                            <select class="form-control" id="featured" name="featured">
                                <option>Please Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Browse… <input type="file" name="thumbnail[]" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" hidden readonly>
                            </div>
                            <div class="col-offset-lg-3 col-lg-6 mx-auto">
                                <div class="img-fluid img-upload"></div>
                            </div>
                        </div>
                        <div class="input-field">
                            <label for="content">Content</label>
                            <textarea class="form-control" id="content" name="content" rows="10"></textarea>
                        </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
$("#publish_date").datetimepicker({
    useCurrent: false,
    format: "LL",
    showTodayButton: true,
    icons: {
      next: "fa fa-chevron-right",
      previous: "fa fa-chevron-left",
      today: 'todayText',
    },
    debug: true
  });
</script>