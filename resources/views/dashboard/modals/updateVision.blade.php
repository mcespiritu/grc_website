
<div class="modal" id="updateVision">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update About Slider</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
                <form id="updateVisionForm" method="post" enctype="form-data/multipart">
            <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="vision">Vision</label>
                           <textarea class="form-control" id="vision_text" name="vision" rows="5"></textarea>
                            <input type="hidden" class="form-control" id="id" name="id"  readonly>
                        </div>
                        <div class="input-field">
                            <label for="mission">Mission</label>
                           <textarea class="form-control" id="mission_text" name="mission" rows="5"></textarea>
                        </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                 <button type="submit" id="submitBanner" class="btn btn-outline-success btn-circle btn-md">Submit</button>
            </div>
                </form>

        </div>
    </div>
</div>
