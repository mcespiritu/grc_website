<?php $project_name_list[""] = "Select Project";
   foreach ($project_list as $key => $value) 
   {
       $project_name_list[$value->id] = $value->project_name ;
   }
?>

<?php $property_features_list[""] = "Select Features";
   foreach ($feature_list as $key => $value) 
   {
       $property_features_list[$value->id] = $value->name;
   }
?>

   <div class="modal" id="addUnit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Model Unit</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <form id="createModelUnitForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Browse… <input type="file" name="display_filename[]" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly hidden>
                            </div>
                            <div class="col-offset-lg-3 col-lg-6 mx-auto">
                                <img id="img-upload" class="img-fluid">
                            </div>
                        </div>
                        <div class="input-field">
                            <label for="about_subtitle">Project</label>
                            <?php echo Form::select('project_id', $project_name_list , null, array('class' => 'form-control', 'id' => 'project_id', 'name' => 'project_id')); ?>
                        </div>
                        <div class="input-field">
                            <label for="model_name">Model Unit</label>
                            <input type="text" name="model_name" id="model_name">
                        </div>
                        <div class="input-field">
                            <label for="floor_area">Floor Area</label>
                            <input type="text" name="floor_area" id="floor_area">
                        </div>
                        <div class="input-field">
                            <label for="lot_area">Lot Area</label>
                            <input type="text" name="lot_area" id="lot_area">
                        </div>
                        <table class="table table-hover" id="featureTable">
                          <label for="feature">Features</label>
                          <tr class="featureRow">
                           <td width="20%">
                              <input type="text" class="form-control" id="feature_value" name="feature_value[]" placeholder="0" value="0">
                           </td>
                           <td width="80%">
                    <?php echo Form::select('feature_id', $property_features_list , null, array('class' => 'form-control', 'id' => 'feature_id', 'name' => 'feature_id[]')); ?>
                           </td>
                           <td>
                              <center>
                                 <button class="btn btn-danger remove" type="button"><i class="fa fa-trash"></i></button>
                              </center>
                           </td>
                        </tr>
                        <tr id="addButtonRow">
                           <td colspan="3">
                              <center>
                                 <button class="btn btn-success add" type="button">Add Feature</button>
                              </center>
                           </td>
                        </tr>
                      </table>
                        <script type="text/javascript">
                           /* Variables */
                           var p = $("#participants").val();
                           var row = $(".featureRow");
                           
                           /* Functions */
                           function getP() {
                               p = $("#participants").val();
                           }
                           
                           function addRow() {
                               row.clone(true, true).appendTo("#featureTable");
                           }
                           
                           function removeRow(button) {
                               button.closest("tr").remove();
                           }
                           /* Doc ready */
                           $(".add").on('click', function() {
                               getP();
                               if ($("#featureTable tr").length < 99) {
                                   addRow();
                                   var i = Number(p) + 1;
                                   $("#participants").val(i);
                               }
                               $(this).closest("tr").appendTo("#featureTable");
                               if ($("#featureTable tr").length === 3) {
                                   $(".remove").hide();
                               } else {
                                   $(".remove").show();
                               }
                           });
                           $(".remove").on('click', function() {
                               getP();
                               if ($("#featureTable tr").length === 3) {
                                   //alert("Can't remove row.");
                                   $(".remove").hide();
                               } else if ($("#featureTable tr").length - 1 == 3) {
                                   $(".remove").hide();
                                   removeRow($(this));
                                   var i = Number(p) - 1;
                                   $("#participants").val(i);
                               } else {
                                   removeRow($(this));
                                   var i = Number(p) - 1;
                                   $("#participants").val(i);
                               }
                           });
                           $("#participants").change(function() {
                               var i = 0;
                               p = $("#participants").val();
                               var rowCount = $("#featureTable tr").length - 2;
                               if (p > rowCount) {
                                   for (i = rowCount; i < p; i += 1) {
                                       addRow();
                                   }
                                   $("#featureTable #addButtonRow").appendTo("#featureTable");
                               } else if (p < rowCount) {}
                           });
                        </script>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript">
$("#createModelUnitForm").on('submit', function(e) {
    var post_data = new FormData(this);
    simPostUpload(post_data, 'POST', '/dashboard/createModelUnit', createModelUnitResponse);
    e.preventDefault();
    console.log(post_data);
    //     setTimeout(function(){
    //    window.location.reload(1);
    // }, 1000);
    // return false;
});

function createModelUnitResponse(response) {
    if (response == true) {
        swal("Success!", "Model Unit has been added", "success");
        $('#addUnit').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').removeClass('has-error');
    } else {
        swal("Error!", "Model Unit not saved", "error");
        $('#addUnit').modal('hide');
        $('.modal-message').html('');
        $('.error-message').html(""); //reset messages
        $('.form-group').addClass('has-error');
    }
}

</script>
