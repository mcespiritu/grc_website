<div class="modal" id="addBuyerContent">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Buyer's Corner Content</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="addBuyerContentForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="input-field">
                        <label for="content">Content</label>
                        <textarea class="form-control" id="content" name="content" rows="8"></textarea>
           
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBanner" class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>