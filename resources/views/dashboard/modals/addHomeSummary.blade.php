
<div class="modal" id="addHomeSummary">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Company Summary</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
                <form id="addHomeSummaryForm" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="summary_title">Title</label>
                            <input type="text" name="summary_title" id="summary_title">
                        </div>
                        <div class="input-field">
                            <label for="summary_content">Content</label>
                            <input type="text" name="summary_content" id="summary_content">
                        </div>

                        <div class="form-group files">
                            <label>Upload Your File </label>
                            <input type="file" name="display_filename[]" id="display_filename" class="form-control" multiple="">
                          </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                 <button type="submit" class="btn btn-outline-success btn-circle btn-md">Submit</button>
            </div>
                </form>

        </div>
    </div>
</div>
