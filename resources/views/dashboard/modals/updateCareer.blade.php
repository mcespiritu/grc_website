
<?php
    $category_list_data[""] = "Select Code";
    foreach ($category_list as $key => $value) 
    {
        $category_list_data[$value->id] = $value->categories;
    }
?> <div class="modal" id="updateCareer">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Job Post</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="updateCareerForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                                        <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="job_title">Job Title</label>
                            <input type="text" name="job_title" id="job_title">
                            <input type="hidden" class="form-control" id="id" name="id" readonly>
                        </div>
                        <div class="input-field">
                            <label for="industry">Category</label>
                             <?php echo Form::select('categories', $category_list_data , null, array('class' => 'form-control categories', 'id' => 'categories')); ?>
                        </div>
                        <div class="input-field">
                            <label for="job_desc">Job Description</label>
                             <textarea class="form-control" id="job_desc" name="job_desc" rows="10"></textarea>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>