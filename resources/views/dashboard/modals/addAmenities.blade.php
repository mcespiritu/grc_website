<div class="modal" id="addAmenities">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Feature</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="addAmenitiesForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name">
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>