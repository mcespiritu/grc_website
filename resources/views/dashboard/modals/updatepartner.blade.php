
<div class="modal" id="updatePartner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update About Slider</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
        <form id="updatePartnerForm" method="post" enctype="form-data/multipart">
            <div class="modal-body">
                    <?php echo csrf_field(); ?>

                           <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Browse… <input type="file" name="logo[]" id="logo" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" hidden readonly>
                            </div>
                            <div class="col-offset-lg-3 col-lg-6 mx-auto">
                                <div class="img-fluid img-upload"></div>
                            </div>
                        </div>
                        <div class="input-field">
                            <label for="link">Link</label>
                            <input type="text" class="form-control" id="link" name="link">
                             <input type="hidden" class="form-control" id="id" name="id"  readonly>
                        </div>

            </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                     <button type="submit" class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
                </form>

        </div>
    </div>
</div>
