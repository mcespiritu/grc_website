<?php 
  $department_list_data[""] = "Select Department"; 
  foreach( $department_list as $key => $value )
  {
    $department_list_data[$value->id] = $value->dept_name;
  }
?>

<style type="text/css">
    
input[type=number] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    border: 1px solid #000;
}

</style>
<div class="modal" id="addOrgChart">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Position</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="addOrgChartForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name">
                        </div>
                        <div class="input-field">
                            <label for="jobtitle">Job Position</label>
                            <input type="text" name="jobtitle" id="jobtitle">
                        </div>
                        <div class="input-field">
                            <label for="department">Department</label>
                            <?php echo Form::select('dept_id', $department_list_data , null, array('class' => 'form-control', 'id' => 'dept_id')); ?>
                        </div>
                       <div class="input-field">
                            <label for="rank">Rank</label>
                            <input type="number" name="rank" id="rank" placeholder="Eg. 1">
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>