<div class="modal" id="addBuyerbanner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Buyer's Corner Banner</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="createBuyerBannerForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="buyer_title">Title</label>
                            <input type="text" name="buyer_title" id="buyer_title">
                        </div>
                        <div class="input-field">
                            <label for="buyer_subtitle">Subtitle</label>
                            <textarea class="form-control" id="buyer_subtitle" name="buyer_subtitle" rows="8"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Browse… <input type="file" name="display_filename[]" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly hidden>
                            </div>
                            <div class="col-offset-lg-3 col-lg-6 mx-auto">
                                <img id="img-upload" class="img-fluid">
                            </div>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>