
<div class="modal" id="addProject">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <h4 class="modal-title">Add Project</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <!-- Modal body -->
         <form id="createProject" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div>
               <h3>Project</h3>
               <section>
                  <div class="row">
                     <div class="col-md-6">
                        <label>Featured?</label>
                        <select class="form-control" id="featured" name="featured" style="margin-bottom: 20px;">
                           <option>Please Select</option>
                           <option value="Yes">Yes</option>
                           <option value="No">No</option>
                        </select>
                     </div>
                     <div class="col-md-6">
                        <label>Status</label>
                        <select class="form-control" id="status" name="status" style="margin-bottom: 20px;">
                           <option>Please Select</option>
                           <option value="1">On-going</option>
                           <option value="2">Completed</option>
                           <option value="3">Sold Out</option>
                        </select>
                     </div>
                  </div>
                  <div class="input-field">
                     <label for="project_name">Project Name</label>
                     <input type="text" class="form-control" name="project_name" id="project_name">
                  </div>
                  <div class="form-group">
                     <label for="project_summary">Project Summary</label>
                     <textarea class="form-control" id="project_summary" name="project_summary" rows="5"></textarea>
                  </div>
                  <div class="form-group files">
                     <label>Upload your Walkthrough Video</label>
                     <input type="file" name="display_filename[]" id="display_filename" class="form-control" multiple>
                  </div>
               </section>
               <h3>Slider</h3>
               <section>
                  <div class="form-group files">
                     <label>Upload your Slider</label>
                     <input type="file" name="banner[]" id="banner" class="form-control" multiple>
                  </div>
               </section>
               <h3>Introduction</h3>
               <section>
                  <div class="form-group">
                     <label for="project_intro">Introduction</label>
                     <textarea class="form-control" id="exampleFormControlTextarea1" name="project_intro" rows="15"></textarea>
                  </div>
               </section>
    
               <h3>Vicinity Map</h3>
               <section>
                  <div class="form-group">
                     <label for="project_location">Project Location</label>
                     <input type="text" name="project_location" id="project_location">
                  </div>
                  <div class="form-group files">
                     <label>Upload your Vicinity Map</label>
                     <input type="file" name="vicinity_map" id="vicinity_map" class="form-control">
                  </div>
               </section>
               <h3>Amenities</h3>
               <section>
                  <div class="form-group files">
                     <label>Upload your Images</label>
                     <input type="file" name="amenities_image[]" id="amenities_image" class="form-control" multiple>
                  </div>
                  <button type="submit" id="submitProject" class="btn btn-block btn-success">Submit</button>
               </section>
            </div>
         </form>
      </div>
   </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.steps.min.js') }}"></script>
<script src="{{ asset('js/module/project.js') }}"></script>