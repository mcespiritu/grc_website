<div class="modal" id="updateHomeSummary">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Company Summary</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="updateHomeSummary" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="input-field">
                        <label for="summary_title">Summary Title</label>
                        <input type="text" name="summary_title" id="summary_title">
                        <input type="hidden" class="form-control" id="id" name="id" readonly>
                    </div>
                    <div class="input-field">
                        <label for="summary_content">Summary Content</label>
                        <textarea class="form-control" id="summary_content" name="summary_content" rows="8"></textarea>
                    </div>
                    <div class="input-field">
                        <label> Video Link</label>
                        <input type="text" name="video_link" id="video_link">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBanner" class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>