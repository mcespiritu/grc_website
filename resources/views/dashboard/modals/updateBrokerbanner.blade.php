<div class="modal" id="updateBrokerBanner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Broker Banner</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="updateBrokerBannerForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="broker_title">Title</label>
                            <input type="text" name="broker_title" id="broker_title">
                             <input type="hidden" class="form-control" id="id" name="id" readonly>
                        </div>
                        <div class="input-field">
                            <label for="broker_subtitle">Subtitle</label>
                            <textarea class="form-control" id="broker_subtitle" name="broker_subtitle" rows="8"></textarea>
                        </div>

                           <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Browse… <input type="file" name="display_filename[]" id="display_filename" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" hidden readonly>
                            </div>
                            <div class="col-offset-lg-3 col-lg-6 mx-auto">
                                <div class="img-fluid img-upload"></div>
                            </div>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>