<div class="modal" id="updateBuyerBanner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Buyer Slider</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="updateBuyerBannerForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="buyer_title">Title</label>
                            <input type="text" name="buyer_title" id="buyer_title">
                            <input type="hidden" class="form-control" id="id" name="id" readonly>
                        </div>
                        <div class="input-field">
                            <label for="buyer_subtitle">Subtitle</label>
                            <textarea class="form-control" id="buyer_subtitle" name="buyer_subtitle" rows="8"></textarea>
                        </div>

                           <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Browse… <input type="file" name="display_filename[]" id="display_filename" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" hidden readonly>
                            </div>
                            <div class="col-offset-lg-3 col-lg-6 mx-auto">
                                <div class="img-fluid img-upload"></div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBanner" class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>