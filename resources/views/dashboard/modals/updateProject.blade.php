<div class="modal" id="addProject">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Project</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <form id="createProject" method="post" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                    <div>
                        <h3>Slider</h3>
                        <section>
                    <label>Featured?</label>
                            <select class="form-control" id="featured" name="featured">
                              <option>Please Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                            <div class="input-field">
                                <label for="project_name">Project Name</label>
                                <input type="text" class="form-control" name="project_name" id="project_name">

                            </div>
                            <div class="form-group">
                                <label for="project_summary">Project Summary</label>
                                <textarea class="form-control" id="project_summary" name="project_summary" rows="2"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Upload your Sliders</label>
                                <input type="file" name="display_filename" id="display_filename" class="form-control">
                            </div>
                        </section>
                        <h3>Introduction</h3>
                        <section>
                            <div class="form-group">
                                <label for="project_intro">Introduction</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="project_intro" rows="15"></textarea>
                            </div>
                        </section>
                        <h3>Project Details</h3>
                        <section>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="floor_area">Floor Area</label>
                                    <input type="number" name="floor_area" id="floor_area">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="gross_floor_area">Gross Floor Area</label>
                                    <input type="number" name="gross_floor_area" id="gross_floor_area">

                                </div>
                                <div class="form-group col-md-3">
                                    <label for="toilets_baths">Bedroom</label>
                                    <input type="number" name="toilets_baths" id="toilets_baths">

                                </div>
                                <div class="form-group col-md-3">
                                    <label for="project_name">Toilet and Baths</label>
                                    <input type="number" name="bedroom" id="bedroom">

                                </div>

                                <div class="form-group col-md-3">
                                    <label for="service_area">Service Area</label>
                                    <input type="number" name="service_area" id="service_area">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="carport_area">Carport Area</label>
                                    <input type="number" name="carport_area" id="carport_area">
                                </div>

                               
                        </section>

                        <h3>Vicinity Map</h3>
                        <section>
                         
                            <div class="form-group">
                                <label for="project_location">Project Location</label>
                                <input type="text" name="project_location" id="project_location">
                            </div>

                            <div class="form-group files">
                                <label>Upload your Vicinity Map</label>
                                <input type="file" name="vicinity_map" id="vicinity_map" class="form-control">
                            </div>
                    
                        </section>

                        <h3>Amenities</h3>
                        <section>
                            <div class="form-group files">
                                <label>Upload your Images</label>
                                <input type="file" name="amenities_image[]" id="amenities_image" class="form-control" multiple>
                                    <div class="row">
                                        @foreach($amenities_list as $row)
                                        <a href="{{asset('/storage/imageGallery/'.$row->amenities_image)}}"
                                        class="glightbox gallery_product col-6 col-lg-4 col-md-4 col-sm-4 col-xs-6 filter">
                                            <img src="{{asset('/storage/imageGallery/'.$row->amenities_image)}}">
                                        </a>
                                      @endforeach
                                  </div>
                            </div>
                        <button type="submit" id="submitProject" class="btn btn-block btn-success">Submit</button>
                        </section>


                        </div>

            </form>
            </div>
        </div>
    </div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.steps.min.js') }}"></script>
<script src="{{ asset('js/module/project.js') }}"></script>
<script type="text/javascript">
    var form = $("#createProject");
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            $("a[href='#finish']").css("display", "none");
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();

        },
        onFinished: function(event, currentIndex) {
            //$("img").attr("width","500");
            // /* alert(form);
            //  // var form = $("#createProject");
            // var post_data = new FormData(form);
            // var post_data = $('#createProject').serialize();
            // simPostUpload(post_data, 'POST', '/dashboard/createProject', createProjectResponse);
            // return false;

            //   var post_data = new FormData('#createProject');
            // submitProject(post_data);
        }
    });
    // function submitProject() {
    //     // var post_data = $('#createProject').serialize();
    //     var post_data = new FormData(this);
    //     simPostUpload(post_data, 'POST', '/dashboard/createProject', createProjectResponse);
    //     e.preventDefault();
    //     return false;
    // }

//     $('#createProject').on('submit', function(e) {
//         $('.modal-message').html('');
//         $('.error-message').html(""); //reset messages
//         $('.form-group').removeClass('has-error');
//         var post_data = new FormData(form);
//         simPostUpload(post_data, 'POST', '/dashboard/createProject', createProjectResponse);
//         return false;
//     });

//    function createProjectResponse(response) {
//  swal("Success!", "New Project has been added", "success"); 
//     $('#addProject').modal('hide');
//     $('.modal-message').html('');
//     $('.error-message').html(""); //reset messages
//     $('.form-group').removeClass('has-error');

// }
</script>