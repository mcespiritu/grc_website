<div class="modal" id="updateBuyerContent">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Buyer's Corner Content</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="updateBuyerContentForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                     <?php echo csrf_field(); ?>
                    <div class="input-field">
                        <label for="content">Content</label>
                        <textarea class="form-control" id="content" name="content" rows="8"></textarea>
                        <input type="hidden" class="form-control" id="id" name="id" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBanner" class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>