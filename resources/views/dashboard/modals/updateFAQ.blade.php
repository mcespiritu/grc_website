
 <div class="modal" id="updateFAQ">
    <div class="modal-dialog modal-lgs">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update FAQ</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="updateFAQForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="question">Question</label>
                            <input type="text" name="question" id="question">
                             <input type="hidden" class="form-control" id="id" name="id"  readonly>
                        </div>
                        <div class="input-field">
                            <label for="answer">Answer</label>
                             <textarea class="form-control" id="answer" name="answer" rows="10"></textarea>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>