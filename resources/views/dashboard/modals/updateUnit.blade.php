
<?php $project_name_list[""] = "Select Project";
   foreach ($project_list as $key => $value) 
   {
       $project_name_list[$value->id] = $value->project_name ;
   }
?>
<div class="modal" id="updateUnit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Job Post</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="updateModelUnitForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="about_subtitle">Project</label>
                            <?php echo Form::select('project_id', $project_name_list , null, array('class' => 'form-control', 'id' => 'project_id', 'name' => 'project_id')); ?>
                        </div>
                        <div class="input-field">
                            <label for="model_name">Model Unit</label>
                            <input type="text" name="model_name" id="model_name">
                        </div>
                        <div class="input-field">
                            <label for="floor_area">Floor Area</label>
                            <input type="text" name="floor_area" id="floor_area">
                        </div>
                        <div class="input-field">
                            <label for="lot_area">Lot Area</label>
                            <input type="text" name="lot_area" id="lot_area">
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-outline-success btn-circle btn-md">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script src="{{ asset('js/module/project.js') }}"></script>