
<div class="modal" id="updateCompanyDetails">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Company Details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
                <form id="updateCompanyDetailsForm" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                    <?php echo csrf_field(); ?>
                        <div class="input-field">
                            <label for="company_name">Company Name</label>
                            <input type="text" name="company_name" id="company_name">
                            <input type="hidden" class="form-control" id="id" name="id" readonly>
                        </div>
                        <div class="input-field">
                            <label for="address">Address</label>
                            <input type="text" name="address" id="address">
                        </div>
                        <div class="input-field">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="phone">
                        </div>
                        <div class="input-field">
                            <label for="mobile">Mobile</label>
                            <input type="text" name="mobile" id="mobile">
                        </div>
                        <div class="input-field">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email">
                        </div>
                        <div class="input-field">
                            <label for="social-media">Social Media</label>
                            <input type="text" class="mb-3" name="facebook" id="facebook" placeholder="Facebook Link">
                            <input type="text" class="mb-3" name="twitter" id="twitter" placeholder="Twitter Link">
                            <input type="text" class="mb-3" name="instagram" id="instagram" placeholder="Instagram Link">
                            <input type="text" class="mb-3" name="youtube" id="youtube" placeholder="Youtube Link">
                            <input type="text" class="mb-3" name="linkedin" id="linkedin" placeholder="LinkedIn Link">
                        </div>

                        <div class="input-field">
                            <label for="social-media">Social Media</label>
                            <input type="file" name="partnerlogo[]" id="partnerlogo" class="form-control" multiple>
                        </div>

                      
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                 <button type="submit" id="submitBanner" class="btn btn-outline-success btn-circle btn-md">Submit</button>
            </div>
                </form>

        </div>
    </div>
</div>
