@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="py-5">

        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Project List</h2>
                        <div class="float-right"><a class=" btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addProject"><i class="fas fa-plus"></i></a></div>
                    </div>
                    <div class="card-body">
                        <div class="card-deck">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Project Name</th>
                                        <th scope="col">Location</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($project_list as $row)
                                    <tr>
                                        <th scope="row">{{$row->id}}</th>
                                        <td>{{$row->project_name}}</td>
                                        <td>{{$row->project_location}}</td>
                                        <td><a href="/updateProjectPage/{{$row->id}}" class="btn btn-info btn-border btn-round mr-2 btn-sm pull-right"><i class="fas fa-pencil-alt"></i></a></td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript">
$("#createProject").on('submit', function(e)
{
  var post_data = new FormData(this);
  simPostUpload(post_data, 'POST', '/dashboard/createProject', createProjectResponse);
  e.preventDefault();
    setTimeout(function(){
    window.location.reload(1);
    }, 1000);
});

function createProjectResponse(response) {
 swal("Success!", "New Project has been added", "success"); 
    $('#addProject').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');

}

</script>
@endsection