@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">News</h2>
                         <div class="float-right"><a class="addNews btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addNews"><i class="fas fa-plus"></i></a></div>
                     </div>
                    <div class="card-body">
                        <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">News title</th>
                                        <th scope="col">Publish Date</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @foreach($news_list as $row)
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->title}}</td>
                                        <td>{{$row->publish_date}}</td>
                                        <td><a class="updateNewsClass btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateNews" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a><a class="deleteNews btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
          
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('dashboard.modals.addNews')
@include('dashboard.modals.updateNews')
<script type="text/javascript" src="{{ URL::asset('/js/module/news.js') }}"></script>
@endsection