
@extends('dashboard.layouts.app') 
@section('content')
@php 
    $property_features_list = [];
    foreach ($feature_list as $key => $value) 
    {
        $property_features_list[$value->id] = $value->name;
    }
@endphp
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
        <!-- Area Chart Example-->
        <div class="card mb-3">
            <div class="card-header">
                <h2 class="h6 text-uppercase mb-0 float-left">Update Project Details</h2>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <form id="updateProjectForm" method="POST" enctype="multipart/form-data" style="width:100%">
                        <?php echo csrf_field(); ?>
                            <div>
                                <div class="col-md-12">
                                    <input type="hidden" class="form-control" id="id" name="id" value="{{ $id }}" readonly="">
                                    <div class="row">
                                      <div class="col-md-6">
                                            <label>Featured?</label>
                                            <select class="form-control" id="featured" name="featured" style="margin-bottom: 20px;">
                                              <option>Please Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Status</label>
                                            <select class="form-control" id="status" name="status" style="margin-bottom: 20px;">
                                              <option>Please Select</option>
                                              <option value="1">On-going</option>
                                              <option value="2">Completed</option>
                                              <option value="3">Sold Out</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field">
                                        <label for="project_name">Project Name</label>
                                        <input type="text" class="form-control" name="project_name" id="project_name">
                                    </div>
                                                                            <div class="form-group">
                                            <label for="project_location">Project Location</label>
                                            <input type="text" name="project_location" id="project_location">
                                        </div>
                                    <div class="form-group">
                                        <label for="project_summary">Project Summary</label>
                                        <textarea class="form-control" id="project_summary" name="project_summary" rows="8"></textarea>
                                    </div>
                                        <div class="form-group">
                                                <label>Upload your Sliders</label>

                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                    <span class="btn btn-info btn-file">
                                                        Browse… <input type="file" name="banner[]" id="imgInp" multiple>
                                                    </span>
                                                    </span>
                                                    <input type="text" class="form-control" hidden readonly>
                                                </div>

                                                <div class="card-body">
                                                    <div class="card-deck">
                                                        <div class="row">
                                                            @foreach($banner_list as $row)
                                                            

                                                            <div class=" col-md-3">

                                                                <img src="/storage/imageGallery/{{$row->banner}}" class="img-fluid" alt="...">
                                                                <div class="card-body">
                                                                    <p class="float-right">
                                                                        <a class="deleteBannerImage btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></p>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                            <div class="col-md-12">
                                     <div class="input-field">
                                        <label for="walkthrough">Walkthrough Video</label>
                                        <input type="text" class="form-control" name="walkthrough" id="walkthrough">
                                    </div>
                                </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="project_intro">Introduction</label>
                                            <textarea class="form-control" id="project_intro" name="project_intro" rows="15"></textarea>

                                        </div>
                                    </div>

                          <div class="col-md-12">
                            <table class="table table-hover" id="featureTable">
                                <thead>
                                    <tr>
                                        <th colspan="3">Features</th>
                                    </tr>
                                </thead>

                                @foreach($feature_list_per_project as $row)
                                <tr class="featureRow">
                                    <td width="20%">
                                      <input type="hidden" class="form-control" id="feat_id" name="feat_id[]"  value="{{$row->id}}">
                                      <input type="text" class="form-control" id="feature_value" name="feature_value[]"   placeholder="0" value="{{$row->value}}">
                                    </td>
                                    <td width="80%">
                                    @php 
                                      echo Form::select('feature_id', $property_features_list , $row->feature, ['class' => 'form-control', 'id' => 'feature_'.$row->feature, 'name' => 'feature_id[]']); 
                                    @endphp

                                    </td>
                                    <td>
                                      <center>
                                        <button class="btn btn-rye btn-danger remove" type="button" id="{{$row->id}}"><i class="fa fa-trash"></i></button>
                                      </center>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                                    </div>
                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label>Upload Vicinity Map</label>
                                            <div class="input-group">
                                              <span class="input-group-btn">
                                                <span class="btn btn-info btn-file">
                                                    Browse… <input type="file" name="vicinity_map[]" id="imgInp">
                                                </span>
                                              </span>
                                              <input type="text" class="form-control" hidden readonly>
                                            </div>
                                            <div class="col-offset-lg-3 col-lg-6 mx-auto">
                                                <div id="img-upload-vicinity" class="img-fluid">
                                                </div>
                                            </div>

                                        </div>
                                     </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Upload Amenities</label>

                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                    <span class="btn btn-info btn-file">
                                                        Browse… <input type="file" name="amenities_image[]" id="imgInp" multiple>
                                                    </span>
                                                    </span>
                                                    <input type="text" class="form-control" hidden readonly>
                                                </div>

                                                <div class="card-body">
                                                    <div class="card-deck">
                                                        <div class="row">
                                                            @foreach($amenities_list as $row)
                                                            

                                                            <div class=" col-md-3">

                                                                <img src="/storage/imageGallery/{{$row->amenities_image}}" class="img-fluid" alt="...">
                                                                <div class="card-body">
                                                                    <p class="float-right">
                                                                        <a class="deleteAmenitiesImage btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></p>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="submit" id="submitProject" class="btn btn-block btn-success">Submit</button>
                                        </div>
                                    </div>
                               
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<script src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript">

     /* Variables */
var p = $("#participants").val();
var row = $(".featureRow");

/* Functions */
function getP(){
  p = $("#participants").val();
}

function addRow() {
  row.clone(true, true).appendTo("#featureTable");
}

function removeRow(button) {
  button.closest("tr").remove();
}
/* Doc ready */
$(".add").on('click', function () {
  getP();
  if($("#featureTable tr").length < 99) {
    addRow();
    var i = Number(p)+1;
    $("#participants").val(i);
  }
  $(this).closest("tr").appendTo("#featureTable");
  if ($("#featureTable tr").length === 3) {
    $(".remove").hide();
  } else {
    $(".remove").show();
  }
});
$(".remove").on('click', function (e) {
  // getP();
   var id = this.id;
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this Feature?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

                if (isConfirm) {
                  
                    simPost({id: id}, 'POST', '/dashboard/deleteFeature', removeFeatureResponse);
                    e.preventDefault();
                    swal("Success!", "Feature has been deleted", "success");
                    setTimeout(function() {
                        // window.location.reload(1);
                    }, 1000);
                } else {
                    swal("Error!", "You unsucessfully deleted the Feature!", "error");
                }
            });

        return false;

});
$("#participants").change(function () {
  var i = 0;
  p = $("#participants").val();
  var rowCount = $("#featureTable tr").length - 2;
  if(p > rowCount) {
    for(i=rowCount; i<p; i+=1){
      addRow();
    }
    $("#featureTable #addButtonRow").appendTo("#featureTable");
  } else if(p < rowCount) {
  }
});

$( document ).ready(function() {
    var id = $('input#id').val();
    simPost({id:id}, 'POST', '/getProjectDataForEditing', getProjectDataForEditingResponse);

});
function removeFeatureResponse(response) {
    if (response == '') {
        window.setInterval(function() {
            location.reload();
        }, 1200);
        swal("Success!", "You have deleted the Feature!", "success");
        return false;
    }
}
function getProjectDataForEditingResponse(response)
{
  
    $.each(response, function(key, value) {
        console.table(response)
        $("input#id").val(value.id);
        $("select#featured").val(value.featured);
        $("select#status").val(value.status);
        $("input#project_name").val(value.project_name);
        $("textarea#project_summary").val(value.project_summary);
        $("textarea#project_intro").val(value.project_intro);
        $("input#floor_area").val(value.floor_area);
        $("input#gross_floor_area").val(value.gross_floor_area);
        $("input#toilets_baths").val(value.toilets_baths);
        $("input#bedroom").val(value.bedroom);
        $("input#service_area").val(value.service_area);
        $("input#carport_area").val(value.carport_area);
        $("input#project_location").val(value.project_location);
        $("input#walkthrough").val(value.walkthrough);
        $("input#feature_value"+value.value).val(value.value);
        $("select#feature_"+value.feature).val(value.feature);
        $("#img-upload").append('' +
        '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.display_filename+'" alt="'+value.title+'" />'); 
        $("#img-upload-vicinity").append('' +
        '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.vicinity_map+'" alt="'+value.title+'" />'); 
        $("#img-upload-amenities").append('' +
        '<img id="img-upload" class="img-fluid" src="/storage/imageGallery/'+value.amenities_image+'" alt="'+value.title+'" />'); 
    });
}

$("#updateProjectForm").on('submit', function(e)
{
  var post_data = new FormData(this);
  simPostUpload(post_data, 'POST', '/dashboard/updateProject', updateProjectResponse);
  e.preventDefault();

    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
});

function updateProjectResponse(response) {
 swal("Success!", "Project has been updated", "success"); 
    $('#addProject').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
}


$(".deleteAmenitiesImage").on('click', function (e) {
   var id = this.id;
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this Feature?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

                if (isConfirm) {
                  
                    simPost({id:id}, 'POST', '/dashboard/deleteAmenitiesImage', deleteAmenitiesResponse);
                    e.preventDefault();
                    swal("Success!", "You have deleted an Amenities Image!", "success");
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 1000);
                } else {
                    swal("Error!", "You unsucessfully deleted an Amenities Image!", "error");
                }
            });

        return false;
});


function deleteAmenitiesResponse( response ) 
{
  if(response=="1")
  {
    window.setInterval(function(){
      location.reload();
    }, 1200);
    swal("Success!", "You have deleted an Amenities Image!", "success");
  }
  else 
  {
   swal("Error!", "You unsucessfully deleted an Amenities Image!", "error");
  } 

  return false;
}

$(".deleteBannerImage").on('click', function (e) {
   var id = this.id;
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this Banner?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

                if (isConfirm) {
                  
                    simPost({id:id}, 'POST', '/dashboard/deleteBannerImage', deleteBannerResponse);
                    e.preventDefault();
                    swal("Success!", "You have deleted an Banner Image!", "success");
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 1000);
                } else {
                    swal("Error!", "You unsucessfully deleted an Banner Image!", "error");
                }
            });

        return false;
});


function deleteBannerResponse( response ) 
{
  if(response=="1")
  {
    window.setInterval(function(){
      location.reload();
    }, 1200);
    swal("Success!", "You have deleted an Banner Image!", "success");
  }
  else 
  {
   swal("Error!", "You unsucessfully deleted an Banner Image!", "error");
  } 

  return false;
}
</script>
@endsection