@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Event</h2>
                        <div class="float-right"><a class="addEvent btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addEvent"><i class="fas fa-plus"></i></a></div>
                    </div>
                    <div class="card-body">
                        <table class="table" width="100%">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Event Name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Start </th>
                                    <th scope="col">End </th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($event_list as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td>{{$row->event_name}}</td>
                                    <td>{{$row->event_date}}</td>
                                    <td>{{$row->event_start}}</td>
                                    <td>{{$row->event_end}}</td>
                                    <td>
                                        <a class="updateEventClass btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateEvent" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a><a class="deleteEvent btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('dashboard.modals.updateEvent')
@include('dashboard.modals.addEvent')

<script type="text/javascript" src="{{ URL::asset('/js/module/events.js') }}"></script>
@endsection