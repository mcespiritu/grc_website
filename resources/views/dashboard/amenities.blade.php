@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="float-left"><i class="fas fa-image"></i> List of Features</div>
                        <div class="float-right"><a class="addAmenities btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addAmenities"><i class="fas fa-plus"></i></a></div>
                    </div>
                    <div class="card-body">
                        <table class="table" width="100%">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col"> </th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($amenities_list as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td>{{$row->name}}</td>
                                    <td><a class="updateAmenitiesClass btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateAmenities" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a><a class="deleteAmenities btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('dashboard.modals.addAmenities')
@include('dashboard.modals.updateAmenities')
<script type="text/javascript" src="{{ URL::asset('/js/module/amenities.js') }}"></script>
@endsection