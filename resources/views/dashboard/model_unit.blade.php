@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="py-5">

        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Model Unit List</h2>
                        <div class="float-right"><a class=" btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addUnit"><i class="fas fa-plus"></i></a></div>
                    </div>
                    <div class="card-body">
                        <div class="card-deck">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Model Unit Name</th>
                                        <th scope="col">Project</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($model_unit_list as $row)
                                    <tr>
                                        <th scope="row">{{$row->id}}</th>
                                        <td>{{$row->model_name}}</td>
                                        <td>{{$row->project_id}}</td>
                                        <td><a id="{{$row->id}}" class="updateModelUnit btn btn-info btn-border btn-round mr-2 btn-sm pull-right"  data-toggle="modal" data-target="#updateUnit"> <i class="fas fa-pencil-alt"></i></a>
                                            <a id="{{$row->id}}" class="deleteModelUnit btn btn-danger btn-border btn-round mr-2 btn-sm pull-right"><i class="fas fa-minus"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('dashboard.modals.addUnit')
@include('dashboard.modals.updateUnit')
<script src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript">
    $('a.deleteModelUnit').on('click', function(e)
{
var id = this.id;
 swal({
    title: "Are you sure?",
    text: "Are you sure you want to delete this Model Unit",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Yes, I am sure!',
    cancelButtonText: "No, cancel it!",
    closeOnConfirm: false,
    closeOnCancel: false
 },
 function(isConfirm){

   if (isConfirm){
     simPost({id:id}, 'POST', '/dashboard/deleteModelUnit', deleteModelUnitResponse);
     e.preventDefault();
     swal("Success!", "Model Unit has been deleted", "success");
    setTimeout(function(){
       window.location.reload(1);
    }, 1000);
    }
      else 
  {
   swal("Error!", "You unsucessfully deleted the Model Unit!", "error");
  } 
 });
  
  return false;
});

function deleteModelUnitResponse( response ) 
    {
         if (response == '') {
        window.setInterval(function(){
          location.reload();
        }, 1200);
        swal("Success!", "You have deleted the slider!", "success");
      return false;
    }
}

$("#updateModelUnitForm").on('submit', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    let post_data = new FormData(this)
    simPostUpload(post_data, 'POST', '/dashboard/updateModelUnit', updateModelUnitResponse);
    e.preventDefault();
        setTimeout(function(){
       window.location.reload(1);
    }, 1000);

});


function updateModelUnitResponse(response) {
    swal("Success!", "Model Unit has been updated", "success");
    $('#updateModelUnit').modal('hide');
    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
}
function modelUnitInfoResponse(response) {
    $.each(response, function(key, value) {
        $("input#id").val(value.id);
        $("select#project_id").val(value.project_id);
        $("input#model_name").val(value.model_name);
        $("input#floor_area").val(value.floor_area);
        $("input#lot_area").val(value.lot_area);
    
    });
}

$(".updateModelUnit").on('click', function(e) {

    $('.modal-message').html('');
    $('.error-message').html(""); //reset messages
    $('.form-group').removeClass('has-error');
    var id = this.id;
    simPost({
        id: id
    }, 'POST', '/fetch/modelUnitInfo', modelUnitInfoResponse);
    e.preventDefault();
});

</script>
@endsection