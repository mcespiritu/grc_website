@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                @foreach($company_summary as $row)
                <div class="card">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Company Summary</h2>
                        <div class="float-right">
                            <a class="updateHomeSummary btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#updateHomeSummary" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="POST" name="form-example-1" id="form-example-1" enctype="multipart/form-data">
                            <div class="input-field">
                                <label for="name-1">Title</label>
                                <p><em>{{$row->summary_title}}</em></p>
                                <input type="hidden" class="form-control" id="id" name="id" readonly />
                            </div>
                            <div class="input-field">
                                <label for="description-1">Content</label>
                                <p><em>{{$row->summary_content}}</em></p>
                            </div>
                        </form>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="py-3">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                
                <div class="card">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Partners / Affiliates</h2>
                        <div class="float-right">
                            <a class="addLogo btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addLogo"><i class="fas fa-pencil-alt"></i></a>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            @foreach($partners_list as $row)
                            <div class="col-md-3">
                                <a href="{{$row->link}}" target="_blank">
                                <img src="/storage/imageGallery/{{$row->logo}}" class="img-fluid" alt="..." />
                                <div class="card-body">
                                    <p class="float-right">
                                        <a class="updatePartner btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#updatePartner" id="{{$row->id}}"><i class="fas fa-minus"></i></a>
                                        <a class="deleteLogo btn btn-danger btn-border btn-round mr-2 btn-sm pull-right" id="{{$row->id}}"><i class="fas fa-minus"></i></a>
                                    </p>
                                </div>
                            </a>
                            </div>
                             @endforeach
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </section>

</div>
@include('dashboard.modals.addHomebanner')
@include('dashboard.modals.updateHomebanner')
@include('dashboard.modals.updateHomeSummary')
@include('dashboard.modals.addHomeSummary')
@include('dashboard.modals.updatepartner')
@include('dashboard.modals.uploadpartner')
<script type="text/javascript" src="{{ URL::asset('/js/module/home.js') }}"></script>
@endsection