@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Buyer's Corners Slider</h2>
                        <div class="float-right"><a class="btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addBuyerbanner"><i class="fas fa-plus"></i></a></div>
                    </div>

                    <div class="card-body">
                        <div class="card-deck">
                            <div class="row">
                                @foreach($buyerbanner_list as $row)
                                <div class="card col-md-4">
                                    <div class="row">
                                        <img src="/storage/imageGallery/{{$row->banner}}" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h4 class="card-title">{{$row->buyer_title}}</h4>
                                            <p class="card-text">{{ str_limit($row->buyer_subtitle, 40) }}</p>
                                            <p class="float-right"><a class="updateBuyerBanner btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateBuyerBanner" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a>
                                    <a class="deleteBuyerbanner btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{{--         <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Buyer's Content</h2>
                        <div class="float-right">
                           <a id="1" class="updateBuyerContent btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#updateBuyerContent"><i class="fas fa-pencil-alt"></i></a></div>
                    </div>

                    <div class="card-body">
                        @foreach($buyer_content as $row)
                        <form method="POST" name="form-example-1" id="form-example-1" enctype="multipart/form-data">
                            <div class="input-field">
                                <label for="content">Content</label>
                                <p><em>{{$row->content}}</em></p>
                            </div>
                        </form>
                        @endforeach
                    </div>
                </div>
            
            </div>
        </div>
    </section> --}}
</div>
@include('dashboard.modals.updateBuyerContent')
@include('dashboard.modals.addBuyerbanner')
@include('dashboard.modals.updateBuyerbanner')

<script type="text/javascript" src="{{ URL::asset('/js/module/buyer.js') }}"></script>
@endsection