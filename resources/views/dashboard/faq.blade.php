@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Frequently Asked Questions </h2>
                        <div class="float-right"><a class="addFAQ btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addFAQ"><i class="fas fa-plus"></i></a></div>
                    </div>
                    <div class="card-body">
                        <table class="table" width="100%">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Question</th>
                                    <th scope="col">Answer</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($faq_list as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td width="40%">{{$row->question}}</td>
                                    <td width="50%">{{$row->answer}}</td>
                                    <td width="30%"><a class="updateFAQClass btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateFAQ" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a><a class="deleteFAQ btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('dashboard.modals.addFAQ')
@include('dashboard.modals.updateFAQ')
<script type="text/javascript" src="{{ URL::asset('/js/module/faq.js') }}"></script>
@endsection