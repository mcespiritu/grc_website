@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">

                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Accounts</h2>
                        <div class="float-right"><a class="addAccount btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addAccount"><i class="fas fa-plus"></i></a></div>
                    </div>
                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('dashboard.modals.addAccount')
@endsection