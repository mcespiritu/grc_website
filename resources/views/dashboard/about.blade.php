@extends('dashboard.layouts.app') 
@section('content')
<div id="content-wrapper">
    <div class="container-fluid  px-xl-5">

        <!-- About Banner-->
        <section class="pt-5">

            <div class="row">
                <div class="col-lg-12 mb-4 mb-lg-0">
                    <div class="card mb-3">
                        <div class="card-header">
                           <h2 class="h6 text-uppercase mb-0 float-left">About Slider</h2>
                            <div class="float-right"><a class="btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addAboutbanner"><i class="fas fa-plus"></i></a></div>
                        </div>

                        <div class="card-body">
                            <div class="card-deck">
                                <div class="row">
                                    @foreach($aboutbanner_list->slice(0, 3) as $row)
                                    <div class="card col-md-4">
                                        <div class="row">
                                            <img src="/storage/imageGallery/{{$row->banner}}" class="card-img-top" alt="...">
                                            <div class="card-body">
                                                <h4 class="card-title">{{$row->about_title}}</h4>
                                                <p class="card-text">{{ str_limit($row->about_subtitle, 40) }}</p>
                                                <p class="float-right"><a class="updateAboutbanner btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateAboutbanner" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a>
                                                    <a class="deleteAboutbanner btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Vision & Mission-->
        <section class="pt-5">

            <div class="row">
                <div class="col-lg-12 mb-4 mb-lg-0">
                    <div class="card mb-3">
                        <div class="card-header">
                            <h2 class="h6 text-uppercase mb-0 float-left">Vision & Mission</h2>
                            <div class="float-right"><a id="1" class="updateVision btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#updateVision"><i class="fas fa-pencil-alt"></i></a></div>
                        </div>
                        <div class="card-body">
                            @foreach($visionmission as $row)
                            <form method="POST" name="form-example-1" id="form-example-1" enctype="multipart/form-data">
                                <div class="input-field">
                                    <label for="vision">Vision</label>
                                    <p><em>{{$row->vision}}</em></p>
                                </div>
                                <div class="input-field">
                                    <label for="mission">Mission</label>
                                    <p><em>{{$row->mission}}</em></p>
                                </div>
                                <div class="input-field">
                                    <label for="mission">Quality Policy</label>
                                    <p><em> {!! nl2br(e($row->quality_policy)) !!}</em></p>
                                </div>
                            </form>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
</div>
@include('dashboard.modals.addAboutbanner')
@include('dashboard.modals.updateAboutbanner')
@include('dashboard.modals.updateVision')
<script type="text/javascript" src="{{ URL::asset('/js/module/about.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/module/OrgChart.js') }}"></script>
@endsection