@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
            <section class="py-5">
                <div class="row">
                    <h5>WELCOME TO DASHBOARD</h5>
                </div>
            </section>
</div>
@include('dashboard.modals.addAmenities')
@include('dashboard.modals.updateAmenities')
<script type="text/javascript" src="{{ URL::asset('/js/module/amenities.js') }}"></script>
@endsection

