@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Broker's Corners Slider</h2>
                        <div class="float-right"><a class="btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addBrokerbanner"><i class="fas fa-plus"></i></a></div>
                    </div>

                    <div class="card-body">
                        <div class="card-deck">
                            <div class="row">
                                @foreach($brokerbanner_list as $row)
                                <div class="card col-md-4">
                                    <div class="row">
                                        <img src="/storage/imageGallery/{{$row->banner}}" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h4 class="card-title">{{$row->broker_title}}</h4>
                                            <p class="card-text">{{ str_limit($row->broker_subtitle, 40) }}</p>
                                            <p class="float-right"><a class="updateBrokerBanner btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateBrokerBanner" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a>
                                    <a class="deleteBrokerbanner btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
@include('dashboard.modals.addBrokerbanner')
@include('dashboard.modals.updateBrokerbanner')

<script type="text/javascript" src="{{ URL::asset('/js/module/broker.js') }}"></script>
@endsection