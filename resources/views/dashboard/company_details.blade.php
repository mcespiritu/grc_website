@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <!-- Company Details -->
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Company Details</h2>
                        <div class="float-right"><a id="1" class="updateCompanyDetails btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#updateCompanyDetails"><i class="fas fa-pencil-alt"></i></a></div>
                    </div>
                    <div class="card-body">
                        @foreach($company_details as $row)
                        <form method="POST" name="form-example-1" id="form-example-1" enctype="multipart/form-data">
                            <div class="input-field">
                                <label for="company_name">Company Name</label>
                                <p><em>{{$row->company_name}}</em></p>
                            </div>
                            <div class="input-field">
                                <label for="address">Address</label>
                                <p><em>{{$row->address}}</em></p>
                            </div>
                            <div class="input-field">
                                <label for="phone">Phone Number</label>
                                <p><em>{{$row->phone}}</em></p>
                            </div>
                            <div class="input-field">
                                <label for="mobile">Mobile Number</label>
                                <p><em>{{$row->mobile}}</em></p>
                            </div>
                            <div class="input-field">
                                <label for="email">Email Address</label>
                                <p><em>{{$row->email}}</em></p>
                            </div>
                            <div class="input-field">
                                <label for="social-media">Social Media</label>
                                <p>Facebook : <em>{{$row->facebook}}</em></p>
                                <p>Instagram : <em>{{$row->instagram}}</em></p>
                                <p>Youtube : <em>{{$row->youtube}}</em></p>
                                <p>LinkedIn : <em>{{$row->linkedin}}</em></p>
                            </div>
                        </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('dashboard.modals.updateCompanyDetails')
<script type="text/javascript" src="{{ URL::asset('/js/module/companyDetails.js') }}"></script>
@endsection