<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Greenroof - Dashboard</title>
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link href="{{ URL::asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="{{ asset('css/orionicons.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/style-dashboard.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jq_steps.css') }}">
    <link href="{{ URL::asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

    <script type="text/javascript">
         var BASE_URL = '{{ url('/') }}';
         var CSRF_TOKEN = '{{ csrf_token() }}';
         var ASSETS = '{{ asset('/') }}';
      </script>   

  <script src="{{ URL::asset('lib/jquery/jquery.min.js') }}"></script>

</head>
<style type="text/css">
    input[type=text],input[type=number] {
        width: 100%;
        padding: 8px 20px;
        border:1px solid #000;
        border-radius: 0.5rem;
    }
    .input-field {
    margin-bottom: 20px;
}

</style>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>

  <body>
    @include('dashboard.layouts.header') 
    <div class="d-flex align-items-stretch">
        @include('dashboard.layouts.sidebar') 

    <div class="page-holder w-100 d-flex flex-wrap">
        @yield('content')
      </div>
    </div>

    <!-- JavaScript files-->
    
  <script src="{{ URL::asset('lib/easing/easing.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="{{ asset('lib/popper.js/umd/popper.min.js') }}"> </script>
 <script src="{{ URL::asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('lib/jquery.cookie/jquery.cookie.js') }}"> </script>
    <script src="{{ asset('js/front.js') }}"></script>
  <script src="{{ URL::asset('lib/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
  </body>
</html>