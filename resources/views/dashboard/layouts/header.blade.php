    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="/dashboard" class="navbar-brand font-weight-bold text-uppercase text-base">Website Dashboard</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item">
          </li>
          @auth
          <li class="nav-item dropdown ml-auto"><a id="userInfo"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src="{{asset('/images/avatar.png')}}" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>
            <div aria-labelledby="userInfo" class="dropdown-menu"><a href="#" class="dropdown-item"><strong class="d-block text-uppercase headings-font-family"> {{ Auth::user()->name }}</strong><small>{{ Auth::user()->role }}</small></a>
              {{--  <div class="dropdown-divider"></div><a href="/dashboard/account-setting" class="dropdown-item">Account Setting</a> --}}
              <div class="dropdown-divider"></div><a href="{{ route('logout') }} " class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
              
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
          </li>
          @endguest
        </ul>
      </nav>
    </header>