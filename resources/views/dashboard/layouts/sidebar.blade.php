 <div id="sidebar" class="sidebar py-3">
    <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
    <ul class="sidebar-menu list-unstyled">
        @if (Auth::user()->role == 'WebAdmin')
        <li class="sidebar-list-item"><a href="/dashboard/home" class="sidebar-link text-muted  {{ Request::segment(2) === 'home' ? 'active' : null }}"><i class="fa fa-window-maximize mr-3 text-gray"></i><span>Home</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/about" class="sidebar-link text-muted {{ Request::segment(2) === 'about' ? 'active' : null }}"><i class="fa fa-question mr-3 text-gray"></i><span>About</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/project" class="sidebar-link text-muted {{ Request::segment(2) === 'project' ? 'active' : null }}"><i class="fa fa-home mr-3 text-gray"></i><span>Project</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/model_unit" class="sidebar-link text-muted {{ Request::segment(2) === 'model_unit' ? 'active' : null }}"><i class="fa fa-home mr-3 text-gray"></i><span>Model Unit</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/amenities" class="sidebar-link text-muted {{ Request::segment(2) === 'amenities' ? 'active' : null }}"><i class="fa fa-list mr-3 text-gray"></i><span>Property Features</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/careers" class="sidebar-link text-muted  {{ Request::segment(2) === 'careers' ? 'active' : null }}"><i class="fa fa-suitcase mr-3 text-gray"></i><span>Careers</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/news" class="sidebar-link text-muted  {{ Request::segment(2) === 'news' ? 'active' : null }}"><i class="fa fa-newspaper mr-3 text-gray"></i><span>News</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/events" class="sidebar-link text-muted  {{ Request::segment(2) === 'events' ? 'active' : null }}"><i class="fa fa-calendar-alt mr-3 text-gray"></i><span>Events</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/faq" class="sidebar-link text-muted  {{ Request::segment(2) === 'faq' ? 'active' : null }}"><i class="fa fa-question-circle mr-3 text-gray"></i><span>FAQs</span></a></li>
        <li class="sidebar-list-item"><a href="/dashboard/contact" class="sidebar-link text-muted  {{ Request::segment(2) === 'contact' ? 'active' : null }}"><i class="fa fa-mobile mr-3 text-gray"></i><span>Contact</span></a></li>
        <li class="sidebar-list-item "><a href="/dashboard/company_details" class="sidebar-link text-muted {{ Request::segment(2) === 'company_details' ? 'active' : null }}"><i class="fa fa-info-circle mr-3 text-gray"></i><span>Company Details</span></a></li>
@endif
        
    </ul>
</div>

<!-- 
<ul class="sidebar navbar-nav"> -->
<!--     <li class="nav-item {{ Request::segment(2) === 'company_details' ? 'active' : null }}">
        <a class="nav-link"  href="/dashboard/company_details">
            <i class="fas fa-fw fa-cog"></i>
            <span>Maintenance</span></a>
                <li class="nav-item {{ Request::segment(2) === 'company_details' ? 'active' : null }}">
                <a class="nav-link"  href="/dashboard/company_details">
            <i class="fas fa-fw fa-cog"></i>
            <span>Company Details</span></a>
    </li>
    </li> -->
<!-- 
    <li class="nav-item dropdown">
        <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-fw fa-cog"></i>
            <span>Maintenance</span>
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li class="nav-item {{ Request::segment(2) === 'company_details' ? 'active' : null }}">
                <a class="nav-link"  href="/dashboard/company_details">
                <i class="fas fa-fw fa-cog"></i>
                <span>Company Details</span></a>
            </li>
            <li class="nav-item {{ Request::segment(2) === 'amenities' ? 'active' : null }}">
                <a class="nav-link"  href="/dashboard/amenities">
                <i class="fas fa-fw fa-cog"></i>
                <span>Amenities</span></a>
            </li>
        </ul>
      </li> -->
   <!--  <li class="nav-item {{ Request::segment(2) === 'home' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/home">
            <i class="fas fa-fw fa-home"></i>
            <span>Homepage</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) === 'about' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/about">
            <i class="fas fa-fw fa-info-circle"></i>
            <span>About</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) === 'project' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/project">
            <i class="fas fa-fw fa-list-alt"></i>
            <span>Projects</span>
        </a>
    </li>
        <li class="nav-item {{ Request::segment(2) === 'bid' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/bid">
            <i class="fas fa-fw fa-money-bill"></i>
            <span>Bid</span>
        </a>
    </li>
        </li>
        <li class="nav-item {{ Request::segment(2) === 'bid-subcontractor' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/bid-subcontractor">
            <i class="fas fa-fw fa-money-bill"></i>
            <span>Bid Subcontractor</span>
        </a>
    </li>
        </li>
        <li class="nav-item {{ Request::segment(2) === 'careers' ? 'active' : null }}">
        <a class="nav-link"  href="/dashboard/careers">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Careers</span></a>
    </li>
        <li class="nav-item {{ Request::segment(2) === 'news' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/news">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>News</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) === 'events' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/events">
            <i class="fas fa-fw fa-calendar"></i>
            <span>Event</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) === 'faq' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/faq">
            <i class="fas fa-fw fa-question"></i>
            <span>FAQs</span></a>
    </li>
    <li class="nav-item {{ Request::segment(2) === 'contact' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/contact">
            <i class="fas fa-fw fa-phone"></i>
            <span>Contact</span></a>
    </li> -->
<!--     <li class="nav-item {{ Request::segment(2) === 'buyer' ? 'active' : null }}">
        <a class="nav-link" href="/dashboard/buyer">
            <i class="fas fa-fw fa-users"></i>
            <span>Buyers</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-users"></i>
            <span>Brokers</span></a>
    </li> -->
<!--     <li class="nav-item {{ Request::segment(2) === 'company_details' ? 'active' : null }}">
        <a class="nav-link"  href="/dashboard/company_details">
            <i class="fas fa-fw fa-cog"></i>
            <span>Company Details</span></a>
    </li> -->

<!-- </ul> -->

@include('dashboard.modals.addProject')