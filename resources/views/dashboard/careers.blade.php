@extends('dashboard.layouts.app') 
@section('content')
<div class="container-fluid px-xl-5">
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card mb-3">
                    <div class="card-header">
                        <h2 class="h6 text-uppercase mb-0 float-left">Career Slider</h2>
                        <div class="float-right"><a class="btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addCareerbanner"><i class="fas fa-plus"></i></a></div>
                    </div>

                    <div class="card-body">
                        <div class="card-deck">
                            <div class="row">
                                @foreach($careerbanner_list->slice(0, 3) as $row)
                                <div class="card col-md-4">
                                    <div class="row">
                                        <img src="/storage/imageGallery/{{$row->banner}}" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h4 class="card-title">{{$row->title}}</h4>
                                            <p class="card-text">{{ str_limit($row->subtitle, 40) }}</p>
                                            <p class="float-right"><a class="updateCareerbanner btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateCareerbanner" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a>
                                                <a class="deleteCareerBanner btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-5">
        <div class="row mb-4">
            <div class="col-lg-12 mb-4 mb-lg-0">
        <div class="card mb-3">
            <div class="card-header">
                <h2 class="h6 text-uppercase mb-0 float-left">Job Post</h2>
                <div class="float-right"><a class="addCareer btn btn-info btn-border btn-round mr-2 btn-sm pull-right" data-toggle="modal" data-target="#addCareer"><i class="fas fa-plus"></i></a></div>
            </div>
            <div class="card-body">
                <table class="table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Job title</th>
                            <th scope="col">Category</th>
                            <th scope="col">Employment Type</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($career_list as $row)
                        <tr>

                            <td>{{$row->id}}</td>
                            <td>{{$row->job_title}}</td>
                            <td>{{$row->categories}}</td>
                            <td>{{$row->employment_type}}</td>
                            <td><a class=" btn btn-info btn-border btn-round mr-2 btn-sm pull-right " data-toggle="modal" data-target="#updateCareer" id="{{$row->id}}"><i class="fas fa-pencil-alt"></i></a><a class="deleteCareer btn btn-danger btn-border btn-round mr-2 btn-sm pull-right " id="{{$row->id}}"><i class="fas fa-minus"></i></a></td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
            </div>
        </div>
    </section>
</div>
@include('dashboard.modals.addCareer')
@include('dashboard.modals.updateCareer')
@include('dashboard.modals.addCareerbanner')
@include('dashboard.modals.updateCareerbanner')
<script type="text/javascript" src="{{ URL::asset('/js/module/career.js') }}"></script>
@endsection