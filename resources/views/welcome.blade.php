@extends('layouts.app')
@section('content')
<div id="pageslider" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active">
            <video class="slider" preload="auto" autoplay loop muted>
                <source src="img/slider/serene.mp4" width="1024" height="552" type="video/mp4"> Your browser does not support the video tag.
            </video>
            <div class="arrow bounce">
                <a class="fa  fa-angle-double-down fa-2x" href="#about"></a>
                <p>SCROLL DOWN</p>
            </div>
            <div class="carousel-caption d-none d-block"></div>
        </div>
    </div>
</div>
<!--About  Us Section-->
<section id="about">
    <div class="container">
        <div class="row about-summary text-center">
            @foreach($company_summary as $row)
            <div class="col-lg-12 wow fadeInUp">
                <img src="{{ URL::asset('img/greenroof.jpg') }}">
            </div>
            <div class="col-lg-12 wow fadeInUp">
                <p>{!! nl2br(e($row->summary_content)) !!}</p>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="property-highlights">
    <div class="container clearfix wow fadeInUp">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <h2 class="title-hl">Property Highlights</h2>
                <p>The Genesis of Serene Royale Residences:</p>
                <p>Serene – stems from the Latin word <i>Serenus</i>; commonly used to depict a calm, peaceful, untroubled, and tranquil state. Also, used in some European royal families as a term of respect. </p>
                <p>Royal – normally used to describe quality suitable for kings or queens; depicts something as superb, excellent, first class.</p>

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="features-box">
                            <div class="features-icon">
                                <i class="icon-map"></i>
                            </div>
                            <div class="features-content">
                                <h6>
                           Convenient
                        </h6>
                                <p>
                                    The site is conveniently located at the town center, making it easy for residents to reach main roads and various points of interests.

                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="features-box">
                            <div class="features-icon">
                                <i class="icon-settings"></i>
                            </div>
                            <div class="features-content">
                                <h6>Accessible</h6>
                                <p>
                                    Easily accessible via McArthur Highway or TPLEX when you exit via Concepcion, Tarlac.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center"> <a href="/project" class="btn btn-common">Know More</a></div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div id="highlightscarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block" src="{{ URL::asset('img/featured/serene.png')}}" alt="Serene Royale">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block" src="{{ URL::asset('img/featured/serene_clubhouse.jpg')}}" alt="Serene Royale">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block" src="{{ URL::asset('img/featured/serene_topview.jpg')}}" alt="Serene Royale">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#highlightscarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#highlightscarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Partners Section -->
<section id="clients" class="text-center">
    <div class="container">
        <div class="section-header">
            <h1 class="section-title">Our Partners</h1>
            <p>Our valued partners that we work with:</p>
        </div>
        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">
             @foreach($partners_list as $row)
            <div class="col-lg-3 col-md-4 col-xs-6">
                <div class="client-logo">
                    <a href="{{$row->link}}" target="_blank"><img src="/storage/imageGallery/{{$row->logo}}" class="img-fluid" alt=""></a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- Ads Section -->

<!-- <div id="ads" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <img src="{{ URL::asset('img/ads/ads.jpg') }}"> 
      </div>
    </div>
  </div>
</div> -->
@endsection