@extends('layouts.app')
 @section('content')
<section class="contact section" style="margin-top: 60px;">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <p class="subtitle"> Got a question?</p>
                <h1 class="section-title" style="padding-bottom: 10px; padding-top: 10px;font-size:34px!important;">Contact Greenroof</h1>
                <p class="desc">
                  We’re here to help and answer any question you might have. <br>We look forward to hearing from you.
                </p>
            </div>
        </div>

        <div style=" margin-top: 50px;">
             <div class="container">
                  <div class="row">
            <div class="col-md-5 col-xs-12">
                <form action="POST" id="inquiry">
                    <div class="row">
                        <div class="form-group border-name col-12">
                            <label>Name <small class="text-danger">*</small></label>
                            <input type="text" class="form-control" id="name" name="name">
                            <small class='error-message' id="error-name"></small>
                        </div>
                        <div class="form-group col-12">
                            <label>Email <small class="text-danger">*</small></label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group col-12">
                            <label>Mobile <small class="text-danger">*</small></label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <label>Message <small class="text-danger">*</small></label>
                            <textarea type="text" class="form-control" id="message" name="message"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary col-12" style="font-size: 11px; letter-spacing: 2px;">Send Message</button>
                </form>
            </div>
            <div class="col-md-7 col-xs-12">
                <div class="faq">
                    <h1 class="section-title">How Can We Help??</h1>
                    <p>
                      <a class="faq-item" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Link with href
                      </a>
                    </p>
                    <div class="collapse" id="collapseExample">
                      <div class="card card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                      </div>
                    </div>
                                        <p>
                      <a class="faq-item" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Link with href
                      </a>
                    </p>
                    <div class="collapse" id="collapseExample">
                      <div class="card card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                      </div>
                    </div>
<!--                    <div class="contactinfo row">
                        <div class="col-3"><i class="fa fa-phone"></i><p>(02) 696 9456</p></div>
                        <div class="col-3"><i class="fa fa-phone"></i><p> (02) 696 9456</p></div>
                        <div class="col-6"><i class="fa fa-home"></i><p> Unit 1010 West Tower, Philippine Stock Exchange Center, Exchange Road, Ortigas Center, Pasig City 1600 Metro Manila</p></div>
                    </div> -->
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
@endsection
