@extends('layouts.app')
@section('content')
<div id="pageslider" class="carousel slide" data-ride="carousel">
   <div class="carousel-inner" role="listbox">
     @foreach($careerbanner_list as $row)
      <div class="carousel-item active" style="background-image: url('{{asset('/storage/imageGallery/'.$row->banner)}}')">
         <div class="carousel-caption wow fadeInUp">
            <h1 class="section-title" style="padding-bottom: 10px; padding-top: 10px;font-size:34px!important;">{{$row->title}}</h1>
            <p class="subtitle">{!! nl2br(e($row->subtitle)) !!}</p>
         </div>
      </div>

       @endforeach
      </div>
      <div class="arrow bounce">
         <a class="fa  fa-angle-double-down fa-2x" href="#career"></a>
         <p>SCROLL DOWN</p>
      </div>
   </div>
</div>
<section id="career" class="text-center">
      <div class="col-md-12">
   <div class="row">
      <div class="col-12 wow fadeInUp">
      <h1 class="section-title">Job Openings</h1>
<!--       <div class="search-box">
         <form class="search">
            <input type="text" autocomplete="off" required="required"/>
            <button type="submit" class="btn btn-primary submit">
               SEARCH
            </button>
         </form>
      </div> -->
</div>

         <div class="container">
            <div class="row">
              @if(count($career_list) > 0)
              @foreach($career_list as $row)              
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div class="box-part text-center wow fadeInUp">
                     <div class="title">
                        <h4>{{$row->job_title}}</h4>
                     </div>
                     <div class="text">
                        <span>{{$row->job_desc}}</span>
                     </div>
                     <a data-toggle="modal" href="/careers/job-description">More Info</a>
                  </div>
               </div>
                @endforeach

               @else
                               <div class="box-part text-center wow fadeInUp col-12">
                     <div class="title">
                        <h4>We're not hiring at the moment.</h4>
                     </div>
                  </div>
             
             @endif
              
            </div>
         </div>
      </div>
   </div>
</section>

<!-- <div id="jodesc" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4> Site Engineer</h4><br>
        <p class="job-description">
           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p><br>
         <div class="text-center" ><a href="mailto:hr@greenroof.com?Subject=Applicant">Apply</a></div>
      </div>
    </div>
  </div>
</div> -->
@endsection