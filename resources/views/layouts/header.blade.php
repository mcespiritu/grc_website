<header id="header" class="fixed-top">
    <div class="container">

        <div class="logo float-left">
            <a href="/" class="scrollto"><img src="{{ URL::asset('img/logo.png')}}" alt="" class="img-fluid"></a>
        </div>

        <nav class="main-nav float-right d-none d-lg-block">
            <ul>
                <li class="{{ (request()->segment(1) == '/') ? 'active' : '' }}"><a href="/">Home</a></li>
                <li class="drop-down"><a>About Us</a>
                    <ul>
                        <li class="{{ (request()->segment(2) == '/company') ? 'active' : '' }}"><a href="/about/company">Our Company</a></li>
<!--                         <li class="{{ (request()->segment(2) == '/history') ? 'active' : '' }}"><a href="/about/history">Our History</a></li> -->
                        <li class="{{ (request()->segment(2) == '/vision-mission-quality-policy') ? 'active' : '' }}"><a href="/about/vision-mission-quality-policy">Vision, Mission and Quality Policy</a></li>
                    </ul>
                </li>
                <li class="drop-down"><a>Projects</a>
                    <ul>
                    @foreach($project_list as $row)
                        <li class="{{ Request::segment(2) === '.$row->project_url' ? 'active' : null }}"><a class="serene" href="{{ url('/project/'.$row->project_url ) }}" id="{{$row->id}}">{{$row->project_name}}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="{{ (request()->segment(2) == 'careers') ? 'active' : '' }}"><a href="/careers">Careers</a></li>
                <li class="{{ (request()->segment(2) == 'contact') ? 'active' : '' }}"><a href="/contact">Contact Us</a></li>
                <li class="{{ (request()->segment(2) == 'broker') ? 'active' : '' }}"><a href="http://tracker.localdev/login">Brokers Corner</a></li>
            </ul>
        </nav><!-- .main-nav -->

    </div>
</header><!-- #header -->