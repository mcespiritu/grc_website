 @foreach($company_details as $row)
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-6 footer-info">
                   <img src="{{asset('img/logo.png')}}" alt="Greenroof">
                   <br>  <br>
                    <p>Greenroof Corporation is a real estate corporation established in 2011 and is engaged in the acquisition, development and management of real estate properties.</p>
                </div>

                <div class="col-lg-4 col-md-6 footer-contact">
                    <h4>Contact Us</h4>
                    <p>{{$row->address}} </p>
                    <strong>Phone:</strong> {{$row->phone}}
                </div>
                                <div class="col-lg-4 col-md-6 footer-contact">
                    <h4>Follow Us</h4>
                    <div class="social-links">
                       @if($row->facebook !== null) 
                        <a href="{{$row->facebook}}" target="_blank" class="facebook"><i class="zmdi zmdi-facebook"></i></i></a>
                        @else 
                        @endif
                          @if($row->linkedin !== null) 
                        <a href="{{$row->linkedin}}" target="_blank" class="linkedin"><i class="zmdi zmdi-linkedin-box"></i></i></a>
                                                @else 
                        @endif
                         @if($row->instagram !== null) 
                         <a href="{{$row->instagram}}" target="_blank" class="instagram"><i class="zmdi zmdi-instagram"></i></i></a>
                                                 @else 
                        @endif
                          @if($row->youtube !== null) 
                         <a href="{{$row->youtube}}" target="_blank" class="youtube"><i class="zmdi zmdi-youtube"></i></i></a>
                              @else 
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>

<div class="container">
    <div class="footer">
        <div class="row">
            <div class="col-4">
                <div class="copyright text-left">
                    &copy; Copyright <strong>Greenroof</strong>. All Rights Reserved
                    <br>
                </div>
            </div>
              <div class="col-4 footer-links text-center">
                    <a href="/privacypolicy">Private Policy</a> <span> - </span>
                    <a href="/termsofservice">Term of Service</a>
            </div>
            <div class="col-4 powered text-right">
                Powered By <strong><a href="https://www.itechsystemsolutions.com/" target="_blank">Itech System Solutions</a></strong>
            </div>
        </div>
    </div>
</div>
</footer><!-- #footer -->
@endforeach
<div id="cookiescontent" style="opacity: 0.8; bottom: 0px;text-align: center;">
    <div class="container">
        <p>We use cookies on this site. By using this site, you agree that we may store and access cookies on your device. <button onclick="$('#cookiescontent').remove();" class="close-div" style="margin-left: 10px;">Continue</button> </p>
    </div>
</div>
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<div id="preloader"></div>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v4.0'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="248688925568957"
  theme_color="#0084ff">
</div>

<script type="text/javascript">
  $(".close-div").on("click", function(event) {
    $("#cookiescontent").remove();
    event.preventDefault();
});
</script>