<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Greenroof Corporation</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Favicons -->
  
    <link rel="shortcut icon" href="{{ URL::asset('/img/favicon.png') }}">

    <!-- Bootstrap CSS File -->
    <link href="{{ URL::asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{ URL::asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('lib/font-awesome/css/solid.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('lib/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('fonts/material-design-iconic-font/css/material-design-iconic-font.css') }}">

    <!-- Main Stylesheet File -->
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
<!--     <link href="{{ URL::asset('css/layout.css') }}" rel="stylesheet"> -->

  <script src="{{ URL::asset('lib/jquery/jquery.min.js') }}"></script>

</head>
<body>
    @include('layouts.header')
    <main id="main">
        @yield('content')
    </main>
    @include('layouts.footer')
</body>
   <!-- JavaScript Libraries -->

  <script src="{{ URL::asset('lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ URL::asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ URL::asset('lib/easing/easing.min.js') }}"></script>
  <script src="{{ URL::asset('lib/mobile-nav/mobile-nav.js') }}"></script>
  <script src="{{ URL::asset('lib/wow/wow.min.js') }}"></script>
  <script src="{{ URL::asset('lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ URL::asset('lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ URL::asset('lib/isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ URL::asset('lib/lightbox/js/lightbox.min.js') }}"></script>
  <script src="{{ URL::asset('lib/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ URL::asset('lib/font-awesome/js/fontawesome.min.js') }}"></script>
  <script src="{{ URL::asset('lib/font-awesome/js/solid.min.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ URL::asset('js/contactform.js') }}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ URL::asset('js/main.js') }}"></script>
  <script src="{{ URL::asset('js/grc.js') }}"></script>
</html>
