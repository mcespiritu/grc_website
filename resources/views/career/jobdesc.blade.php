@extends('layouts.app')
@section('content')
<div id="pageslider" class="carousel slide" data-ride="carousel">
   <div class="carousel-inner" role="listbox">
      <div class="carousel-item active" style="background-image: url('{{asset('img/slider/careers.jpg')}}')">
         <div class="carousel-caption wow fadeInUp">
            <h1 class="section-title" style="padding-bottom: 10px; padding-top: 10px;font-size:34px!important;">SITE ENGINEER</h1>
            <p class="subtitle">Greenroof Corporation is a family corporation whose Incorporators are all professionals, mostly Engineers and Architect whose philosophy is to excel in the field of real estate development by introducing innovative concepts, and by achieving the highest levels of quality, customer care and satisfaction.</p>
         </div>
      </div>
      </div>
      <div class="arrow bounce">
         <a class="fa  fa-angle-double-down fa-2x" href="#career"></a>
         <p>SCROLL DOWN</p>
      </div>
   </div>
</div>
   <div class="container">
            <div class="row">
                  <div class="col-12 box-part text-left wow fadeInUp">
                     <div class="title">
                        <h4>Responsibilities</h4>
                     </div>
                     <div class="text">
                        <span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.</span>
                         <ul class="text-left" style="margin-top: 20px">
                            <li><i class="fas fa-check-circle"></i> Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed.</li>
                            <li><i class="fas fa-check-circle"></i> Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed.</li>
                            <li><i class="fas fa-check-circle"></i> Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed.</li>
                            <li><i class="fas fa-check-circle"></i> Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed.</li>
                            <li><i class="fas fa-check-circle"></i> Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed.</li>
                            <li><i class="fas fa-check-circle"></i> Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed.</li>
                         </ul>
                     </div>
                     <p class="text-center"><a href = "mailto:admin@greenroof.com">Apply</a></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection