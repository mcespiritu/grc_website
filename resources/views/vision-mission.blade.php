@extends('layouts.app')
 @section('content')
<div id="pageslider" class="carousel slide" data-ride="carousel">
   <div class="carousel-inner" role="listbox">
    @foreach($aboutbanner_list as $row)
      <div class="carousel-item active" style="background-image: url('{{asset('/storage/imageGallery/'.$row->banner)}}')">
         <div class="carousel-caption d-none d-md-block">      
         <h1 class="section-title" style="padding-bottom: 10px; padding-top: 10px;font-size:34px!important;">{{$row->about_title}}</h1>  
         </div>
      </div>
      @endforeach

        <div class="arrow bounce">
            <a class="fa  fa-angle-double-down fa-2x" href="#about"></a>
            <p>SCROLL DOWN</p>
         </div>

   </div>
</div>
<section class="vision">

    <div class="container">
        @foreach($visionmission as $row)
        <div class="row">
            <div class="col-md-12 col-sm-12 text-left">
                <h1 class="section-title">Vision</h1><br>
                <p class="desc">{!! nl2br(e($row->vision)) !!}
 
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 text-left"><br>
                <h1 class="section-title">MISSION</h1><br>
                <p class="desc">{!! nl2br(e($row->mission)) !!}
     
                </p>
            </div>
        </div>
 @endforeach
        <div class="row">
            <div class="col-md-12 col-sm-12 text-left"><br>
                <h1 class="section-title">QUALITY POLICY</h1><br>
                <p class="desc">
                 {!! nl2br(e($row->quality_policy)) !!}
                </p>
            </div>
        </div>
    </div>
</section>
@endsection