@extends('layouts.app')
@section('content')


<div id="pageslider" class="carousel slide" data-ride="carousel">
   <div class="carousel-inner" role="listbox">
    @foreach($aboutbanner_list as $row)
      <div class="carousel-item active" style="background-image: url('{{asset('/storage/imageGallery/'.$row->banner)}}')">
         <div class="carousel-caption d-none d-md-block">      
         <h1 class="section-title" style="padding-bottom: 10px; padding-top: 10px;font-size:34px!important;">{{$row->about_title}}</h1>  
         </div>
      </div>
      @endforeach

        <div class="arrow bounce">
            <a class="fa  fa-angle-double-down fa-2x" href="#about"></a>
            <p>SCROLL DOWN</p>
         </div>

   </div>
</div>
<section id="about">
    <div class="container wow fadeInUp">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="text-center"><img src="{{ URL::asset('img/greenroof.jpg') }}"></div><br>

                @foreach($company_summary as $row)
                <div class="text-left">
                <p>
                    {!! nl2br(e($row->summary_content)) !!}
                </p>
            </div>
            @endforeach
        </div>
</section>
@endsection